<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPembayaran extends Model
{
    protected $table = 'DetailPembayaran';

    public function pembayaran(){
    	return $this->belongsTo('App\Pembayaran', 'pembayaran_id');
    }

    public function administrator(){
    	return $this->belongsTo('App\Admin');
    }
}
