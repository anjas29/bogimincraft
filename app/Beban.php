
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beban extends Model
{
    protected $table = 'beban';

    public function pesanan(){
    	return $this->belongsTo('Pesanan');
    }
}
