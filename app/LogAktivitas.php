<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogAktivitas extends Model
{
    protected $table = 'log_aktivitas';

    public function administrator(){
    	return $this->belongsTo('App\Admin');
    }
}
