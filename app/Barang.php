<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';

    public function detail_pesanan(){
    	return $this->hasMany('App\DetailPesanan', 'barang_id');
    }

    public function kategori(){
    	return $this->belongsTo('App\Kategori');
    }
}
