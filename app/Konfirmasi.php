<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Konfirmasi extends Model
{
	protected $table = 'konfirmasi';

	public function pesanan(){
		return $this->belongsTo('App\Pesanan');
	}
}
