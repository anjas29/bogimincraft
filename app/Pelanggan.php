<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Pelanggan extends Authenticatable
{
    protected $table = 'pelanggan';
    
    protected $hidden = array('remember_token', 'password');

    public function pesanan(){
    	return $this->hasMany('App\Pesanan');
    }
}
