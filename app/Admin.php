<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    protected $table = 'administrator';

    protected $hidden = array('remember_token', 'password');

    public function log_aktivitas(){
    	return $this->hasMany('App\LogAktivitas');
    }

    public function detail_pembayaran(){
    	return $this->hasMany('App\DetailPembayaran');
    }

    public function isAdmin(){
    	if($this->role == 'Administrator'){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function isKasir(){
    	if($this->role == 'Kasir'){
    		return true;
    	}else{
    		return false;
    	}
    }
}
