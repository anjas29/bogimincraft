<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPesanan extends Model
{
    protected $table = 'detail_pesanan';

    public function barang(){
    	return $this->belongsTo('App\Barang');
    }

    public function pesanan(){
    	return $this->belongsTo('App\Pesanan');
    }
}
