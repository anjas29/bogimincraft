<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();


Route::get('login-administrator', 'AuthController@getAdminLogin');
Route::post('login-administrator', 'AuthController@postAdminLogin');
Route::get('logout-administrator', 'AuthController@getAdminLogout');

Route::group(array('middleware'=>'admin'),function(){
    Route::controller('administrator','AdminController');
});
Route::group(array('prefix'=>'/'), function(){
	Route::get('/', 'PelangganController@getIndex')->name('pelanggan_index');
    Route::get('/index', 'PelangganController@getIndex');

    Route::get('register', 'PelangganController@getRegister')->name('pelanggan_register');
    Route::post('register', 'PelangganController@postRegister')->name('pelanggan_register_post');
    Route::get('login', 'AuthController@getUserLogin')->name('pelanggan_login');
    Route::post('login', 'AuthController@postUserLogin')->name('pelanggan_login_post');
    Route::get('logout', 'PelangganController@getLogout')->name('pelanggan_logout');

    Route::get('profil', 'PelangganController@getProfil')->name('pelanggan_profil');
    Route::post('profil', 'PelangganController@postProfil');
    Route::get('password', 'PelangganController@getPassword');
    Route::post('password', 'PelangganController@postPassword');
    Route::get('panduan', 'PelangganController@getPanduan')->name('pelanggan_panduan');
    Route::get('pesanan', 'PelangganController@getPesanan')->name('pelanggan_pesanan');
    Route::get('detail-pesanan/{id}', 'PelangganController@getDetailPesanan');

    Route::get('barang/{id}', 'PelangganController@getDetailBarang')->name('pelanggan_detail_barang');
    Route::post('cart/tambah', 'PelangganController@postTambahCart')->name('pelanggan_tambah_cart_post');
    Route::post('cart/update', 'PelangganController@postPerbaruiKantongBelanja')->name('pelanggan_perbarui_cart_post');
    Route::post('cart/delete', 'PelangganController@postHapusKantongBelanja')->name('pelanggan_hapus_cart_post');
    Route::get('kantong-belanja', 'PelangganController@getKantongBelanja');
    Route::get('kantong-belanja/detail', 'PelangganController@getDetailKantongBelanja');
    Route::post('kantong-belanja/detail', 'PelangganController@postDetailKantongBelanja');
    Route::post('jasa-pengiriman', 'PelangganController@postJasaPengiriman');
    Route::post('kota-pengiriman', 'PelangganController@postKotaPengiriman');
    Route::get('list-kota-pengiriman', 'PelangganController@getKota');
    Route::post('check-estimasi-pengiriman', 'PelangganController@postCheckEstimasiPengiriman');

    Route::get('invoice', 'PelangganController@getInvoice');
    Route::post('invoice', 'PelangganController@postInvoice');
    Route::get('print-invoice', 'PelangganController@getPrintInvoice');
    Route::get('checkout', 'PelangganController@getCheckOut');
    Route::post('konfirmasi-pembayaran', 'PelangganController@postKonfirmasiPembayaran');
});


Route::get('/images/barang/{filename}', function ($filename)
{
    $path = storage_path() . '/app/produk/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/images/user/{filename}', function ($filename)
{
    $path = storage_path() . '/app/users/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/images/slider/{filename}', function ($filename)
{
    $path = storage_path() . '/app/slider/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/images/bukti-pembayaran/{filename}', function ($filename)
{
    $path = storage_path() . '/app/bukti_pembayaran/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('/images/{filename}', function ($filename)
{
    $path = storage_path() . '/app/images/' . $filename;

    if(!File::exists($path)) return abort('404');

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
Route::get('/token', function(){return csrf_token();});