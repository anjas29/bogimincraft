<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\LogAktivitas;
use App\Admin;
use App\Pelanggan;

use Session;
use Auth;
use Input;
use Hash;

class AuthController extends Controller
{
    public function getAdminLogin(){
        if(auth('admin')->check()){
            return redirect('/administrator');
        }
    	return view('auth_view.login_admin');
    }

    public function postAdminLogin(Request $request){
    	$auth = auth('admin');

        $credential = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if($auth->attempt($credential)){
            $user = auth('admin')->user();
            $log = new LogAktivitas;
            $log->administrator_id = $user->id;
            $log->aktivitas = 'Login Sistem';
            $log->save();

            if($user->isAdmin())
            	return redirect('/administrator');
            else
            	return redirect('/administrator');
        }else{
            Session::flash('loginFailed', 'Login Gagal!');
            return redirect('/login-administrator');
        }
    	return redirect('/login-administrator');
    }

    public function getAdminLogout(){
        auth('admin')->logout();

        return redirect('/login-administrator');
    }

    public function getUserLogin(){
    	return view('pelanggan.login');
    }

    public function postUserLogin(Request $request){
    	$auth = auth('pelanggan');

        $credential = [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        if($auth->attempt($credential)){
            // $user = auth('pelanggan')->user();
            return redirect()->intended('/');
        }else{
            Session::flash('loginFailed', 'Login Gagal!');
            return redirect('/login');
        }
    	return redirect('/login');
    }

    public function getUserLogout(){
        auth('pelanggan')->logout();

        return redirect('/');
    }
}
