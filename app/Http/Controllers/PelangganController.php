<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Barang;
use App\Kategori;
use App\TarifPengiriman;
use App\Pesanan;
use App\KonfirmasiPesanan;
use App\DetailPesanan;
use App\Pelanggan;
use App\Konfirmasi;
use App\User;
use App\LaporanBarang;

use DB;
use Cart;
use Session;
use Validator;
use Hash;
use Carbon\Carbon;

class PelangganController extends Controller
{
    public function __construct(){
        $sites = array('getIndex', 'getPanduan', 'getLogin', 'getRegister', 'postLogin', 'getLogout', 'getDetailBarang', 'postRegister'
            ,'getKantongBelanja', 'postTambahCart', 'postJasaPengiriman', 'postPerbaruiKantongBelanja', 'postHapusKantongBelanja', 'postKotaPengiriman', 'getKota', 'postCheckEstimasiPengiriman');

        $this->middleware('auth:pelanggan', ['except' => $sites]);
    }

    public function getLogout(){
        auth('pelanggan')->logout();
        Cart::destroy();
        return redirect('/');
    }

    public function getIndex(Request $request){
        $bestSeller = $this->generateLaporanBestSellerBulanan(date('Y'), date('n'));

    	$category_name = "Semua";
        $count = Barang::count();
        $limit = 12;
        $kategori = Kategori::all();
        $searchWord = $request->input('search');
        $kategoriWord = $request->input('category');

        if(!is_null($request->get('limit'))){
            $limit = $request->get('limit');
        }

        if($kategoriWord != null){
            $data = Barang::whereHas('kategori', function($q) use ($kategoriWord)
                {
                    $q->where('nama', '=', $kategoriWord);
                })->orderBy('barang.created_at', 'DESC')->paginate($limit);
        }elseif($searchWord!= null){
            $data = Barang::where('nama', 'like', '%' . $searchWord . '%')->orderBy('created_at', 'DESC')->paginate($limit);
        }else{
            $data = Barang::orderBy('created_at', 'DESC')->paginate($limit);
        
            if(!is_null($request->get('limit'))){
                $data->appends(['limit'=>$limit])->links();
            }
        }
        

        return view('index')->withData($data)
                    ->withKategori($kategori)
                    ->withSelect($category_name)
                    ->withCount($count)
                    ->withBestSeller($bestSeller)
                    ->withSearch($searchWord)
                    ->withCategory($kategoriWord);
    }
    public function generateLaporanBestSellerBulanan($tahun, $bulan){
        $data = DetailPesanan::select('*','barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun, $bulan){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
            })->first();

        if($data == null){
            $data = DetailPesanan::select('*','barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->first();
        }
        return $data;
    }

    public function getRegister(){
        return view('pelanggan.register');
    }

    public function postRegister(Request $request){
        $rules = ['captcha' => 'required|captcha'];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
        {
            Session::flash('messageError', 'Captcha tidak sesuai');
            return redirect('/register');
        }else{
            $data = new Pelanggan;
            $data->nama = $request->input('nama');
            $data->alamat = $request->input('alamat');
            $data->kota = $request->input('kota');
            $data->kode_pos = $request->input('kode_pos');
            $data->no_telepon = $request->input('no_telepon');
            $data->email = $request->input('email');
            $data->password = Hash::make($request->input('password'));
            $data->save();

            Session::flash('registrationSuccess', 'Registrasi Berhasil');
            return redirect('/login');
        }
    }

    public function getDetailBarang($id){
        $data = Barang::where('id', $id)->firstOrFail();    

        return view('pelanggan.detail_barang')->withData($data);
    }

    public function postTambahCart(Request $request){
        $id = $request->input('id');
        $qty = $request->input('jumlah');

        $barang = Barang::findOrFail($id);
        $harga = $barang->harga_jual;
        
        foreach (Cart::content() as $i => $c) {
            if($c->id == $barang->id)
                if(($c->qty + $qty) > $barang->persediaan){
                    Session::flash('messageError','Persediaan tidak mencukupi');
                    return redirect('/kantong-belanja');        
                }
        }

        if($qty <= 0){
            Session::flash('messageError','Barang tidak valid');
            return redirect('/kantong-belanja');
        }if($qty > $barang->persediaan){
            Session::flash('messageError','Persediaan tidak mencukupi');
            return redirect('/kantong-belanja');
        }

        Cart::add(array(
                'id' => $id,
                'name' => $barang->nama,
                'qty' => $qty,
                'price' => $harga,
                'options'=> array('foto' => $barang->foto, 'berat' => $barang->berat)
            ));
        Session::flash('message', 'Barang berhasil ditambahkan');
        return redirect('/kantong-belanja');
    }

    public function getKantongBelanja(){
        
        $cart = Cart::content();
        $data = array(
                'count' => Cart::count(),
                'total' => Cart::total(),
                'details' => $cart
            );
        $diskon = 0;
        if(Cart::count() >= 10){
            $diskon = 10/100 * Cart::subtotal('0',',','');
        }
        Session::put('temp_diskon', $diskon);

        return view('pelanggan.kantong_belanja')->withData($data)->withDiskon($diskon);
    }

    public function postPerbaruiKantongBelanja(Request $request){
        $rowid = $request->input('rowId');
        $qty = $request->input('qty');     
        $id = $request->input('id');

        if(!isset($rowid) || !isset($qty)){
            Session::flash('messageError','Barang tidak valid');
            return redirect('/kantong-belanja');
        }   

        foreach ($rowid as $i => $r) {
            if($qty[$i] > Barang::where('id', $id[$i])->first()->persediaan){
                Session::flash('messageError','Persediaan tidak mencukupi');
                return redirect('/kantong-belanja');
            }

            if($qty[$i] <= 0){
                Session::flash('messageError','Minimal pembelian barang adalah 1');
                return redirect('/kantong-belanja');
            }

            Cart::update($r, ['qty' => $qty[$i]]);
        }

        Session::flash('messageSuccess', 'Update barang berhasil');
        return redirect('/kantong-belanja');

    }

    public function postHapusKantongBelanja(Request $request){
        $rowId = $request->input('rowid');
        Cart::remove($rowId);   
    }

    public function getDetailKantongBelanja(){
        $cart = Cart::content();
        $data = array(
                'count' => Cart::count(),
                'total' => Cart::total(),
                'details' => $cart
            );
        $kota = TarifPengiriman::select('nama_kota')->orderBy('nama_kota', 'asc')->distinct()->get();
        $jenis = TarifPengiriman::select('jasa_pengiriman')->orderBy('jasa_pengiriman', 'asc')->distinct()->get();
        $diskon = Session::get('temp_diskon');

        return view('pelanggan.detail_kantong')->withData($data)->withKota($kota)->withJenis($jenis)->withDiskon($diskon);
    }

    public function postDetailKantongBelanja(Request $request){
        $data = Pesanan::orderBy('id', 'desc')->first();
        $id = 1;
        if($data != null){
            $id = $data->id + 1;
        }

        $no_pesanan = 'PO'.sprintf('%05d', $id);
        Session::put('temp_no_pesanan', $no_pesanan);
        Session::put('temp_nama', $request->input('nama'));
        Session::put('temp_alamat', $request->input('alamat'));
        Session::put('temp_no_telepon', $request->input('no_telepon'));
        Session::put('temp_email', $request->input('email'));
        Session::put('temp_tarif_pengiriman', $request->input('tarif_pengiriman'));
        Session::put('temp_harga_per_kg', $request->input('harga_per_kg'));
        Session::put('temp_kota_tujuan', $request->input('kota'));
        Session::put('estimasi_waktu', $request->input('estimasi_waktu'));
        Session::put('temp_jasa_pengiriman', $request->input('jasa_pengiriman'));

        return redirect('invoice');
    }

    public function postKotaPengiriman(Request $request){
        $data = TarifPengiriman::where('nama_kota', $request->input('kota'))->get();
        return $data;
    }

    public function postJasaPengiriman(Request $request){
        $data = TarifPengiriman::where('nama_kota', $request->input('kota'))->where('jasa_pengiriman', $request->input('jasa'))->first();
        return $data;
    }
    
    public function getInvoice(){
        $cart = Cart::content();
        $data = array(
                'count' => Cart::count(),
                'total' => Cart::total(),
                'details' => $cart
            );
        Session::put('temp_kode_unik', rand(1, 999));

        return view('pelanggan.invoice')->withData($data);
    }

    public function getKota(){
        $data = TarifPengiriman::select(DB::raw("DISTINCT nama_kota, estimasi_waktu"))->get();

        return $data;
    }

    public function postCheckEstimasiPengiriman(Request $request){
        $data = TarifPengiriman::where('nama_kota', $request->input('nama_kota'))->first();

        if($data != null){
            return $data->estimasi_waktu;
        }else{
            return -1;
        }
    }

    public function getPrintInvoice(){
        $cart = Cart::content();
        $data = array(
                'count' => Cart::count(),
                'total' => Cart::total(),
                'details' => $cart
            );
        return view('pelanggan.print-invoice')->withData($data);
    }

    public function postInvoice(Request $request){
        $data = new Pesanan;

        $data->pelanggan_id = auth('pelanggan')->user()->id;
        $data->no_pesanan = Session::get('temp_no_pesanan');
        $data->nama_pelanggan = Session::get('temp_nama');
        $data->no_telepon = Session::get('temp_no_telepon');
        $data->kota = Session::get('temp_kota_tujuan');
        $data->alamat = Session::get('temp_alamat');
        $data->jenis_transaksi = 'Online';
        $data->potongan_harga = Session::get('temp_diskon');
        $data->berat = $request->input('berat');
        $data->tarif_pengiriman = Session::get('temp_tarif_pengiriman');
        $data->total_pembayaran = Cart::subtotal('0',',','');
        $data->jasa_pengiriman = Session::get('temp_jasa_pengiriman');
        $data->ongkos_per_kg = Session::get('temp_harga_per_kg');
        $data->kode_unik = Session::get('temp_kode_unik');

        //$data->estimasi_waktu = Session::get('estimasi_waktu');
        $data->tanggal_pesanan = Carbon::now();
        $data->tanggal_kirim = Carbon::now();
        $data->status = 'Konfirmasi Pembayaran';
        $data->kode_pesanan = str_random('12');
        $data->save();

        foreach (Cart::content() as $c) {
            $item = new DetailPesanan;
            $item->pesanan_id = $data->id;
            $item->barang_id = $c->id;
            $item->harga = $c->price;
            $item->jumlah = $c->qty;
            $item->save();

            $barang = Barang::where('id', $c->id)->first();
            $barang->persediaan -= $c->qty;
            $barang->save();
            $this->addBarangKeluar($barang->id, $c->qty, $barang->persediaan);
        }

        $total = $data->total_pembayaran - $data->diskon + $data->tarif_pengiriman + Session::get('temp_kode_unik');

        Session::forget('temp_no_pesanan');
        Session::forget('temp_nama');
        Session::forget('temp_alamat');
        Session::forget('temp_no_telepon');
        Session::forget('temp_tarif_pengiriman');
        Session::forget('temp_harga_per_kg');
        Session::forget('temp_kota_tujuan');
        Session::forget('estimasi_waktu');

        Session::flash('messageSuccess', 'Pensanan berhasil');
        Cart::destroy();
        return view('pelanggan.checkout')->withTotal($total)->withKode($data->kode_pesanan)->withData($data);
    }

    public function postKonfirmasiPembayaran(Request $request){

        $file = array('image' => $request->file('gambar'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);

        if($validator->fails()){

        }else{
            if($request->file('gambar')->isValid()){
                $id = $request->input('id');
                $pesanan = Pesanan::where('id', $id)->firstOrFail();

                $path = storage_path().'/app/bukti_pembayaran/';
                $extension = $request->file('gambar')->getClientOriginalExtension();

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Bukti Tidak Valid');
                    return redirect('/pesanan/');
                }

                $fileName = Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;
                
                $request->file('gambar')->move($path, $fileName);

                $data = new Konfirmasi;
                $data->pesanan_id = $id;
                $data->bank = $request->input('bank');
                $data->nominal = $request->input('nominal');
                $data->no_rekening = $request->input('no_rekening');
                $data->nama_pengirim = $request->input('nama_pengirim');
                $data->tanggal_transfer = $request->input('tanggal');
                $data->status_konfirmasi = 0;
                $data->bukti = $fileName;

                $data->save();

                $pesanan->status = 'Proses';
                $pesanan->save();
            }
        }

        return redirect('/pesanan');
    }

    public function getCheckOut(){
        return view('pelanggan.checkout')->withTotal(500000)->withKode("243u7fysdf9o97");
    }

    public function getProfil(){
        $data = auth('pelanggan')->user();
        return view('pelanggan.profil')->withData($data);
    }

    public function postProfil(Request $request){
        $file = array('image' => $request->file('foto'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);

        if($validator->fails()){
            $data = auth('pelanggan')->user();
            $data->nama = $request->input('nama');
            $data->kode_pos = $request->input('kode_pos');
            $data->kota = $request->input('kota');
            $data->alamat = $request->input('alamat');
            $data->no_telepon =$request->input('no_telepon');
            $data->save();
        }else{
            if($request->file('foto')->isValid()){
                
                $data = auth('pelanggan')->user();

                $path = storage_path().'/app/users/';
                $extension = $request->file('foto')->getClientOriginalExtension();

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Tidak Valid');
                    return redirect('/profil/');
                }

                $fileName = $data->fileName;
                if($fileName == null){
                    $fileName = Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;    
                }

                $request->file('foto')->move($path, $fileName);

                $data->nama = $request->input('nama');
                $data->kode_pos = $request->input('kode_pos');
                $data->alamat = $request->input('alamat');
                $data->kota = $request->input('kota');
                $data->foto = $fileName;
                $data->no_telepon =$request->input('no_telepon');
                $data->save();
            }
        }
        return redirect('/profil');
    }

    public function getPassword(){
        return view('pelanggan.password');
    }

    public function postPassword(Request $request){
        $data = auth('pelanggan')->user();
        if(Hash::check($request->input('old-password'), $data->password)){
            $data->password = Hash::make($request->input('password'));
            $data->save();
            Session::flash('messageSuccess', 'Password berhasil diubah.');
        }else{
            Session::flash('messageError', 'Password lama tidak sesuai!');
        }

        return redirect('/password');
    }

    public function getPanduan(){
        return view('pelanggan.panduan');
    }

    public function getPesanan(){
        $id = auth('pelanggan')->user()->id;

        $data = Pesanan::where('pelanggan_id', $id)->orderBy('created_at', 'desc')->get();
        return view('pelanggan.pesanan')->withData($data);
    }

    public function getDetailPesanan($id){
        $data = Pesanan::where('id', $id)->firstOrFail();

        return view('pelanggan.detail_pesanan')->withData($data);
    }

    public function addBarangKeluar($id, $jumlah, $persediaan){
        $data = new LaporanBarang;
        $data->barang_id = $id;
        $data->keluar = $jumlah;
        $data->persediaan = $persediaan;
        $data->save();
    }
}


