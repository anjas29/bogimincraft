<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use Session;
use Hash;
use Validator;
use Carbon\Carbon;
use Cart;
use DB;


use App\Barang;
use App\Kategori;

use App\Pesanan;
use App\DetailPesanan;
use App\Konfirmasi;

use App\Pelanggan;
use App\Admin;
use App\LogAktivitas;

use App\LaporanBarang;

use App\TarifPengiriman;

class AdminController extends Controller
{
    public function __construrct(){
        $sites = array('getLogin','postLogin', 'getLogout');

        $this->middleware('admin', ['except' => $sites]);
    }

    public function getIndex(){
        return view('admin.index');
    }

    public function getLogout(){
        auth('admin')->logout();
        return redirect('/login');
    }

    public function getBarang(){
        $data = Barang::with('kategori')->get();
        $kategori = Kategori::all();

    	return view('admin.barang')->withData($data)->withKategori($kategori);
    }

    public function getKategori(){
        $data = Kategori::all();

        return view('admin.kategori')->withData($data);
    }

    public function postTambahKategori(Request $request){
        $data = new Kategori;
        $data->nama = $request->input('nama');
        $data->keterangan = $request->input('keterangan');
        $data->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Kategori : ' . $data->nama);
        
        Session::flash('messageSuccess', 'Tambah kategori berhasil');
        return redirect('/administrator/kategori');

    }

    public function postEditKategori(Request $request){
        $id = $request->input('id');

        $data = Kategori::where('id', $id)->firstOrFail();

        $data->nama = $request->input('nama');
        $data->keterangan = $request->input('keterangan');
        $data->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Edit Kategori : ' . $data->nama);

        Session::flash('messageSuccess', 'Edit kategori berhasil');
        return redirect('/administrator/kategori');
    }

    public function postHapusKategori(Request $request){
        $id = $request->input('rowid');

        $kategori = Kategori::where('id', $id)->first();
        $nama = $kategori->nama;

        if(!is_null($kategori)){
            $kategori->delete();
            $this->addLogAktivitas(auth('admin')->user()->id, 'Hapus Kategori : ' . $nama);
        }else{
            Session::flash('messageError', 'Kategori tidak valid');
            return redirect('/administrator/kategori');
        }
        
        Session::flash('messageSuccess', 'Hapus Kategori berhasil');
        return redirect('/administrator/kategori');
    }

    public function postTambahBarang(Request $request){
        $file = array('image' => $request->file('gambar'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);

        $kategori = Kategori::where('id', $request->input('kategori_id'))->first();

        if($validator->fails()) {
                $barang = new Barang;
                $barang->nama = $request->input('nama');
                $barang->kategori_id = $request->input('kategori_id');
                $barang->harga_jual = $request->input('harga');
                $barang->persediaan = $request->input('stok');
                $barang->ukuran = $request->input('ukuran');
                $barang->berat = $request->input('berat');
                $barang->harga_pokok = $request->input('harga_pokok');
                $barang->deskripsi = $request->input('deskripsi');
                $barang->foto = 'default.jpg';
                $saved = $barang->save();

                $barang->kode_barang = 'BG'. sprintf('%05d', $barang->id);
                $barang->save();

                $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Barang : ' . $barang->nama);

                $this->addBarangMasuk($barang->id, $barang->persediaan, $barang->persediaan);
        } else {
            if($request->file('gambar')->isValid()){

                $path = storage_path().'/app/produk/';
                $extension = $request->file('gambar')->getClientOriginalExtension();

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Barang Tidak Valid');
                    return redirect('/administrator/barang/');
                }

                $fileName = Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;
                $request->file('gambar')->move($path, $fileName);
                

                $barang = new Barang;
                $barang->nama = $request->input('nama');
                $barang->kategori_id = $request->input('kategori_id');
                $barang->harga_jual = $request->input('harga');
                $barang->persediaan = $request->input('stok');
                $barang->ukuran = $request->input('ukuran');
                $barang->berat = $request->input('berat');
                $barang->harga_pokok = $request->input('harga_pokok');
                $barang->deskripsi = $request->input('deskripsi');
                $barang->foto = $fileName;
                $saved = $barang->save();

                $barang->kode_barang = 'BG'. sprintf('%05d', $barang->id);
                $barang->save();

                $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Barang : ' . $barang->nama);
                $this->addBarangMasuk($barang->id, $barang->persediaan, $barang->persediaan);
            }else{
                return abort('500');
            }
        }

        if($saved){
            Session::flash('messageSuccess', 'Tambah Barang Berhasil');
            return redirect('/administrator/barang/');
        }else{
            Session::flash('messageError', 'Tambah Barang Gagal');
            return redirect('/administrator/barang/');
        }
    }

    public function postTambahPersediaan(Request $request){
        $id = $request->input('id');
        $stok = $request->input('stok');

        if($stok >= 0){
            $barang = Barang::where('id', $id)->firstOrFail();
            $barang->persediaan += $stok;
            $barang->save();
            $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Persediaan : ' . $barang->nama);
            $this->addBarangMasuk($barang->id, $stok, $barang->persediaan);
        }else{
            Session::flash('messageError', 'Jumlah barang tidak valid');
            return redirect('/administrator/barang');            
        }            

        Session::flash('messageSuccess', 'Tambah persediaan berhasil');
        return redirect('/administrator/barang');
    }

    public function postEditBarang(Request $r){
        $file = array('image' => $r->file('gambar'));
        $rules = array('image' => 'required');

        $validator = Validator::make($file, $rules);

        $id = $r->input('id');

        if($validator->fails()){
            $barang = Barang::where('id', $id)->firstOrFail();
            $barang->nama = $r->input('nama');
            $barang->nama = $r->input('nama');
            if($r->input('kategori_id') != null)
                $barang->kategori_id = $r->input('kategori_id');
            $barang->harga_jual = $r->input('harga');
            $barang->ukuran = $r->input('ukuran');
            $barang->berat = $r->input('berat');
            $barang->harga_pokok = $r->input('harga_pokok');
            $barang->deskripsi = $r->input('deskripsi');
            $saved = $barang->save();

            $this->addLogAktivitas(auth('admin')->user()->id, 'Edit Barang : ' . $barang->nama);
        }else{
            if($r->file('gambar')->isValid()){

                $barang = Barang::where('id', $id)->firstOrFail();

                $path = storage_path().'/app/produk/';
                $extension = $r->file('gambar')->getClientOriginalExtension();

                if(strcasecmp($extension, 'jpg') != 0 && strcasecmp($extension, 'jpeg') != 0 && strcasecmp($extension, 'png') != 0){
                    Session::flash('messageError', 'Gambar Barang Tidak Valid');
                    return redirect('/administrator/barang/');
                }

                $fileName = $barang->foto;
                if($fileName == 'default.jpg'){
                    $fileName = Carbon::now()->format('YmdHis').rand(1,99).'.'.$extension;    
                }
                
                $r->file('gambar')->move($path, $fileName);
                
                $barang->nama = $r->input('nama');
                $barang->nama = $r->input('nama');
                //$barang->kategori_id = $r->input('kategori_id');
                $barang->harga_jual = $r->input('harga');
                $barang->ukuran = $r->input('ukuran');
                $barang->berat = $r->input('berat');
                $barang->harga_pokok = $r->input('harga_pokok');
                $barang->deskripsi = $r->input('deskripsi');
                $barang->foto = $fileName;
                $saved = $barang->save();

                $this->addLogAktivitas(auth('admin')->user()->id, 'Edit Barang : ' . $barang->nama);
            }
        }

        if($saved){
            Session::flash('messageSuccess', 'Edit Barang Berhasil');
            return redirect('/administrator/barang/');
        }else{
            Session::flash('messageError', 'Edit Barang Gagal');
            return redirect('/administrator/barang/');
        }
    }

    public function postHapusBarang(Request $request){
        $id = $request->input('rowid');

        $barang = Barang::where('id', $id)->first();
        if(!is_null($barang)){
            $barang->delete();
            $this->addLogAktivitas(auth('admin')->user()->id, 'Hapus Barang : ' . $barang->nama);
        }else{
            Session::flash('messageError', 'Barang tidak valid');
            return redirect('/administrator/barang');    
        }
        
        Session::flash('messageSuccess', 'Hapus barang berhasil');
        return redirect('/administrator/barang');
    }



    public function getPesanan(){
        $data = Pesanan::with('pelanggan')->where('jenis_transaksi', 'Online')->with('konfirmasi')->orderBy('created_at','desc')->get();
        
    	return view('admin.pesanan')->withData($data);
    }

    public function getDetailPesanan($id){
        $data = Pesanan::where('id', $id)->with('detail_pesanan.barang')->with('pengiriman')->firstOrFail();
        
        return view('admin.detail_pesanan')->withData($data);
    }

    public function getPrintInvoice($id){
        $data = Pesanan::where('id', $id)->with('detail_pesanan.barang')->firstOrFail();

        return view('admin.print-invoice')->withData($data);
    }

    public function getPrintInvoicePesanan($id){
        $data = Pesanan::where('id', $id)->with('detail_pesanan.barang')->with('pengiriman')->firstOrFail();

        return view('admin.print-invoice-pesanan')->withData($data);
    }

    public function postHapusPesanan(Request $request){
        $id = $request->input('rowid');
        $data = Pesanan::with('detail_pesanan')->where('id', $id)->firstOrFail();

        $no_pesanan = $data->no_pesanan;

        foreach($data->detail_pesanan as $d){
            $barang = Barang::where('id','=',$d->barang_id)->first();
            $barang->persediaan += $d->jumlah;
            $barang->save();

            $lap_barang = new LaporanBarang;
            $lap_barang->barang_id = $barang->id;
            $lap_barang->masuk = $d->jumlah;
            $lap_barang->keluar = 0;
            $lap_barang->persediaan = $barang->persediaan;
            $lap_barang->save();
        }

        $data->delete();
        $this->addLogAktivitas(auth('admin')->user()->id, 'Hapus Pesanan : ' . $no_pesanan);
        Session::flash('messageSuccess', 'Hapus Pesanan berhasil');

    }

    public function getKonfirmasiPembayaran($id){
        $data = Pesanan::with('konfirmasi')->where('id', $id)->first();

        return view('admin.verifikasi_pembayaran')->withData($data);
    }

    public function postKonfirmasiPembayaran(Request $request){
        $id = $request->input('rowid');
        $data = Konfirmasi::with('pesanan')->where('id', $id)->firstOrFail();

        $data->status_konfirmasi = 1;
        $data->administrator_id = auth('admin')->user()->id;
        $data->save();

        $pesanan = Pesanan::where('id', $data->pesanan_id)->firstOrFail();
        $pesanan->status = 'Dalam Pengiriman';

        $pesanan->save();

        $no_pesanan = $pesanan->no_pesanan;
        $this->addLogAktivitas(auth('admin')->user()->id, 'Konfirmasi Pembayaran Pesanan : ' . $no_pesanan);
    }

    public function getPenjualanKasir(){
        $data = Pesanan::where('jenis_transaksi', 'Offline')->orderBy('created_at', 'desc')->get();

    	return view('admin.penjualan_kasir')->withData($data);
    }

    public function getDetailPenjualanKasir($id){
        $data = Pesanan::where('id', $id)->with('detail_pesanan.barang')->firstOrFail();

        return view('admin.detail_penjualan')->withData($data);
    }

    public function getPenjualanDataPembeli(){
        Session::forget('kasir_nama_pelanggan');
        Session::forget('kasir_no_telepon');
        Session::forget('kasir_no_pesanan');
        Session::forget('kasir_alamat');
        Session::forget('kasir_kota');
        Session::forget('kasir_diskon');
        Session::forget('kasir_berat');
        Cart::destroy();

        $data = Pesanan::orderBy('id', 'desc')->first();
        $id = 1;
        if($data != null){
            $id = $data->id + 1;
        }

        $no_pesanan = 'PF'.sprintf('%05d', $id);


        Session::put('kasir_no_pesanan', $no_pesanan);
        return view('admin.penjualan_kasir_pelanggan')->withNoPesanan($no_pesanan);
    }

    public function postPenjualanDataPembeli(Request $request){
        Session::put('kasir_nama_pelanggan', $request->input('nama_pelanggan'));
        Session::put('kasir_no_telepon', $request->input('no_telepon'));
        Session::put('kasir_kota', $request->input('kota'));
        Session::put('kasir_alamat', $request->input('alamat'));

        return redirect('/administrator/penjualan-data-barang');
    }

    public function getPenjualanDataBarang(){
        $barang = Barang::all();
        $data = Cart::content();

        return view('admin.penjualan_kasir_barang')->withData($data)->withBarang($barang);
    }

    public function postPenjualanTambahBarang(Request $request){
        $id = $request->input('barang_id');
        $qty = $request->input('jumlah');
        if($qty > Barang::where('id', $id)->first()->persediaan){
                Session::flash('messageError','Jumlah barang tersedia tidak mencukupi');
                return redirect('/administrator/penjualan-data-barang');
        }
        if($qty <= 0 ){
            Session::flash('messageError','Minimal pembelian barang adalah 1');
            return redirect('/administrator/penjualan-data-barang');
        }

        $barang = Barang::where('id',$id)->first();

        if(is_null($barang)){
            Session::flash('messageError','Barang tidak ditemukan');
            return redirect('/administrator/penjualan-data-barang');
        }

        $harga = $barang->harga_jual;

        Cart::add(array(
                'id' => $id,
                'name' => $barang->nama,
                'qty' => $qty,
                'price' => $harga,
                'options'=> array('foto' => $barang->foto, 'berat' => $barang->berat, 'kode_barang' => $barang->kode_barang)
            ));

        Session::flash('messageSuccess','Tambah Barang Berhasil');
        return redirect('/administrator/penjualan-data-barang');
    }

    public function postUpdatePenjualanBarang(Request $request){
        $id = $request->input('id');
        $rowid = $request->input('rowid');
        $qty = $request->input('qty');     

        if(!isset($rowid) || !isset($qty)){
            Session::flash('messageError','Barang tidak valid');
            return redirect('/administrator/penjualan-data-barang');
        }   

        foreach ($rowid as $i => $r) {
            if($qty[$i] > Barang::where('id', $id[$i])->first()->persediaan){
                Session::flash('messageError','Jumlah barang tersedia tidak mencukupi');
                return redirect('/administrator/penjualan-data-barang');
            }
            if($qty[$i] <= 0){
                Session::flash('messageError','Minimal pembelian barang adalah 1');
                return redirect('/administrator/penjualan-data-barang');
            }

            Cart::update($r, ['qty' => $qty[$i]]);
        }

        Session::flash('messageSuccess', 'Update barang berhasil');
        return redirect('/administrator/penjualan-data-barang');
    }

    public function postPenjualanHapusBarang(Request $request){
        $rowId = $request->input('rowid');
        Cart::remove($rowId);
    }

    public function getPenjualanDataKonfirmasi(){        
        $data = Cart::content();
        $diskon = 0;
        if(Cart::count() >= 10){
            $diskon = (10/100) * Cart::subtotal('0','.','');
        }
        
        Session::put('kasir_diskon', $diskon);
        return view('admin.penjualan_kasir_konfirmasi')->withData($data)->withDiskon($diskon);
    }

    public function getPrintInvoiceKasir(){
        $data = Cart::content();
        $diskon = 0;
        if(Cart::count() >= 10){
            $diskon = (10/100) * Cart::subtotal('0','.','');
        }

        return view('admin.print-invoice-kasir')->withData($data);
    }

    public function postPenjualanFinal(Request $request){
        $data = new Pesanan;

        $data->no_pesanan = Session::get('kasir_no_pesanan');
        $data->nama_pelanggan = Session::get('kasir_nama_pelanggan');
        $data->no_telepon = Session::get('kasir_no_telepon');
        $data->kota = Session::get('kasir_kota');
        $data->alamat = Session::get('kasir_alamat');
        $data->jenis_transaksi = 'Offline';
        $data->potongan_harga = Session::get('kasir_diskon');
        $data->berat = $request->input('berat');
        $data->tarif_pengiriman = 0;
        $data->total_pembayaran = Cart::subtotal('0',',','');
        $data->tanggal_pesanan = Carbon::now();
        $data->tanggal_kirim = Carbon::now();
        $data->status = 'Dalam Pengiriman';
        $data->kode_pesanan = str_random('12');
        $data->save();

        foreach (Cart::content() as $c) {
            $item = new DetailPesanan;
            $item->pesanan_id = $data->id;
            $item->barang_id = $c->id;
            $item->harga = $c->price;
            $item->jumlah = $c->qty;
            $item->save();

            $barang = Barang::where('id', $c->id)->first();
            $barang->persediaan -= $c->qty;
            $barang->save();
            $this->addBarangKeluar($barang->id, $c->qty, $barang->persediaan);
        }

        $no_pesanan = $data->no_pesanan;
        $this->addLogAktivitas(auth('admin')->user()->id, 'Penjualan Offline : ' . $no_pesanan);

        Session::flash('messageSuccess', 'Penjualan kasir berhasil');
        return redirect('/administrator/penjualan-kasir');

    }

    public function getOngkosKirim(){
        $data = TarifPengiriman::orderBy('nama_kota', 'ASC')->get();

    	return view('admin.ongkos_kirim')->withData($data);
    }

    public function postTambahTarif(Request $request){
        $tarif = new TarifPengiriman;
        $tarif->nama_kota = $request->input('nama_kota');
        $tarif->jasa_pengiriman = $request->input('jasa_pengiriman');
        $tarif->tarif_pengiriman = $request->input('tarif_pengiriman');
        $tarif->estimasi_waktu = $request->input('estimasi_waktu');

        $tarif->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Tarif Pengiriman : ' . $tarif->nama_kota);
        Session::flash('messageSuccess', 'Tambah tarif pengiriman berhasil');
        return redirect('/administrator/ongkos-kirim');
    }

    public function postEditTarif(Request $request){
        $id = $request->input('id');

        $tarif = TarifPengiriman::where('id', $id)->first();


        if(!is_null($tarif)){
            $tarif->nama_kota = $request->input('nama_kota');
            $tarif->jasa_pengiriman = $request->input('jasa_pengiriman');
            $tarif->tarif_pengiriman = $request->input('tarif_pengiriman');
            $tarif->estimasi_waktu = $request->input('estimasi_waktu');

            $tarif->save();
            $this->addLogAktivitas(auth('admin')->user()->id, 'Edit Tarif Pengiriman : ' . $tarif->nama_kota);
        }else{
            Session::flash('messageError', 'Edit tarif pengiriman gagal');
            return redirect('/administrator/ongkos-kirim');   
        }

        Session::flash('messageSuccess', 'Edit tarif pengiriman berhasil');
        return redirect('/administrator/ongkos-kirim');
    }

    public function postHapusTarif(Request $request){
        $id = $request->input('rowid');

        $tarif = TarifPengiriman::where('id', $id)->first();

        if(!is_null($tarif)){
            $nama_kota = $tarif->nama_kota;

            $tarif->delete();

            $this->addLogAktivitas(auth('admin')->user()->id, 'Hapus Tarif Pengiriman : ' . $nama_kota); 
        }else{
            Session::flash('messageError', 'Hapus tarif pengiriman gagal');
            return redirect('/administrator/ongkos-kirim');   
        }

        Session::flash('messageSuccess', 'Hapus tarif pengiriman berhasil');
        return redirect('/administrator/ongkos-kirim');   
    }

    public function getPelanggan(){
        $data = Pelanggan::orderby('nama','asc')->get();

    	return view('admin.pelanggan')->withData($data);
    }

    public function getAdmin(){
        $data = Admin::where('role', 'Administrator')->get();

    	return view('admin.admin')->withData($data);
    }

    public function postAdmin(Request $request){
        $data = new Admin;
        $data->nama = $request->input('nama');
        $data->password = Hash::make($request->input('password'));
        $data->email = $request->input('email');
        $data->alamat = $request->input('alamat');
        $data->no_telepon = $request->input('no_telepon');
        $data->foto = 'default.jpg';
        $data->role = 'Administrator';
        $data->save();

        $data->kode_admin = 'A'.sprintf('%05d', $data->id);
        $data->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Administrator : ' . $data->kode_admin);

        Session::flash('messageSuccess', 'Tambah admin berhasil');
        return redirect('/administrator/admin');
    }

    public function postEditAdmin(Request $request){
        $data = Admin::where('id', $request->input('id'))->firstOrFail();
        $data->nama = $request->input('nama');
        $data->alamat = $request->input('alamat');
        $data->no_telepon = $request->input('no_telepon');
        $data->foto = 'default.jpg';
        $data->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Edit Data Administrator : ' . $data->kode_admin); 

        Session::flash('messageSuccess', 'Edit admin berhasil');
        return redirect('/administrator/admin');
    }

    public function postHapusAdmin(Request $request){
        $id = $request->input('rowid');

        $data = Admin::where('id', $id)->first();

        if($data != null){
            $data->delete(); 
            $this->addLogAktivitas(auth('admin')->user()->id, 'Hapus Administrator : ' . $kode_admin);    
        }else{
            Session::flash('messageError', 'Data tidak valid');
            return redirect('/administrator/admin');    
        }
        Session::flash('messageSuccess', 'Hapus Administrator berhasil');
        return redirect('/administrator/admin');
    }

    public function postPasswordAdmin(Request $request){
        $id = $request->input('id');
        $data = Admin::where('id', $id)->first();

        if($data != null){
            $data->password = Hash::make($request->input('password'));
            $data->save();
            $this->addLogAktivitas(auth('admin')->user()->id, 'Reset Password Administrator : ' . $data->kode_admin); 
        }else{
            Session::flash('messageError', 'Data tidak valid');
            return redirect('/administrator/admin');
        }

        Session::flash('messageSuccess', 'Reset Password berhasil');
        return redirect('/administrator/admin');
    }

    public function postPasswordKasir(Request $request){
        $id = $request->input('id');
        $data = Admin::where('id', $id)->first();

        if($data != null){
            $data->password = Hash::make($request->input('password'));
            $data->save();
            $this->addLogAktivitas(auth('admin')->user()->id, 'Reset Password Kasir : ' . $data->kode_admin); 
        }else{
            Session::flash('messageError', 'Data tidak valid');
            return redirect('/administrator/kasir');
        }

        Session::flash('messageSuccess', 'Reset Password berhasil');
        return redirect('/administrator/kasir');
    }



    public function getKasir(){
        $data = Admin::where('role', 'Kasir')->get();
        
    	return view('admin.kasir')->withData($data);
    }

    public function postKasir(Request $request){
        $data = new Admin;
        $data->nama = $request->input('nama');
        $data->password = Hash::make($request->input('password'));
        $data->email = $request->input('email');
        $data->alamat = $request->input('alamat');
        $data->no_telepon = $request->input('no_telepon');
        $data->foto = 'default.jpg';
        $data->role = 'Kasir';
        $data->save();

        $data->kode_admin = 'K'.sprintf('%05d', $data->id);
        $data->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Tambah Kasir : ' . $data->kode_admin); 

        Session::flash('messageSuccess', 'Tambah kasir berhasil');
        return redirect('/administrator/kasir');

    }

    public function postEditKasir(Request $request){
        $data = Admin::where('id', $request->input('id'))->firstOrFail();
        $data->nama = $request->input('nama');
        $data->alamat = $request->input('alamat');
        $data->no_telepon = $request->input('no_telepon');
        $data->foto = 'default.jpg';
        $data->save();

        $this->addLogAktivitas(auth('admin')->user()->id, 'Edit Data Kasir : ' . $data->kode_admin); 

        Session::flash('messageSuccess', 'Edit admin kasir berhasil');
        return redirect('/administrator/kasir');
    }


    public function getLogAktivitas(){
        $data = LogAktivitas::with('administrator')->orderBy('created_at', 'DESC')->get();

        return view('admin.log_aktifitas')->withData($data);
    }

    public function getLaporan(){
    	return view('admin.laporan');
    }

    public function getPrintKoleksiBarang(){
        $data = Barang::select('barang.id', 'barang.nama', 'kategori.nama as nama_kategori', 'barang.berat', 'barang.ukuran', 'barang.berat', 'barang.harga_pokok', 'barang.harga_jual', 'barang.persediaan', 'barang.foto', 'barang.kode_barang')
                ->join('kategori','kategori.id','=','barang.kategori_id')
                ->orderby('kategori.nama', 'asc')
                ->orderby('barang.id', 'asc')
                ->get();

        return view('admin.print-koleksi-barang')->withData($data);
    }

    public function postLaporan(Request $request){
        $data = null;
        $jenis_laporan = $request->input('jenis_laporan');
        $jenis_penjualan = $request->input('jenis_penjualan');
        $jenis_kategori = $request->input('kategori');
        $tahun = $request->input('tahun');
        $bulan = $request->input('bulan');
        $bulan2 = $request->input('bulan2');
        if($bulan == null){
            $bulan = '01';
        }
        switch ($jenis_laporan) {
            case 'koleksi_barang':
                $data = $this->generateLaporanKoleksiBarang();
                $judul = 'Katalog Barang';
                return view('admin.laporan_koleksi_barang')->withData($data)->withJudul($judul)->withForm($request->all());
                break;
            case 'persediaan_barang':
                
                $data = $this->generateLaporanPersediaanBarang($tahun, $bulan);
                
                $judul = 'Laporan Persediaan Barang Tahun '.$tahun.' Bulan '.$bulan;
                return view('admin.laporan_persediaan_barang')->withData($data)->withJudul($judul)->withForm($request->all());
                break;
            case 'penjualan':
                $query_jenis_penjualan = '';
                $judul = 'Laporan Penjualan';
                switch ($jenis_penjualan) {
                    case 'semua':
                        $query_jenis_penjualan = 'Semua';
                        break;
                    case 'online':
                        $query_jenis_penjualan = 'Online';
                        break;
                    case 'offline':
                        $query_jenis_penjualan = 'Offline';
                        break;
                }
                switch ($jenis_kategori) {
                    case 'tahunan':  
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanPenjualanTahunan($tahun);
                            return view('admin.laporan_tahunan_total')->withData($data)->withJudul($judul)->withForm($request->all());
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanPenjualanTahunanTransaksi($tahun, $query_jenis_penjualan);
                            return view('admin.laporan_tahunan')->withData($data)->withJudul($judul)->withForm($request->all());
                        }
                        break;
                    case 'bulanan':
                        
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            if($bulan2 == null)
                                $bulan2 = $bulan;
                            $data = $this->generateLaporanPenjualanBulanan($tahun, $bulan, $bulan2);
                            return view('admin.laporan_bulanan_total')->withData($data)->withJudul($judul)->withForm($request->all());
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            if($bulan2 == null)
                                $bulan2 = $bulan;
                            $data = $this->generateLaporanPenjualanBulananTransaksi($tahun, $bulan, $bulan2, $query_jenis_penjualan);
                            return view('admin.laporan_bulanan')->withData($data)->withJudul($judul)->withForm($request->all());
                        }
                        break;
                    case 'harian':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanPenjualanHarian($tahun, $bulan);
                            return view('admin.laporan_harian_total')->withData($data)->withJudul($judul)->withForm($request->all());
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanPenjualanHarianTransaksi($tahun, $bulan, $query_jenis_penjualan);

                            return view('admin.laporan_harian')->withData($data)->withJudul($judul)->withForm($request->all());
                        }
                        break;
                }
                break;
            case 'best_seller':
                $query_jenis_penjualan = '';
                $judul = 'Laporan Best Seller';
                switch ($jenis_penjualan) {
                    case 'semua':
                        $query_jenis_penjualan = 'Semua';
                        break;
                    case 'online':
                        $query_jenis_penjualan = 'Online';
                        break;
                    case 'offline':
                        $query_jenis_penjualan = 'Offline';
                        break;
                }
                switch ($jenis_kategori) {
                    case 'tahunan':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanBestSellerTahunan($tahun);

                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanBestSellerTahunanTransaksi($tahun, $query_jenis_penjualan);
                        }
                        break;
                    case 'bulanan':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerBulanan($tahun, $bulan, $bulan);
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerBulananTransaksi($tahun, $bulan, $bulan, $query_jenis_penjualan);
                        }
                        break;
                    case 'harian':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerHarian($tahun, $bulan, $bulan);
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerHarianTransaksi($tahun, $bulan, $bulan, $query_jenis_penjualan);
                        }
                        break;
                }
                return view('admin.laporan_best_seller_barang')->withData($data)->withJudul($judul)->withForm($request->all());
                break;

            return redirect('/administrator/laporan');
        }

        return $data;
    }

    public function postPrintLaporan(Request $request){
        $data = null;
        $jenis_laporan = $request->input('jenis_laporan');
        $jenis_penjualan = $request->input('jenis_penjualan');
        $jenis_kategori = $request->input('kategori');
        $tahun = $request->input('tahun');
        $bulan = $request->input('bulan');
        $bulan2 = $request->input('bulan2');

        switch ($jenis_laporan) {
            case 'persediaan_barang':
                $data = $this->generateLaporanPersediaanBarang($tahun, $bulan);
                $judul = 'Laporan Persediaan Barang Tahun '.$tahun.' Bulan '.$bulan;
                
                return view('admin.print-persediaan-barang')->withData($data)->withJudul($judul);
                break;
            case 'penjualan':
                $query_jenis_penjualan = '';
                $judul = 'Laporan Penjualan';
                switch ($jenis_penjualan) {
                    case 'semua':
                        $query_jenis_penjualan = 'Semua';
                        break;
                    case 'online':
                        $query_jenis_penjualan = 'Online';
                        break;
                    case 'offline':
                        $query_jenis_penjualan = 'Offline';
                        break;
                }
                switch ($jenis_kategori) {
                    case 'tahunan':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanPenjualanTahunan($tahun);
                            return view('admin.print-tahunan-total')->withData($data)->withJudul($judul)->withForm($request->all());
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanPenjualanTahunanTransaksi($tahun, $query_jenis_penjualan);
                            return view('admin.print-tahunan')->withData($data)->withJudul($judul)->withForm($request->all());
                        }
                        break;
                    case 'bulanan':
                        
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            if($bulan2 == null)
                                $bulan2 = $bulan;
                            $data = $this->generateLaporanPenjualanBulanan($tahun, $bulan, $bulan2);
                            return view('admin.print-bulanan-total')->withData($data)->withJudul($judul)->withForm($request->all());
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            if($bulan2 == null)
                                $bulan2 = $bulan;
                            $data = $this->generateLaporanPenjualanBulananTransaksi($tahun, $bulan, $bulan2, $query_jenis_penjualan);
                            return view('admin.print-bulanan')->withData($data)->withJudul($judul)->withForm($request->all());
                        }
                        break;
                    case 'harian':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanPenjualanHarian($tahun, $bulan);
                            return view('admin.print-harian-total')->withData($data)->withJudul($judul)->withForm($request->all());
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanPenjualanHarianTransaksi($tahun, $bulan, $query_jenis_penjualan);
                            return view('admin.print-harian')->withData($data)->withJudul($judul)->withForm($request->all());
                        }
                        break;
                }
                break;
            case 'best_seller':
                $query_jenis_penjualan = '';
                $judul = 'Laporan Best Seller';
                switch ($jenis_penjualan) {
                    case 'semua':
                        $query_jenis_penjualan = 'Semua';
                        break;
                    case 'online':
                        $query_jenis_penjualan = 'Online';
                        break;
                    case 'offline':
                        $query_jenis_penjualan = 'Offline';
                        break;
                }
                switch ($jenis_kategori) {
                    case 'tahunan':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanBestSellerTahunan($tahun);

                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Tahunan';
                            $judul .= ' ('.$tahun.')';
                            $data = $this->generateLaporanBestSellerTahunanTransaksi($tahun, $query_jenis_penjualan);
                        }
                        break;
                    case 'bulanan':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerBulanan($tahun, $bulan, $bulan);
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Bulanan';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerBulananTransaksi($tahun, $bulan, $bulan, $query_jenis_penjualan);
                        }
                        break;
                    case 'harian':
                        if($query_jenis_penjualan == 'Semua'){
                            $judul .= ' Total Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerHarian($tahun, $bulan, $bulan);
                        }else{
                            $judul .= ' '.$query_jenis_penjualan.' Harian';
                            $monthName   = Carbon::createFromFormat('!m', $bulan)->format('F');
                            $judul .= ' ('.$monthName.' '.$tahun.')';
                            $data = $this->generateLaporanBestSellerHarianTransaksi($tahun, $bulan, $bulan, $query_jenis_penjualan);
                        }
                        break;
                }
                return view('admin.print-best-seller')->withData($data)->withJudul($judul)->withForm($request->all());
                break;

            return redirect('/administrator/laporan');
        }

        return $data;
    }

    public function generateLaporanKoleksiBarang(){
        $data = Barang::select('barang.id', 'barang.nama', 'kategori.nama as nama_kategori', 'barang.berat', 'barang.ukuran', 'barang.berat', 'barang.harga_pokok', 'barang.harga_jual', 'barang.persediaan', 'barang.foto', 'barang.kode_barang')
                ->join('kategori','kategori.id','=','barang.kategori_id')
                ->orderby('kategori.nama', 'asc')
                ->orderby('barang.id', 'asc')
                ->get();

        return $data;
    }

    public function generateLaporanPersediaanBarang($tahun,$bulan){
        /*$data = Barang::select('barang.id', 'barang.nama', 'kategori.nama as nama_kategori', 'barang.berat', 'barang.ukuran', 'barang.berat', 'barang.harga_pokok', 'barang.harga_jual', 'barang.persediaan', 'barang.kode_barang')
                ->join('kategori','kategori.id','=','barang.kategori_id')
                ->orderby('kategori.nama', 'asc')
                ->orderby('barang.id', 'asc')
                ->get();*/
        // $data = LaporanBarang::select('barang.id', 'barang.nama', 'kategori.nama as nama_kategori', 'barang.berat', 'barang.ukuran', 'barang.harga_pokok', 'barang.harga_jual', DB::raw("SUM(laporan_barang.keluar) as jumlah_keluar"), DB::raw("SUM(laporan_barang.masuk) as jumlah_masuk"), 'laporan_barang.persediaan')
        //         ->join('barang', 'barang.id', '=', 'laporan_barang.barang_id')
        //         ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
        //         ->groupBy('barang.id')
        //         ->where(function($query) use ($tahun, $bulan){
        //             $query->where('laporan_barang.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
        //             ->where('laporan_barang.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
        //         })->get();

        $data = DB::table("laporan_barang AS a")
                    ->select('barang.kode_barang','barang.id', 'barang.nama', 'kategori.nama as nama_kategori', 'barang.berat', 'barang.ukuran', 'barang.harga_pokok', 'barang.harga_jual', 'b.jumlah_masuk','b.jumlah_keluar', 'a.persediaan')
                    ->join(DB::raw("(SELECT barang_id, MAX(created_at) AS latest, SUM(keluar) as jumlah_keluar, SUM(masuk) as jumlah_masuk FROM laporan_barang WHERE created_at >= '".$tahun."-".$bulan."-01 00:00:00' AND created_at <= '".$tahun."-".$bulan."-31 23:59:59' GROUP BY barang_id ) AS b"), function($join){
                        $join->on('a.barang_id','=','b.barang_id')
                             ->on('a.created_at','=','b.latest');
                    })
                    ->join('barang', 'barang.id', '=', 'a.barang_id')
                    ->join('kategori', 'kategori.id', '=', 'barang.kategori_id')
                    ->get();

        return $data;
    }

    public function generateLaporanPenjualanTahunan($tahun){
        $data = DetailPesanan::select(DB::raw('year(detail_pesanan.created_at) as Tahun'), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Online' THEN detail_pesanan.jumlah ELSE 0 END) as Terjual_Online"), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Online' THEN detail_pesanan.jumlah*detail_pesanan.harga ELSE 0 END) as Total_Harga_Online"),DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Offline' THEN detail_pesanan.jumlah ELSE 0 END) as Terjual_Offline"), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Offline' THEN detail_pesanan.jumlah*detail_pesanan.harga ELSE 0 END) as Total_Harga_Offline"), 
            DB::raw("sum(detail_pesanan.jumlah) as Total_Terjual"), DB::raw("sum(detail_pesanan.jumlah*detail_pesanan.harga) as Total_Harga"))
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('pesanan', 'pesanan.id', '=', 'detail_pesanan.pesanan_id')
            ->groupBy(DB::raw('year(detail_pesanan.created_at)'))
            ->where(function($query) use ($tahun){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-01-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-12-31 23:59:59');
            })->get();

        return $data;
    }

    public function generateLaporanPenjualanTahunanTransaksi($tahun, $jenis_transaksi){
        $data = DetailPesanan::select(DB::raw('year(detail_pesanan.created_at) as Tahun'), DB::raw('sum(detail_pesanan.jumlah) as terjual'), DB::raw('sum(detail_pesanan.harga*detail_pesanan.jumlah) as Harga_Jual'), DB::raw('sum(barang.harga_pokok*detail_pesanan.jumlah) as Harga_Pokok'), DB::raw('sum(detail_pesanan.harga*detail_pesanan.jumlah) - sum(barang.harga_pokok*detail_pesanan.jumlah) as Laba'))
            ->join('pesanan', 'pesanan.id', '=', 'detail_pesanan.pesanan_id')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->groupBy(DB::raw('year(detail_pesanan.created_at)'))
            ->where(function($query) use ($tahun, $jenis_transaksi){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-01-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-12-31 23:59:59')
                ->where('pesanan.jenis_transaksi','=',$jenis_transaksi);
            })->get();

        return $data;
    }

    //UNCLEAR
    public function generateLaporanPenjualanBulanan($tahun, $bulan1, $bulan2){
        $data = DetailPesanan::select(DB::raw('monthname(detail_pesanan.created_at) as Bulan'),DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Online' THEN detail_pesanan.jumlah ELSE 0 END) as Terjual_Online"), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Online' THEN detail_pesanan.jumlah*detail_pesanan.harga ELSE 0 END) as Total_Harga_Online"),DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Offline' THEN detail_pesanan.jumlah ELSE 0 END) as Terjual_Offline"), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Offline' THEN detail_pesanan.jumlah*detail_pesanan.harga ELSE 0 END) as Total_Harga_Offline"), 
            DB::raw("sum(detail_pesanan.jumlah) as Total_Terjual"), DB::raw("sum(detail_pesanan.jumlah*detail_pesanan.harga) as Total_Harga"))
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('pesanan', 'pesanan.id', '=', 'detail_pesanan.pesanan_id')
            // ->join(DB::raw(""), function($join){
            //             $join->on('a.barang_id','=','b.barang_id');
            //         })
            // ->join(DB::raw("
            //         select MONTH(CONCAT('2011-',months,'-01')) as date
            //         from (select 1 months union select 2 union select 3 union select 4 union select 5 union select 6 union
            //         select 7 months union select 8 union select 9 union select 10 union select 11 union select 12) as a1
            //         WHERE months >= 10 and months <= 12
            //         ) as generate"), 
            //     function($join){    
            //         $join->on('generate.date', '=' ,'month(detail_pesanan.created_at)');
            //     })
            ->groupBy(DB::raw('month(detail_pesanan.created_at)'))
            ->where(function($query) use ($tahun, $bulan1, $bulan2){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan1.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan2.'-31 23:59:59');
            })->get();

        return $data;
    }

    public function generateLaporanPenjualanBulananTransaksi($tahun, $bulan1, $bulan2, $jenis_transaksi){
        $data = DetailPesanan::select(DB::raw('monthname(detail_pesanan.created_at) as Bulan'), DB::raw('sum(detail_pesanan.jumlah) as terjual'), DB::raw('sum(detail_pesanan.harga*detail_pesanan.jumlah) as Harga_Jual'), DB::raw('sum(barang.harga_pokok*detail_pesanan.jumlah) as Harga_Pokok'), DB::raw('sum(detail_pesanan.harga*detail_pesanan.jumlah) - sum(barang.harga_pokok*detail_pesanan.jumlah) as Laba'))
            ->join('pesanan', 'pesanan.id', '=', 'detail_pesanan.pesanan_id')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->groupBy(DB::raw('month(detail_pesanan.created_at)'))
            ->where(function($query) use ($tahun, $jenis_transaksi, $bulan1, $bulan2){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan1.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan2.'-31 23:59:59')
                ->where('pesanan.jenis_transaksi','=',$jenis_transaksi);
            })->get();

        return $data;
    }

    public function generateLaporanPenjualanHarian($tahun, $bulan){
        $data = DetailPesanan::select(DB::raw('date(detail_pesanan.created_at) as Tanggal'), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Online' THEN detail_pesanan.jumlah ELSE 0 END) as Terjual_Online"), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Online' THEN detail_pesanan.jumlah*detail_pesanan.harga ELSE 0 END) as Total_Harga_Online"),DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Offline' THEN detail_pesanan.jumlah ELSE 0 END) as Terjual_Offline"), DB::raw("sum(CASE pesanan.jenis_transaksi WHEN 'Offline' THEN detail_pesanan.jumlah*detail_pesanan.harga ELSE 0 END) as Total_Harga_Offline"), 
            DB::raw("sum(detail_pesanan.jumlah) as Total_Terjual"), DB::raw("sum(detail_pesanan.jumlah*detail_pesanan.harga) as Total_Harga"))
            ->join('pesanan', 'pesanan.id', '=', 'detail_pesanan.pesanan_id')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->groupBy(DB::raw('date(detail_pesanan.created_at)'))
            ->where(function($query) use ($tahun, $bulan){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
            })->get();
        return $data;
    }

    public function generateLaporanPenjualanHarianTransaksi($tahun, $bulan, $jenis_transaksi){
        $data = DetailPesanan::select(DB::raw('date(detail_pesanan.created_at) as Tanggal'), DB::raw('sum(detail_pesanan.jumlah) as terjual'), DB::raw('sum(detail_pesanan.harga*detail_pesanan.jumlah) as Harga_Jual'), DB::raw('sum(barang.harga_pokok*detail_pesanan.jumlah) as Harga_Pokok'), DB::raw('sum(detail_pesanan.harga*detail_pesanan.jumlah) - sum(barang.harga_pokok*detail_pesanan.jumlah) as Laba'))
            ->join('pesanan', 'pesanan.id', '=', 'detail_pesanan.pesanan_id')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->groupBy(DB::raw('date(detail_pesanan.created_at)'))
            ->where(function($query) use ($tahun, $jenis_transaksi, $bulan){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                ->where('pesanan.jenis_transaksi','=',$jenis_transaksi);
            })->get();

        return $data;
    }

    public function generateLaporanBestSellerTahunan($tahun){
        $data = DetailPesanan::select('barang.id as Id_Barang', 'barang.id as Kode_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'))
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-01-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-12-31 23:59:59');
            })->get();

        return $data;
    }

    public function generateLaporanBestSellerTahunanTransaksi($tahun, $jenis_transaksi){
        $data = DetailPesanan::select('barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->join('pesanan', 'pesanan.id','=', 'detail_pesanan.pesanan_id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun, $jenis_transaksi){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-01-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-12-31 23:59:59')
                ->where('jenis_transaksi','=',$jenis_transaksi);
            })->get();

        return $data;
    }

    public function generateLaporanBestSellerBulanan($tahun, $bulan1, $bulan2){
        $data = DetailPesanan::select('barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun, $bulan1, $bulan2){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan1.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan2.'-31 23:59:59');
            })->get();

        return $data;
    }

    public function generateLaporanBestSellerBulananTransaksi($tahun, $bulan1, $bulan2, $jenis_transaksi){
        $data = DetailPesanan::select('barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->join('pesanan', 'pesanan.id','=', 'detail_pesanan.pesanan_id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun, $bulan1, $bulan2, $jenis_transaksi){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan1.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan2.'-31 23:59:59')
                ->where('jenis_transaksi','=',$jenis_transaksi);
            })->get();

        return $data;
    }

    public function generateLaporanBestSellerHarian($tahun, $bulan){
        $data = DetailPesanan::select('barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang')
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun, $bulan){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59');
            })->get();

        return $data;
    }

    public function generateLaporanBestSellerHarianTransaksi($tahun, $bulan, $jenis_transaksi){
        $data = DetailPesanan::select('barang.id as Id_Barang' ,'barang.nama as Nama_Barang', 'kategori.nama as Kategori', DB::raw('sum(detail_pesanan.jumlah) as Jumlah_Terbeli'), 'barang.kode_barang as Kode_Barang' )
            ->join('barang','detail_pesanan.barang_id','=','barang.id')
            ->join('kategori', 'barang.kategori_id','=','kategori.id')
            ->join('pesanan', 'pesanan.id','=', 'detail_pesanan.pesanan_id')
            ->groupBy('barang.id')
            ->orderBy(DB::raw('Jumlah_Terbeli'), 'desc')
            ->where(function($query) use ($tahun, $bulan, $jenis_transaksi){
                $query->where('detail_pesanan.created_at', '>=',$tahun.'-'.$bulan.'-1 00:00:00')
                ->where('detail_pesanan.created_at', '<=',$tahun.'-'.$bulan.'-31 23:59:59')
                ->where('jenis_transaksi','=',$jenis_transaksi);
            })->get();

        return $data;
    }

    public function addBarangMasuk($id, $jumlah, $persediaan){
        $data = new LaporanBarang;
        $data->barang_id = $id;
        $data->masuk = $jumlah;
        $data->persediaan = $persediaan;
        $data->save();
    }

    public function addBarangKeluar($id, $jumlah, $persediaan){
        $data = new LaporanBarang;
        $data->barang_id = $id;
        $data->keluar = $jumlah;
        $data->persediaan = $persediaan;
        $data->save();
    }

    public function addLogAktivitas($id, $aktivitas){
        $data = new LogAktivitas;
        $data->administrator_id = $id;
        $data->aktivitas = $aktivitas;
        $data->save();
    }
}
