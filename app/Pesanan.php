<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model
{
    protected $table = 'pesanan';

    public function detail_pesanan(){
    	return $this->hasMany('App\DetailPesanan');
    }

    public function konfirmasi(){
    	return $this->hasMany('App\Konfirmasi', 'pesanan_id');
    }

    public function pelanggan(){
    	return $this->belongsTo('App\Pelanggan');
    }

    public function beban(){
    	return $this->hasOne('App\Beban');
    }

    public function getNotifAttribute(){
        return $this->hasMany('App\Konfirmasi', 'pesanan_id')->where('status_konfirmasi',0)->count();
    }

    public function pengiriman(){
        return $this->belongsTo('App\TarifPengiriman', 'jasa_pengiriman');
    }
}
