@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Detail Pesanan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li><a href="/administrator/pesanan">Pesanan</a></li>
        <li class="active">Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
          <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                  <h4>Detail Konfirmasi Pembayaran <button class="btn btn-sm btn-success pull-right  verifikasi">Verifikasi Pembayaran</button></h4>
                </div>
                  <div class="col-sm-8">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No Pesanan</th>
                        <td>{{$data->pesanan->no_pesanan}}</td>
                      </tr>
                      <tr>
                        <th>Nama Pengirim</th>
                        <td>{{$data->nama_pengirim}}</td>
                      </tr>
                      <tr>
                        <th>No Rekening</th>
                        <td>{{$data->no_rekening}}</td>
                      </tr>
                      <tr>
                        <th>Nominal</th>
                        <td>{{$data->nominal}}</td>
                      </tr>
                      <tr>
                        <th>Tanggal Transfer</th>
                        <td>{{$data->tanggal_transfer}}</td>
                      </tr>
                      <tr>
                        <th>Status Konfirmasi</th>
                        <td><b>{{$data->status_konfirmasi}}</b></td>
                      </tr>
                      <tr>
                        <th>Bukti Transfer</th>
                        <td></td>
                      </tr>
                    </thead>
                  </table>
                      
                  </div>
                    
                </div>
                <hr>
                <h4>Detail Barang </h4>
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Berat</th>
                    <th>Harga Satuan</th>
                    <th>Jumlah</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $total = 0; $jumlah = 0; $berat =0;?>
                  @foreach($data->detail_pesanan as $d)
                  <tr>
                    <td>{{ $c++ }}</td>
                    <td>{{ $d->barang->nama }}</td>
                    <td>{{ $d->barang->berat }} kg</td>
                    <?php $berat += $d->barang->berat; ?>
                    <td>Rp {{ number_format($d->harga, 0, ',', '.')}},-</td>
                    <?php $jumlah += $d->jumlah; ?>
                    <td>{{ $d->jumlah }}</td>
                    <?php $subtotal = $d->jumlah * $d->harga; ?>
                    <td>Rp {{ number_format($subtotal, 0, ',', '.')}},-</td>
                    <?php $total += $subtotal; ?>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="4" class="">Harga total</th>
                        <th> {{$jumlah}}</th>
                        <th>Rp {{ number_format($total, 0, ',', '.')}},-</th>
                    </tr>
                    <tr>
                        <th colspan="4" class="">Diskon</th>
                        @if($data->potongan_harga > 0)
                        <th>10%</th>
                        @else
                        <th>0%</th>
                        @endif
                        <th>Rp {{ number_format($data->potongan_harga, 0, ',', '.')}},-</th>
                    </tr>
                    <tr>
                        <th colspan="5" class="">Grand total</th>
                        <th>Rp {{ number_format(($total - $data->potongan_harga), 0, ',', '.')}},-</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            </div>
        </div>
        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
@stop