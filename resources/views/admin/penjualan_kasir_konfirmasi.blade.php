@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan Offline
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li><a href="/administrator/penjualan-kasir">Penjualan Offline</a></li>
        <li class="active">Konfirmasi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
          <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                  <h4>Detail Penjualan <a href="/administrator/print-invoice-kasir" target="_blank" class="btn btn-sm btn-primary pull-right">Cetak</a></h4>
                </div>
                  <div class="col-sm-8">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No Pesanan</th>
                        <td>{{Session::get('kasir_no_pesanan')}}</td>
                      </tr>
                      <tr>
                        <th>Nama</th>
                        <td>{{Session::get('kasir_nama_pelanggan')}}</td>
                      </tr>
                      <tr>
                        <th>No Telepon</th>
                        <td>{{Session::get('kasir_no_telepon')}}</td>
                      </tr>
                      <tr>
                        <th>Kota</th>
                        <td>{{Session::get('kasir_kota')}}</td>
                      </tr>
                      <tr>
                        <th>Alamat</th>
                        <td>{{Session::get('kasir_alamat')}}</td>
                      </tr>
                    </thead>
                  </table>
                      
                  </div>
                    
                </div>
                <hr>
                <h4>Detail Barang </h4>
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Berat</th>
                    <th>Harga Satuan</th>
                    <th>Jumlah</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $total = 0;?>
                  <?php $berat = 0; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c++ }}</td>
                    <td>{{ $d->options->kode_barang }}</td>
                    <td>{{ $d->name }}</td>
                    <td>{{ $d->options->berat }} kg</td>
                    <?php $berat += $d->options->berat; ?>
                    <td>Rp {{ number_format($d->price, 0, ',', '.')}},-</td>
                    <td>{{ $d->qty }}</td>
                    <td>Rp {{ number_format($d->subtotal, 0, ',', '.')}},-</td>
                    <?php $total += $d->subtotal; ?>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="5" class="">Total belanja</th>
                        <th> {{Cart::count()}}</th>
                        <th>Rp {{ number_format($total, 0, ',', '.')}},-</th>
                    </tr>
                    <tr>
                        <th colspan="5" class="">Diskon</th>
                        @if($diskon > 0)
                        <th>10%</th>
                        @else
                        <th>0%</th>
                        @endif
                        <th>Rp {{ number_format($diskon, 0, ',', '.')}},-</th>
                    </tr>
                    <tr>
                        <th colspan="6" class="">Grand total</th>
                        <th>Rp {{ number_format(($total-$diskon), 0, ',', '.')}},-</th>
                    </tr>
                </tfoot>
              </table>
              <div class="col-md-12">
                  <br>
                  <br>
                  <a href="/administrator/penjualan-data-barang" class="btn btn-sm btn-danger"> Batalkan Pembelian</a>
                  <form action="/administrator/penjualan-final" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="berat" value="{{$berat}}">
                    <button class="pull-right btn btn-sm btn-success"> Selesai </button>
                  </form>
              </div>
            </div>
            </div>
        </div>
        
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
@stop