@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kategori
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li class="activate">Kategori</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-cubes"></i>
              <h3 class="box-title">Daftar Kategori</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#inputKategori" id="tambah-kategori">Tambah Kategori <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Keterangan</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c = 1; ?>
                  @foreach($data as $k)
                  <tr>
                    <td>{{ $c++ }}</td>
                    <td>{{ $k->nama }}</td>
                    <td>{{ $k->keterangan }}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm edit-kategori" data-toggle='modal' data-target='#editKategori' data-nama='{{$k->nama}}' data-keterangan='{{$k->keterangan}}' data-id="{{$k->id}}"><i class="fa fa-pencil fa-fw"></i></button>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-sm delete-kategori" data-nama='{{$k->nama}}' data-keterangan='{{$k->keterangan}}' data-id="{{$k->id}}"><i class="fa fa-trash fa-fw"></i></button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->

     
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal Input Kategori -->
  <div class="modal fade" id="inputKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/tambah-kategori" class="form-horizontal" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Kategori</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tambah_kategori_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Keterangan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="tambah_kategori_keterangan" name='keterangan'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Input Kategori -->

  <!-- Modal Edit Kategori -->
  <div class="modal fade" id="editKategori" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/edit-kategori" class="form-horizontal" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Kategori</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="edit_kategori_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-2 control-label">Keterangan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="edit_kategori_keterangan" name='keterangan'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="" id="edit_kategori_id">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Edit Kategori -->

  @endsection
  @section('js')
    <script type="text/javascript">
      $('.edit-kategori').click(function() {
        $('#edit_kategori_nama').val($(this).data('nama'));
        $('#edit_kategori_keterangan').val($(this).data('keterangan'));
        $('#edit_kategori_id').val($(this).data('id'));
      });
      $('.delete-kategori').click(function() {
        var rowid = $(this).data('id');
        var kategori = $(this).data('nama');
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Kategori <strong>"+kategori+" </strong>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/hapus-kategori", {rowid: rowid, _token:_token})
            .done(function(result) {
              window.location.replace("/administrator/kategori/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });
    </script>
  @endsection