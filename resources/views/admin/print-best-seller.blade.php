<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bogimin Craft</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
        <div class="col-md-12">
          <b><span class="logo-mini pull-left"><b><img src="/images/bogimin-b.png" style="height: auto; width: 180px;"></b></span></b>
          <span class="pull-right">Bogimin Craft beralamat di Jalan Kasongan, <br>RT 02, DK 17, Bangunjiwo, Kasihan, <br>Bantul, Daerah Istimewa Yogyakarta.
          </span>
          <br>
          <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          
          <div class='box'>
            <div class="box-header">
              <i class="fa fa-bar-chart"></i>
              <b class="box-title">{{$judul}}</b>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Barang</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Jumlah Terbeli</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->Kode_Barang}}</td>
                    <td>{{ $d->Nama_Barang }}</td>
                    <td>{{ $d->Kategori }}</td>
                    <td>{{ $d->Jumlah_Terbeli }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach 
                </tbody>
              </table>
            </div>
          </div>
      </div>
      
      <!-- /.row -->
    </section>
    
  <script src="{{ asset('/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/admin/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},2000);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
