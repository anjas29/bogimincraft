@extends('layouts.admin_layout')
@section('header')
  <style type="text/css">
    .badge-notify{
       background:red;
       position:absolute;
       top: -8px;
       right: -6px;
    }
  </style>
@endsection
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
      </h1>
      <ol class="breadcrumb">
        <li class="activate"><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li>Pesanan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-shopping-bag"></i>
              <h3 class="box-title">Daftar Pesanan</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Pesanan</th>
                    <th>Nama Pelanggan</th>
                    <th>Total Pembayaran</th>
                    <th>Status</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($data as $i => $d)
                  <tr>
                    <td>{{++$i}}</td>
                    <td>{{$d->created_at->format('d F Y, H:i')}}</td>
                    <td>{{$d->no_pesanan}}</td>
                    <td>{{$d->pelanggan->nama}}</td>
                    <td>{{$d->total_pembayaran}}</td>
                    <td><strong style='color:#009688;'>{{$d->status}}</strong></td>
                    <td>
                      <div class="btn-group">
                          <a href="/administrator/detail-pesanan/{{$d->id}}" class="btn btn-success btn-sm"><i class="fa fa-eye fa-fw"></i></a>
                      </div>
                      <div class="btn-group">
                        @if($d->konfirmasi->count() == 0)
                          <button type="button" class="btn btn-primary btn-sm" disabled=""><i class="fa fa-money fa-fw"></i></button>
                        @else
                          @if($d->notif > 0)
                          <div class="btn-group" style="margin-right: 5px;">
                            <a href="/administrator/konfirmasi-pembayaran/{{$d->id}}" type="button" class="btn btn-primary btn-sm"><i class="fa fa-money fa-fw"></i></a>
                              <span class="badge badge-xs badge-notify" style="z-index:99;">{{$d->notif}}</span>
                          </div>
                          @else
                          <a href="/administrator/konfirmasi-pembayaran/{{$d->id}}" type="button" class="btn btn-primary btn-sm"><i class="fa fa-money fa-fw"></i></a>
                          @endif
                        @endif
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-sm delete-pesanan" data-id='{{$d->id}}' data-no_pesanan='{{$d->no_pesanan}}'><i class="fa fa-trash fa-fw"></i></button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });
    </script>
    <script type="text/javascript">
      $('.delete-pesanan').click(function() {
        var rowid = $(this).data('id');
        var no_pesanan = $(this).data('no_pesanan');
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Pesanan <strong>"+no_pesanan+" </strong>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/hapus-pesanan", {rowid: rowid, _token:_token})
            .done(function(result) {
              window.location.replace("/administrator/pesanan/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });
    </script>
  @endsection