@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan Offline
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"> Beranda</i></a></li>
        <li class="activate"><a href="/administrator/penjualan-kasir">Penjualan Offline</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-opencart"></i>
              <h3 class="box-title">Daftar Penjualan Offline</h3>
              <div class="box-tools pull-right">
                <a href="/administrator/penjualan-data-pembeli" class="btn btn-sm btn-primary">Tambah Penjualan <i class="fa fa-plus"></i></a>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Pesanan</th>
                    <th>Nama Pelanggan</th>
                    <th>Total Pembayaran</th>
                    <th>Status</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c = 1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{$c++}}</td>
                    <td>{{$d->tanggal_pesanan}}</td>
                    <td>{{$d->no_pesanan}}</td>
                    <td>{{$d->nama_pelanggan}}</td>
                    <td>Rp {{ number_format($d->total_pembayaran, 0, ',','.') }},-</td>
                    <td><strong style='color:#009688;'>Lunas</strong></td>
                    <td>
                      <div class="btn-group">
                        <a href="/administrator/detail-penjualan-kasir/{{$d->id}}" type="button" class="btn btn-primary btn-sm"><i class="fa fa-eye fa-fw"></i></a>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-danger btn-sm delete-pesanan" data-id='{{$d->id}}' data-no_pesanan='{{$d->no_pesanan}}'><i class="fa fa-trash fa-fw"></i></button>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });
    </script>
    <script type="text/javascript">
      $('.delete-pesanan').click(function() {
        var rowid = $(this).data('id');
        var no_pesanan = $(this).data('no_pesanan');
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Pesanan <strong>"+no_pesanan+" </strong>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/hapus-pesanan", {rowid: rowid, _token:_token})
            .done(function(result) {
              window.location.replace("/administrator/penjualan-kasir/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });
    </script> 
  @endsection