@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kasir
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li class="activate"><a href="/administrator/kasir"> Kasir</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-primary'>
            <div class="box-header">
              <i class="fa fa-balance-scale"></i>
              <h3 class="box-title">Daftar Kasir</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#inputAdmin">Tambah Kasir <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Kasir</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                    <th>Email</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c = 1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{$c++}}</td>
                    <th>{{$d->kode_admin}}</th>
                    <td>{{$d->nama}}</td>
                    <td>{{$d->alamat}}</td>
                    <td>{{$d->no_telepon}}</td>
                    <td>{{$d->email}}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary detail-admin" data-toggle='modal' data-target='#editAdmin' data-nama='{{$d->nama}}' data-id='{{$d->id}}' data-email='{{$d->email}}' data-alamat='{{$d->alamat}}' data-no_telepon='{{$d->no_telepon}}'><i class="fa fa-eye"></i></button>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-warning edit-password-admin" data-toggle='modal' data-target='#passwordAdmin' data-id='{{$d->id}}' data-email="{{$d->email}}"><i class="fa fa-key"></i></button>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-danger delete-admin" data-nama='{{$d->nama}}' data-rowid='{{$d->id}}' data-kode_admin="{{$d->kode_admin}}"><i class="fa fa-trash"></i></button>
                      </div>
                   </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="inputAdmin" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      
      <form action="" method="post" class='form-horizontal' role='form' data-toggle='validator'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Admin Kasir</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="input_nama" name='nama' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="input_email" name='email' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Alamat</label>
            <div class="col-sm-9">
              <textarea class="form-control " name="alamat" required></textarea>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">No Telepon</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="input_no_telp" name='no_telepon' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9">
              <input type="password" class="form-control" id="input_password" name='password' data-minlength='6' required>
              <div class="help-block with-errors">Minimal 6 karakter</div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{csrf_field()}}
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </div>
        </form>
        
      </div>
    </div>

    <div class="modal fade" id="editAdmin" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
      
      <form action="/administrator/edit-kasir" method="post" class='form-horizontal' role='form' data-toggle='validator'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Admin Kasir</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_nama" name='nama' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="edit_email" name='email' disabled>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Alamat</label>
            <div class="col-sm-9">
              <textarea class="form-control " id='edit_alamat' name="alamat" required></textarea>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">No Telepon</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_no_telp" name='no_telepon' required>
              <div class="help-block with-errors"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{csrf_field()}}
          <input type="hidden" name="id" id="edit_id">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </div>
        </form>
        
      </div>
    </div>

    <div class="modal fade" id="passwordAdmin" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
      <form action="/administrator/password-kasir" method="post" class='form-horizontal' role='form' data-toggle='validator'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Reset Password Kasir</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
              <input type="email" class="form-control" id="password_email" name='email' disabled>
              <div class="help-block with-errors"></div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Password Baru</label>
            <div class="col-sm-9">
              <input class="form-control" id="password_generated" name='password' disabled>
              <div class="help-block">Catat password baru</div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{csrf_field()}}
          <input type="hidden" name="id" id="password_id">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary submitPassword">Simpan</button>
        </div>
        </div>
        </form>
      </div>
    </div>
  @endsection
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });
      $('.detail-admin').click(function() {
        $('#edit_id').val($(this).data('id'));
        $('#edit_nama').val($(this).data('nama'));
        $('#edit_email').val($(this).data('email'));
        $('#edit_alamat').val($(this).data('alamat'));
        $('#edit_no_telp').val($(this).data('no_telepon'));
      });

      $('.delete-admin').click(function() {
        var rowid = $(this).data('rowid');
        var nama = $(this).data('nama');
        var kode_admin = $(this).data('kode_admin');
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Kasir <strong>"+nama+" ("+kode_admin+")"+" </strong>?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/hapus-admin", {rowid: rowid, _token:_token})
            .done(function(result) {
              window.location.replace("/administrator/kasir/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });

      $('.edit-password-admin').click(function() {
        $('#password_id').val($(this).data('id'));
        $('#password_email').val($(this).data('email'));
        $('#password_generated').val(makeid());
      });

      $('.submitPassword').click(function(){
          $('#password_generated').removeAttr('disabled');
      });

      function makeid(){
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

          for( var i=0; i < 6; i++ )
              text += possible.charAt(Math.floor(Math.random() * possible.length));

          return text;
      }
    </script>
  @endsection