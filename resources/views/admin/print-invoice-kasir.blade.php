<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bogimin Craft</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
        <div class="col-md-12">
          <b><span class="logo-mini pull-left"><b><img src="/images/bogimin-b.png" style="height: auto; width: 180px;"></b></span></b>
          <span class="pull-right">Bogimin Craft beralamat di Jalan Kasongan, <br>RT 02, DK 17, Bangunjiwo, Kasihan, <br>Bantul, Daerah Istimewa Yogyakarta.
          </span>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
          <div class="box-body">
                <div class="row">
                <div class="col-md-12">
                  <h2 style="margin-top: 0;"><b>Invoice</b></h2>
                  <h4> <b>Detail Penjualan</b></h4>
                </div>
                  <div class="col-sm-8">
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th>No Penjualan</th>
                        <td>{{Session::get('kasir_no_pesanan')}}</td>
                      </tr>
                      <tr>
                        <th>Nama</th>
                        <td>{{Session::get('kasir_nama_pelanggan')}}</td>
                      </tr>
                      <tr>
                        <th>No Telepon</th>
                        <td>{{Session::get('kasir_no_telepon')}}</td>
                      </tr>
                      <tr>
                        <th>Kota</th>
                        <td>{{Session::get('kasir_kota')}}</td>
                      </tr>
                      <tr>
                        <th>Alamat</th>
                        <td>{{Session::get('kasir_alamat')}}</td>
                      </tr>
                    </thead>
                  </table>
                      
                  </div>
                    
                </div>
                <hr>
                <h4><b>Detail Barang</b></h4>
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Nama Barang</th>
                    <th>Berat</th>
                    <th>Harga Satuan</th>
                    <th>Jumlah</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $total = 0;?>
                  <?php $berat = 0; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c++ }}</td>
                    <td>{{ $d->options->kode_barang }}</td>
                    <td>{{ $d->name }}</td>
                    <td>{{ $d->options->berat }} kg</td>
                    <?php $berat += $d->options->berat; ?>
                    <td>Rp {{ number_format($d->price, 0, ',', '.')}},-</td>
                    <td>{{ $d->qty }}</td>
                    <td>Rp {{ number_format($d->subtotal, 0, ',', '.')}},-</td>
                    <?php $total += $d->subtotal; ?>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="5" class="">Total belanja</th>
                        <th> {{Cart::count()}}</th>
                        <th>Rp {{ number_format($total, 0, ',', '.')}},-</th>
                    </tr>
                    <tr>
                        <th colspan="5" class="">Diskon</th>
                        @if(Session::get('kasir_diskon') > 0)
                        <th>10%</th>
                        @else
                        <th>0%</th>
                        @endif
                        <th>Rp {{ number_format(Session::get('kasir_diskon'), 0, ',', '.')}},-</th>
                    </tr>
                    <tr>
                        <th colspan="6" class="">Grand total</th>
                        <th>Rp {{ number_format(($total-Session::get('kasir_diskon')), 0, ',', '.')}},-</th>
                    </tr>
                </tfoot>
              </table>
            </div>
            </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    
  <script src="{{ asset('/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/admin/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},2000);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
