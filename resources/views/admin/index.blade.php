@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Beranda
      </h1>
      <ol class="breadcrumb">
        <li class="activate"><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
          <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-body">
                  <h2>Selamat Datang</h2>
                  <blockquote>
                  <p>
                    Hai Bogimin Craft, selamat datang di halaman <b>{{strtoupper(auth('admin')->user()->role)}}</b>.<br>
                    Silahkan klik menu pilihan yang berada disebelah kiri untuk mengelola aktivitas Anda.
                  </p>
                  </blockquote>
                </div>
            </div>
              
          </div>
      </div>
      <!-- /.row -->

      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection