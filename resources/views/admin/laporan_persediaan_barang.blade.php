@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Persediaan Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Laporan</li>
        <li class="active">Laporan Persediaan Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-bar-chart"></i>
              <b class="box-title">{{$judul}}</b>
              <div class="pull-right">
                  <div class="btn-group ">
                    <form action="/administrator/print-laporan" method="post" target="_blank">
                    
                      {{csrf_field()}}
                      <input type="hidden" name="jenis_laporan" value="{{$form['jenis_laporan']}}">
                      <input type="hidden" name="jenis_penjualan" value="{{$form['jenis_penjualan']}}">
                      <input type="hidden" name="kategori" value="{{$form['kategori']}}">
                      <input type="hidden" name="tahun" value="{{$form['tahun']}}">
                      <input type="hidden" name="bulan" value="{{$form['bulan']}}">
                      <button class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</button>
                    </form> 
                  </div>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Barang</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Ukuran</th>
                    <th>Berat (Kg)</th>
                    <th>Harga Pokok</th>
                    <th>Harga Jual</th>
                    <th>Persediaan</th>
                    <th>Terjual</th>
                    <th>Sisa</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->kode_barang}}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->nama_kategori }}</td>
                    <td>{{ $d->ukuran }}</td>
                    <td>{{ $d->berat }}</td>
                    <td>Rp {{ number_format($d->harga_pokok, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($d->harga_jual, 0, ',','.') }}</td>
                    <td>{{ $d->persediaan + $d->jumlah_keluar }}</td>
                    <td>{{ $d->jumlah_keluar }}</td>
                    <td>{{ $d->persediaan }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach 
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('.dataTable').dataTable();
    });
  </script>
@stop