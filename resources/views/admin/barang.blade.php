@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li class="activate">Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-primary'>
            <div class="box-header">
              <i class="fa fa-cubes"></i>
              <h3 class="box-title">Daftar Barang</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#inputBarang">Tambah Barang <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Barang</th>
                    <th>Kategori</th>
                    <th>Nama Barang</th>
                    <th>Harga</th>
                    <th>Harga Pokok</th>
                    <th>Persediaan</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                <?php $number = 1; ?>
                @foreach($data as $d)
                  <tr>
                    <td>{{$number++}}</td>
                    <th>{{ $d->kode_barang }}</th>
                    <td>{{ $d->kategori->nama}}</td>
                    <td>{{ $d->nama}}</td>
                    <td>Rp {{ number_format($d->harga_jual, 0, ',','.') }},-</td>
                    <td>Rp {{ number_format($d->harga_pokok, 0, ',', '.')}},-</td>
                    <td>{{ $d->persediaan }}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary edit-barang" data-toggle='modal' data-target='#editBarang' data-id='{{$d->id}}' data-nama='{{$d->nama}}' data-harga='{{$d->harga_jual}}' data-harga_pokok='{{$d->harga_pokok}}' data-kategori='{{$d->kategori->id}}' data-deskripsi='{{$d->deskripsi}}' data-ukuran1='{{$d->ukuran}}'  data-berat='{{$d->berat}}'><i class="fa fa-pencil"></i></button>
                      </div>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-success tambah-stok" data-toggle='modal' data-target='#tambahStok' data-id='{{$d->id}}' data-nama='{{$d->nama}}'><i class="fa fa-plus"></i></button>
                      </div>
                      <div class="btn-group">
                        <a class="delete btn btn-sm btn-danger" data-rowid="{{$d->id}}" data-kode_barang="{{$d->kode_barang}}" data-nama='{{$d->nama}}'><i class="fa fa-trash"></i></a>
                      </div>
                   </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal Input Barang -->
  <div class="modal fade" id="inputBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/tambah-barang" class="form-horizontal" method="post" enctype="multipart/form-data" data-toggle='validator'>
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Barang</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control input-barang" id="input_barang_nama" name='nama'>
              <div class="help-block with-errors">testing</div>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Kategori</label>
            <div class="col-sm-9">
              <select name="kategori_id" class="form-control">
                  <option>-- Pilih Kategori--</option>
                @foreach($kategori as $k)
                  <option value="{{$k->id}}">{{$k->nama}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-leftpull-left">Berat <br>(dalam kg)</label>
            <div class="col-sm-9">
              <input type="text" class="form-control input-barang" id="input_barang" name='berat'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Ukuran</label>
            <div class="col-sm-9">
              <select name="ukuran" class="form-control">
                  <option  value="small">Small</option>
                  <option  value="medium">Medium</option>
                  <option  value="high">High</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Persediaan</label>
            <div class="col-sm-9">
              <input type="number" class="form-control input-barang" name='stok'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Harga Pokok</label>
            <div class="col-sm-9">
              <input type="text" class="form-control input-barang"  name='harga_pokok'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Harga Jual</label>
            <div class="col-sm-9">
              <input type="text" class="form-control input-barang" name='harga'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Deskripsi</label>
            <div class="col-sm-9">
              <textarea class="form-control input-barang" name="deskripsi"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Gambar</label>
            <div class="col-sm-9">
              <input type="file" class="form-control" name='gambar'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Input Barang -->
  <!-- Modal Edit Barang -->
  <div class="modal fade" id="editBarang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/edit-barang" class="form-horizontal" method="post" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Barang</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_barang_nama" name='nama'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Kategori</label>
            <div class="col-sm-9">
              <select name="kategori_id" class="form-control" id="edit_barang_kategori">
                <option>-- Pilih Kategori--</option>
                @foreach($kategori as $k)
                  <option value="{{$k->id}}">{{$k->nama}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-leftpull-left">Berat</label>
            <div class="col-sm-9">
              <input type="text" class="form-control " id="edit_barang_berat" name='berat'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Ukuran</label>
            <div class="col-sm-9">
              <select name="ukuran" class="form-control">
                  <option  value="small">Small</option>
                  <option  value="medium">Medium</option>
                  <option  value="high">High</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Harga Pokok</label>
            <div class="col-sm-9">
              <input type="text" class="form-control " id="edit_barang_harga_pokok" name='harga_pokok'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Harga Jual</label>
            <div class="col-sm-9">
              <input type="text" class="form-control " id="edit_barang_harga" name='harga'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Deskripsi</label>
            <div class="col-sm-9">
              <textarea class="form-control " id="edit_barang_deskripsi" name="deskripsi"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 pull-left">Gambar</label>
            <div class="col-sm-9">
              <input type="file" class="form-control" name='gambar'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <input type="hidden" name="id" id="edit_barang_id">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Edit Barang -->
  <!-- Modal tambah Stok -->
  <div class="modal fade" id="tambahStok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/tambah-persediaan" class="form-horizontal" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Persediaan</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Nama</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="stok_nama" name='nama' disabled>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-3 control-label">Jumlah barang</label>
            <div class="col-sm-9">
              <input type="number" class="form-control" id="stok_jumlah" name='stok'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <input type="hidden" name="id" id="stok_id">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Tambah Stok -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });
      $('.edit-kategori').click(function() {
        $('#edit_kategori_nama').val($(this).data('nama'));
        $('#edit_kategori_keterangan').val($(this).data('keterangan'));
        $('#edit_kategori_id').val($(this).data('id'));
      });
      $('.tambah-stok').click(function() {
        $('#stok_id').val($(this).data('id'));
        $('#stok_nama').val($(this).data('nama'));
        $('#stok_jumlah').val(0);
      });

      $('.edit-barang').click(function() {
        $('#edit_barang_id').val($(this).data('id'));
        $('#edit_barang_nama').val($(this).data('nama'));
        $('#edit_barang_kategori').val($(this).data('kategori'));
        $('#edit_barang_ukuran1').val($(this).data('ukuran1'));
        $('#edit_barang_ukuran2').val($(this).data('ukuran2'));
        
        $('#edit_barang_harga').val($(this).data('harga'));
        $('#edit_barang_harga_pokok').val($(this).data('harga_pokok'));
        $('#edit_barang_berat').val($(this).data('berat'));
        $('#edit_barang_deskripsi').val($(this).data('deskripsi'));
      });

      $('.delete').click(function() {
        var rowid = $(this).data('rowid');
        var barang = $(this).data('nama');
        var kode_barang = $(this).data('kode_barang');
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Barang <strong>"+barang+" ("+kode_barang+")"+" </strong> ini?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/hapus-barang", {rowid: rowid, _token:_token})
            .done(function(result) {
              window.location.replace("/administrator/barang/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });
    </script>
  @endsection