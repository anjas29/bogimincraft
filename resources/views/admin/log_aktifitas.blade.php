@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Log Aktivitas
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li class="activate"><a href="/administrator/log-aktifitas"><i class="fa fa-tasks"></i> Log Aktivitas</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-primary'>
            <div class="box-header">
              <i class="fa fa-tasks"></i>
              <h3 class="box-title">Daftar Aktivitas</h3>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Kode Admin</th>
                    <th>Nama Administrator</th>
                    <th>Email</th>
                    <th>Aktifitas</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c =1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{$c++}}</td>
                    <th>{{$d->created_at->format('d F Y, H:i')}}</th>
                    <td>{{$d->administrator->kode_admin}}</td>
                    <td>{{$d->administrator->nama}}</td>
                    <td>{{$d->administrator->email}}</td>
                    <td>{{$d->aktivitas}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->

      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });
    </script>
  @endsection