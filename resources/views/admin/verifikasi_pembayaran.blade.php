@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
  {{ asset('/css/bootstrap-editable.css') }}
  <link href="//cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.css" type="text/css" rel="stylesheet" />
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Konfirmasi Pembayaran
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li><a href="/administrator/pesanan"></a></li>
        <li class="active">Konfirmasi Pembayaran</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-list"></i>
              <h3 class="box-title">Daftar Pembayaran Pesanan {{$data->no_pesanan}}</h3>
            </div>
            <div class="box-body">
              @if($data->konfirmasi != null)
              <table class="table table-striped table-bordered" id='dataTable'>
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal Pembayaran</th>
                    <th>Bank</th>
                    <th>Rekening atas nama</th>
                    <th>No Rekening</th>
                    <th>Nominal</th>
                    <th>Status Konfirmasi</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data->konfirmasi as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->tanggal_transfer }}</td>
                    <td>{{ $d->bank }}</td>
                    <td>{{ $d->nama_pengirim }}</td>
                    <td>{{ $d->no_rekening }}</td>
                    <td>Rp {{ number_format($d->nominal, 0, ',','.') }}</td>
                    <td>
                    @if($d->status_konfirmasi == 1)
                          <strong style='color:#303F9F;'>Terkonfirmasi</strong>
                    @else
                          <strong style='color:#607D8B'>Belum Terkonfirmasi</strong>
                    @endif
                    </td>
                    <td>
                      <div class="btn-group">
                        @if($d->status_konfirmasi != 1)
                          <a type='button' class='verifikasi btn btn-sm btn-success' data-id="{{$d->id}}" data-rowid="{{ $d->id }}"><i class="fa fa-check"></i></a>
                        @else
                          <a type='button' class='btn btn-sm'><i class="fa fa-check"></i></a>
                        @endif
                      </div>
                      <div class="btn-group">
                          <a href="#" data-featherlight="/images/bukti-pembayaran/{{$d->bukti}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                      </div>
                    </td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
              @else
                <div class="col-sm-12">
                  <h4>Tidak ada transaksi</h4>
                </div>
              @endif
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
<script src="{{ asset('/js/bootstrap-editable.min.js') }}"></script>
<script src="//cdn.rawgit.com/noelboss/featherlight/1.5.0/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
  $('.verifikasi').click(function() {
    var rowid = $(this).data('rowid');
    var id = $(this).data('id');
    var _token = '{{csrf_token()}}';

    bootbox.confirm("Konfirmasi Pembayaran ini?", function(result) {
      if (result) {
        toastr.options.timeOut = 0;
        toastr.options.extendedTimeOut = 0;
        toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang memproses...');
        toastr.options.timeOut = 5000;
        toastr.options.extendedTimeOut = 1000;
        $.post("/administrator/konfirmasi-pembayaran", {rowid: rowid, _token: _token})
        .done(function(result) {
          window.location.replace("/administrator/pesanan/");
        })
        .fail(function(result) {
          toastr.clear();
          toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
        });
      };
    }); 
  });
</script>
@stop