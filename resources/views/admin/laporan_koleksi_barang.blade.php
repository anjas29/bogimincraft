@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Katalog Barang
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Laporan</li>
        <li class="active">Katalog Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-bar-chart"></i>
              <b class="box-title">{{$judul}}</b>
              <div class="pull-right">
                  <div class="btn-group ">
                    <a href="/administrator/print-koleksi-barang"  target="_blank" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</a> 
                  </div>
                </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Id Barang</th>
                    <th>No Pesanan</th>
                    <th>Gambar</th>
                    <th>Nama</th>
                    <th>Kategori</th>
                    <th>Harga</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->kode_barang}}</td>
                    <td><img src="/images/barang/{{$d->foto}}" class="img img-responsive" style="width:80px; height: auto;"></td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ $d->nama_kategori }}</td>
                    <td>Rp {{ number_format($d->harga_jual, 0, ',','.') }}</td>
                  </tr>
                  <?php $c++; ?>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('.dataTable').dataTable();
    });
  </script>
  <script>
    $('.detail-barang').click(function() {
      $('#detail_nama').val($(this).data('nama'));
      $('#detail_harga').val($(this).data('harga'));
      $('#detail_stok').val($(this).data('stok'));
      $('#detail_id').val($(this).data('id'));
      $('#detail_keterangan').val($(this).data('keterangan'));
      $('#detailBarang').modal('show');
    });
    $('.detail-stok').click(function() {
      $('#stok_data').val($(this).data('stok'));
      $('#stok_id').val($(this).data('id'));
    });
    $('#tambah-kategori').click(function() {
      $('#tambah_kategori_nama').val("");
    });
    $('.edit-kategori').click(function() {
      $('#edit_kategori_nama').val($(this).data('nama'));
      $('#edit_kategori_id').val($(this).data('id'));
    });
  </script>
@stop