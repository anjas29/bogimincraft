@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tarif Pengiriman
      </h1>
      <ol class="breadcrumb">
        <li class=""><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li class="activate"><a href="/administrator/ongkos-kirim"> Tarif Pengiriman</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-primary'>
            <div class="box-header">
              <i class="fa fa-globe"></i>
              <h3 class="box-title">Daftar Tarif Pengiriman</h3>
              <div class="box-tools pull-right">
                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#inputTarif">Tambah Data <i class="fa fa-plus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Kota</th>
                    <th>Nama Jasa Pengirim</th>
                    <th>Tarif</th>
                    <th>Estimasi Waktu</th>
                    <th>Operasi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c = 1; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{$c++}}</td>
                    <th>{{$d->nama_kota}}</th>
                    <th>{{$d->jasa_pengiriman}}</th>
                    <td>Rp {{ number_format($d->tarif_pengiriman, 0, ',', '.')}},-</td>
                    <td>{{$d->estimasi_waktu}}</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-primary edit-tarif" data-toggle='modal' data-target='#editTarif' data-id='{{$d->id}}' data-nama_kota='{{$d->nama_kota}}' data-jasa_pengiriman='{{$d->jasa_pengiriman}}' data-tarif_pengiriman='{{$d->tarif_pengiriman}}' data-estimasi_waktu='{{$d->estimasi_waktu}}' data-id='{{$d->id}}'><i class="fa fa-pencil"></i></button>
                      </div>
                      <div class="btn-group">
                        <a class="delete btn btn-sm btn-danger" data-rowid="{{$d->id}}" data-jasa_pengiriman="{{$d->jasa_pengiriman}}" data-nama_kota='{{$d->nama_kota}}'><i class="fa fa-trash"></i></a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal Input Tarif Pengiriman -->
  <div class="modal fade" id="inputTarif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/tambah-tarif" class="form-horizontal" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Tarif Pengiriman</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Kota</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-tarif" id="tarif_kota" name='nama_kota'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Jasa Pengiriman</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-tarif"  name='jasa_pengiriman'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Tarif Pengiriman</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-tarif" name='tarif_pengiriman'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Estimasi Waktu (dalam hari)</label>
            <div class="col-sm-8">
              <input type="text" class="form-control input-tarif"  name='estimasi_waktu'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Input Tarif -->

  <!-- Modal Edit Tarif Pengiriman -->
  <div class="modal fade" id="editTarif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form action="/administrator/edit-tarif" class="form-horizontal" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Edit Tarif Pengiriman</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Kota</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="edit_tarif_kota" name='nama_kota'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Jasa Pengiriman</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="edit_tarif_jasa_pengiriman" name='jasa_pengiriman'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Tarif Pengiriman</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="edit_tarif_tarif_pengiriman" name='tarif_pengiriman'>
            </div>
          </div>
          <div class="form-group">
            <label for="nmprdk" class="col-sm-4 control-label">Estimasi Waktu (dalam hari)</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id='edit_tarif_estimasi_waktu' name='estimasi_waktu'>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <input type="hidden" name="id" id="edit_tarif_id">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End Modal Input Tarif -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $(document).ready(function() {
        $('.dataTable').dataTable();
      });
      $('#tambah-tarif').click(function() {
        $('.input-tarif').val('');
      });
      $('.edit-tarif').click(function(){
          $('#edit_tarif_id').val($(this).data('id'));
          $('#edit_tarif_kota').val($(this).data('nama_kota'));
          $('#edit_tarif_estimasi_waktu').val($(this).data('estimasi_waktu'));
          $('#edit_tarif_jasa_pengiriman').val($(this).data('jasa_pengiriman'));
          $('#edit_tarif_tarif_pengiriman').val($(this).data('tarif_pengiriman'));
      });
      $('.delete').click(function() {
        var rowid = $(this).data('rowid');
        var nama_kota = $(this).data('nama_kota');
        var jasa_pengiriman = $(this).data('jasa_pengiriman');
        var _token = '{{csrf_token()}}';

        bootbox.confirm("Hapus Tarif Pengiriman <strong>"+nama_kota+" ("+jasa_pengiriman+")"+" </strong> ini?", function(result) {
          if (result) {
            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
            toastr.options.timeOut = 5000;
            toastr.options.extendedTimeOut = 1000;
            $.post("/administrator/hapus-tarif", {rowid: rowid, _token:_token})
            .done(function(result) {
              window.location.replace("/administrator/ongkos-kirim/");
            })
            .fail(function(result) {
              toastr.clear();
              toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
            });
          };
        }); 
      });
    </script>
  @endsection