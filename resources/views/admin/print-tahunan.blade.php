<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bogimin Craft</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
        <div class="col-md-12">
          <b><span class="logo-mini pull-left"><b><img src="/images/bogimin-b.png" style="height: auto; width: 180px;"></b></span></b>
          <span class="pull-right">Bogimin Craft beralamat di Jalan Kasongan, <br>RT 02, DK 17, Bangunjiwo, Kasihan, <br>Bantul, Daerah Istimewa Yogyakarta.
          </span>
          <br>
          <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          
          <div class='box'>
            <div class="box-header">
              <b class="box-title">{{$judul}}</b>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tahun</th>
                    <th>Terjual</th>
                    <th>Harga Pokok</th>
                    <th>Harga Jual</th>
                    <th>Laba</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $totalJumlah =0; $totalHarga_Pokok =0; $totalHarga_Jual =0; $totalLaba =0; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->Tahun}}</td>
                    <td>{{ $d->terjual }}</td>
                    <?php $totalJumlah += $d->terjual; ?>

                    <td>Rp {{ number_format($d->Harga_Pokok, 0, ',','.') }}</td>
                    <?php $totalHarga_Pokok += $d->Harga_Pokok; ?>

                    <td>Rp {{ number_format($d->Harga_Jual, 0, ',','.') }}</td>
                    <?php $totalHarga_Jual += $d->Harga_Jual; ?>

                    <td>Rp {{ number_format($d->Laba, 0, ',','.') }}</td>
                    <?php $totalLaba += $d->Laba; ?>
                  </tr>
                  <?php $c++; ?>
                  @endforeach 
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">Total</td>
                    <td>{{ $totalJumlah }}</td>
                    <td>Rp {{ number_format($totalHarga_Pokok, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($totalHarga_Jual, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($totalLaba, 0, ',','.') }}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
      </div>

      <!-- /.row -->
    </section>
    
  <script src="{{ asset('/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/admin/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},2000);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
