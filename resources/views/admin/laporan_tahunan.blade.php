@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Penjualan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Laporan</li>
        <li class="active">Laporan Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-bar-chart"></i>
              <b class="box-title">{{$judul}}</b>
              <div class="pull-right">
                  <div class="btn-group ">
                    <form action="/administrator/print-laporan" method="post" target="_blank">
                    
                      {{csrf_field()}}
                      <input type="hidden" name="jenis_laporan" value="{{$form['jenis_laporan']}}">
                      <input type="hidden" name="jenis_penjualan" value="{{$form['jenis_penjualan']}}">
                      <input type="hidden" name="kategori" value="{{$form['kategori']}}">
                      <input type="hidden" name="tahun" value="{{$form['tahun']}}">
                      <input type="hidden" name="bulan" value="{{$form['bulan']}}">
                      <button class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</button>
                    </form> 
                  </div>
                </div>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tahun</th>
                    <th>Terjual</th>
                    <th>Harga Pokok</th>
                    <th>Harga Jual</th>
                    <th>Laba</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $totalJumlah =0; $totalHarga_Pokok =0; $totalHarga_Jual =0; $totalLaba =0; ?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->Tahun}}</td>
                    <td>{{ $d->terjual }}</td>
                    <?php $totalJumlah += $d->terjual; ?>

                    <td>Rp {{ number_format($d->Harga_Pokok, 0, ',','.') }}</td>
                    <?php $totalHarga_Pokok += $d->Harga_Pokok; ?>

                    <td>Rp {{ number_format($d->Harga_Jual, 0, ',','.') }}</td>
                    <?php $totalHarga_Jual += $d->Harga_Jual; ?>

                    <td>Rp {{ number_format($d->Laba, 0, ',','.') }}</td>
                    <?php $totalLaba += $d->Laba; ?>
                  </tr>
                  <?php $c++; ?>
                  @endforeach 
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">Total</td>
                    <td>{{ $totalJumlah }}</td>
                    <td>Rp {{ number_format($totalHarga_Pokok, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($totalHarga_Jual, 0, ',','.') }}</td>
                    <td>Rp {{ number_format($totalLaba, 0, ',','.') }}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')

  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>
  <script>
    $(document).ready(function() {
      $('.dataTable').dataTable();
    });
  </script>
@stop