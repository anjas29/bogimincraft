@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"></i> Beranda</a></li>
        <li class="activate"><a href="/administrator/laporan"><i class="fa fa-bar-chart"></i> Laporan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
            <div class="box-header">
            <i class="fa fa-bar-chart"></i>
              <h3 class="box-title">Laporan</h3>
            </div>
            <div class="box-body">
              <div class="row">
              <form action="/administrator/laporan" method="POST">
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Jenis Laporan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="jenis_laporan" id="jenis">
                        <option value="">-- Pilih Jenis Laporan --</option>
                        <option value="penjualan">Penjualan</option>
                        <option value="koleksi_barang">Katalog Barang</option>
                        <option value="persediaan_barang">Persediaan Barang</option>
                        <option value="best_seller">Best Seller</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Jenis Penjualan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="jenis_penjualan" id="penjualan" disabled="false">
                        <option value="">-- Pilih Jenis Penjualan --</option>
                        <option value="semua">Total</option>
                        <option value="online">Online</option>
                        <option value="offline">Offline</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Kategori Laporan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="kategori" id="kategori" disabled>
                        <option value="">-- Pilih Kategori Laporan --</option>
                        <option value="tahunan">Tahunan</option>
                        <option value="bulanan">Bulanan</option>
                        <option value="harian">Harian</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Bulan</label>
                    <div class="col-sm-9">
                      <select class="validate[required] form-control" name="bulan" id="bulan" disabled="false">
                        <option value="">-- Pilih Bulan --</option>
                        <option value="01">Januari</option>
                        <option value="02">Februari</option>
                        <option value="03">Maret</option>
                        <option value="04">April</option>
                        <option value="05">Mei</option>
                        <option value="06">Juni</option>
                        <option value="07">Juli</option>
                        <option value="08">Agustus</option>
                        <option value="09">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-md-12">
                    <label for="nmprdk" class="col-sm-3 control-label">Tahun</label>
                    <div class="col-sm-9">
                      <input type="text" class="validate[required] form-control" name="tahun" id="tahun" disabled="false">
                    </div>
                  </div>
                  <div class="col-md-12">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-sm pull-right" id="submit_it">Proses</button>
                  </div>
              </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->

      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection
  @section('js')
    <script type="text/javascript">
      $('#jenis').on('change', function() {
          var jenis = this.value;

          switch(jenis){
            case 'penjualan':
              $('#penjualan').removeAttr('disabled');
              $('#kategori').removeAttr('disabled');
              $('#bulan').attr('disabled', true);
              $('#tahun').attr('disabled', true);
              break;
            case 'best_seller':
              $('#penjualan').removeAttr('disabled');
              $('#kategori').removeAttr('disabled');
              $('#bulan').attr('disabled', true);
              $('#tahun').attr('disabled', true);
              break;
            case 'persediaan_barang':
              $('#penjualan').attr('disabled', true);
              $('#kategori').attr('disabled', true);
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'koleksi_barang':
              $('#penjualan').attr('disabled', true);
              $('#kategori').attr('disabled', true);
              $('#bulan').attr('disabled', true);
              $('#tahun').attr('disabled', true);
              break;
          }
      });
      $('#kategori').on('change', function() {
          var kategori = this.value;

          switch(kategori){
            case 'tahunan':
              $('#bulan').attr('disabled', true);
              $('#tahun').removeAttr('disabled');
              break;
            case 'bulanan':
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
            case 'harian':
              $('#tahun').removeAttr('disabled');
              $('#bulan').removeAttr('disabled');
              break;
          }
      });
      $('#submit_it').click(function() {
            $('.form-control').removeAttr("disabled");
        });
    </script>
  @endsection
