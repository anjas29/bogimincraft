<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bogimin Craft</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
        <div class="col-md-12">
          <b><span class="logo-mini pull-left"><b><img src="/images/bogimin-b.png" style="height: auto; width: 180px;"></b></span></b>
          <span class="pull-right">Bogimin Craft beralamat di Jalan Kasongan, <br>RT 02, DK 17, Bangunjiwo, Kasihan, <br>Bantul, Daerah Istimewa Yogyakarta.
          </span>
          <br>
          <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          
          <div class='box'>
            <div class="box-header">
              <b class="box-title">{{$judul}}</b>
            </div>
            <div class="box-body">
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr >
                    <th rowspan="2">No</th>
                    <th rowspan="2">Tanggal</th>
                    <th colspan="2" class="text-center" >Via Online</th>
                    <th colspan="2" class="text-center">Via Offline</th>
                    <th colspan="2" class="text-center">Total</th>
                  </tr>
                  <tr>
                    <th class="text-center">Terjual</th>
                    <th class="text-center">Nilai</th>
                    <th class="text-center">Terjual</th>
                    <th class="text-center">Nilai</th>
                    <th class="text-center">Terjual</th>
                    <th class="text-center">Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $Terjual_Online =0; $Terjual_Offline = 0;$Total_Harga_Online =0; $Total_Harga =0; $Total_Harga_Offline =0; $Total_Terjual =0;?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->Tanggal}}</td>
                    <td>{{ $d->Terjual_Online }}</td>
                    <?php $Terjual_Online += $d->Terjual_Online; ?>

                    <td>Rp {{ number_format($d->Total_Harga_Online, 0, ',','.') }}</td>
                    <?php $Total_Harga_Online += $d->Total_Harga_Online; ?>

                    <td>{{ $d->Terjual_Offline }}</td>
                    <?php $Terjual_Offline += $d->Terjual_Offline; ?>

                    <td>Rp {{ number_format($d->Total_Harga_Offline, 0, ',','.') }}</td>
                    <?php $Total_Harga_Offline += $d->Total_Harga_Offline; ?>

                    <td>{{ $d->Total_Terjual }}</td>
                    <?php $Total_Terjual += $d->Total_Terjual; ?>

                    <td>Rp {{ number_format($d->Total_Harga, 0, ',','.') }}</td>
                    <?php $Total_Harga += $d->Total_Harga; ?>
                  </tr>
                  <?php $c++; ?>
                  @endforeach 
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">Total</td>
                    <td>{{ $Terjual_Online }}</td>
                    <td>Rp {{ number_format($Total_Harga_Online, 0, ',','.') }}</td>

                    <td>{{ $Terjual_Offline }}</td>
                    <td>Rp {{ number_format($Total_Harga_Offline, 0, ',','.') }}</td>

                    <td>{{ $Total_Terjual }}</td>
                    <td>Rp {{ number_format($Total_Harga, 0, ',','.') }}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
      </div>

      <!-- /.row -->
    </section>
    
  <script src="{{ asset('/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/admin/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},2000);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
