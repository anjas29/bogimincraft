@extends('layouts.admin_layout')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Penjualan Offline
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-home"> Beranda</i></a></li>
        <li class="activate"><a href="/administrator/penjualan-kasir">Penjualan Offline</a></li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="box box-primary">
            <div class="box-header">
              <i class="fa fa-opencart"></i>
              <h3 class="box-title">Detail Data Pembeli</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <form method='post'>
                <div class="col-sm-12 col-md-6">
                  <div class="form-group col-sm-12">
                    <label for="nmprdk" class="col-sm-3 pull-left">No Pesanan</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control disabled-item" name='no_pesanan' disabled value="{{$no_pesanan}}">
                    </div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label for="nmprdk" class="col-sm-3 pull-left">Nama</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name='nama_pelanggan'>
                    </div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label for="nmprdk" class="col-sm-3 pull-left">No Telepon</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name='no_telepon'>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12 col-md-6">
                  <div class="form-group col-sm-12">
                    <label for="nmprdk" class="col-sm-3 pull-left">Kota</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name='kota'>
                    </div>
                  </div>
                  <div class="form-group col-sm-12">
                    <label for="nmprdk" class="col-sm-3 pull-left">Alamat</label>
                    <div class="col-sm-9">
                      <textarea class="form-control" name="alamat"></textarea>
                    </div>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="col-sm-12">
                    {{csrf_field()}}
                    <button type="submit" class=" btn btn-sm btn-primary">Submit</button>
                  </div>
                </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Row-->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  @endsection