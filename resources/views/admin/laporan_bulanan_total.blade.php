@extends('layouts.admin_layout')
@section('header')
  {{ asset('css/dataTables.bootstrap.css') }}
@stop
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Penjualan
      </h1>
      <ol class="breadcrumb">
        <li><a href="/administrator"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Laporan</li>
        <li class="active">Laporan Penjualan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class='row'>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class='box box-info'>
            <div class="box-header">
              <i class="fa fa-bar-chart"></i>
              <b class="box-title">{{$judul}}</b>
              <div class="pull-right">
                  <div class="btn-group ">
                    <form action="/administrator/print-laporan" method="post" target="_blank">
                    
                      {{csrf_field()}}
                      <input type="hidden" name="jenis_laporan" value="{{$form['jenis_laporan']}}">
                      <input type="hidden" name="jenis_penjualan" value="{{$form['jenis_penjualan']}}">
                      <input type="hidden" name="kategori" value="{{$form['kategori']}}">
                      <input type="hidden" name="tahun" value="{{$form['tahun']}}">
                      <input type="hidden" name="bulan" value="{{$form['bulan']}}">
                      <button class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</button>
                    </form> 
                  </div>
                </div>
            </div>
            <div class="box-body">
                <div class="form-group col-md-12 col-xs-12 col-sm-12">
                  <form action="/administrator/laporan" method="post">
                  <label for="nmprdk" class="col-sm-12 col-xs-12 control-label">Masukan Bulan</label>
                  <div class="col-sm-5 col-xs-5">
                    <select class="validate[required] form-control" name="bulan" id="bulan" >
                      <option value="">-- Pilih Bulan --</option>
                      <option value="01">Januari</option>
                      <option value="02">Februari</option>
                      <option value="03">Maret</option>
                      <option value="04">April</option>
                      <option value="05">Mei</option>
                      <option value="06">Juni</option>
                      <option value="07">Juli</option>
                      <option value="08">Agustus</option>
                      <option value="09">September</option>
                      <option value="10">Oktober</option>
                      <option value="11">November</option>
                      <option value="12">Desember</option>
                    </select>
                  </div>
                  <div class="col-sm-5 col-xs-5">
                    <select class="validate[required] form-control" name="bulan2" id="bulan" >
                      <option value="">-- Pilih Bulan --</option>
                      <option value="01">Januari</option>
                      <option value="02">Februari</option>
                      <option value="03">Maret</option>
                      <option value="04">April</option>
                      <option value="05">Mei</option>
                      <option value="06">Juni</option>
                      <option value="07">Juli</option>
                      <option value="08">Agustus</option>
                      <option value="09">September</option>
                      <option value="10">Oktober</option>
                      <option value="11">November</option>
                      <option value="12">Desember</option>
                    </select>
                  </div> 
                  {{csrf_field()}}
                  <input type="hidden" name="jenis_laporan" value="{{$form['jenis_laporan']}}">
                  <input type="hidden" name="jenis_penjualan" value="{{$form['jenis_penjualan']}}">
                  <input type="hidden" name="kategori" value="{{$form['kategori']}}">
                  <input type="hidden" name="tahun" value="{{$form['tahun']}}">
                  <div class="col-sm-2 col-xs-2"><button type="submit" class="btn btn-sm btn-primary">Submit</button></div>
                  </form>
                </div>
              <table class="table table-striped table-bordered dataTable">
                <thead>
                  <tr >
                    <th rowspan="2">No</th>
                    <th rowspan="2">Bulan</th>
                    <th colspan="2" class="text-center" >Via Online</th>
                    <th colspan="2" class="text-center">Via Offline</th>
                    <th colspan="2" class="text-center">Total</th>
                  </tr>
                  <tr>
                    <th class="text-center">Terjual</th>
                    <th class="text-center">Nilai</th>
                    <th class="text-center">Terjual</th>
                    <th class="text-center">Nilai</th>
                    <th class="text-center">Terjual</th>
                    <th class="text-center">Nilai</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $c=1; $Terjual_Online =0; $Terjual_Offline = 0;$Total_Harga_Online =0; $Total_Harga =0; $Total_Harga_Offline =0; $Total_Terjual =0;?>
                  @foreach($data as $d)
                  <tr>
                    <td>{{ $c }}</td>
                    <td>{{ $d->Bulan}}</td>
                    <td>{{ $d->Terjual_Online }}</td>
                    <?php $Terjual_Online += $d->Terjual_Online; ?>

                    <td>Rp {{ number_format($d->Total_Harga_Online, 0, ',','.') }}</td>
                    <?php $Total_Harga_Online += $d->Total_Harga_Online; ?>

                    <td>{{ $d->Terjual_Offline }}</td>
                    <?php $Terjual_Offline += $d->Terjual_Offline; ?>

                    <td>Rp {{ number_format($d->Total_Harga_Offline, 0, ',','.') }}</td>
                    <?php $Total_Harga_Offline += $d->Total_Harga_Offline; ?>

                    <td>{{ $d->Total_Terjual }}</td>
                    <?php $Total_Terjual += $d->Total_Terjual; ?>

                    <td>Rp {{ number_format($d->Total_Harga, 0, ',','.') }}</td>
                    <?php $Total_Harga += $d->Total_Harga; ?>
                  </tr>
                  <?php $c++; ?>
                  @endforeach 
                </tbody>
                <tfoot>
                  <tr>
                    <td colspan="2">Total</td>
                    <td>{{ $Terjual_Online }}</td>
                    <td>Rp {{ number_format($Total_Harga_Online, 0, ',','.') }}</td>

                    <td>{{ $Terjual_Offline }}</td>
                    <td>Rp {{ number_format($Total_Harga_Offline, 0, ',','.') }}</td>

                    <td>{{ $Total_Terjual }}</td>
                    <td>Rp {{ number_format($Total_Harga, 0, ',','.') }}</td>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop
@section('js')
  <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('/js/dataTables.bootstrap.js') }}"></script>

  <script>
    $(document).ready(function() {
      $('.dataTable').dataTable();
    });
  </script>
@stop