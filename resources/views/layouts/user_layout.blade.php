<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bogimin Craft</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="/css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="/css/style.default.css" rel="stylesheet" id="theme-stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">

    <!-- Custom stylesheet - for your changes -->
    <link href="/css/custom.css" rel="stylesheet">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="/images/bogimin-c.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="/img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />
    @yield('css')
</head>

<body>
    <div id="all">

        <header>

            <!-- *** TOP ***
_________________________________________________________ -->
            <div id="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 contact">
                            <div class="login pull-right">
                                @if(auth('pelanggan')->check())
                                <a href="{{Route('pelanggan_profil')}}"><i class="fa fa-user"></i> <span class="hidden-xs text-uppercase">{{auth('pelanggan')->user()->nama}}</span></a>
                                <a href="{{Route('pelanggan_logout')}}"><i class="fa fa-sign-out"></i> <span class="hidden-xs text-uppercase">Logout</span></a>
                                @else
                                <a href="#" data-toggle="modal" data-target="#login-modal"><i class="fa fa-sign-in"></i> <span class="hidden-xs text-uppercase">Log in</span></a>
                                <a href="{{Route('pelanggan_register')}}"><i class="fa fa-user"></i> <span class="hidden-xs text-uppercase">Register</span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- *** TOP END *** -->

            <!-- *** NAVBAR ***
    _________________________________________________________ -->

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header">

                            <a class="navbar-brand home" href="/">
                                <img src="/images/bogimin-b.png" alt="Bogimin Craft" class="img-responsive hidden-xs hidden-sm" style="height:40px; width:187px;">
                                <img src="/images/bogimin-b.png" alt="Bogimin Craft" class="visible-xs visible-sm" style="height:40px; width:187px;"><span class="sr-only">Bogimin Craft</span>
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        <!--/.navbar-header -->

                        <div class="navbar-collapse collapse" id="navigation">

                            <ul class="nav navbar-nav navbar-left">
                                <li class="dropdown
                                    @if(Route::current()->getName() == 'pelanggan_index')
                                        active
                                    @endif
                                ">
                                    <a href="/">Beranda</a>
                                </li>
                                <li class="dropdown
                                    @if(Route::current()->getName() == 'pelanggan_profil')
                                        active
                                    @endif
                                ">
                                    <a href="/profil">Profil</a>
                                </li>
                                <li class="dropdown
                                    @if(Route::current()->getName() == 'pelanggan_panduan')
                                        active
                                    @endif
                                ">
                                    <a href="/panduan">Info</a>
                                </li>
                            </ul>
                            <div class="navbar-buttons" style="padding-top:7px;">
                                <button type="button" class="btn navbar-btn" data-toggle="collapse" data-target="#search">
                                        <i class="fa fa-search"></i>
                                </button>
                            </div>
                            <div class="navbar-buttons" style="padding-top:7px;">
                                <div class="navbar-collapse collapse right" id="basket-overview">
                                    <a href="/kantong-belanja" class="btn navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm"> {{ Cart::count() }} barang</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--/.nav-collapse -->

                        



                        <div class="collapse clearfix" id="search">

                            <form class="navbar-form" role="search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button>
                                    </span> 
                                </div>
                            </form>

                        </div>
                        <!--/.nav-collapse -->

                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>

        <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Login Pelanggan</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{Route('pelanggan_login_post')}}" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email_modal" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password_modal" placeholder="Password" name="password">
                            </div>

                            <p class="text-center">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-template-main"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>
                        </form>

                        <p class="text-center text-muted">Belum punya akun? <a href="{{Route('pelanggan_register')}}"><strong>Daftar sekarang</strong></a>!</p>
                        <p class="text-center text-muted"></p>

                    </div>
                </div>
            </div>
        </div>

        <!-- *** LOGIN MODAL END *** -->


        
        @yield('content')

        <!-- *** GET IT ***
_________________________________________________________ -->

        <section class="bar background-image-fixed-1 no-mb color-white text-center">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon icon-lg"><i class="fa fa-shopping-cart"></i>
                        </div>
                        <h3 class="text-uppercase">Lihat selengkapnya di Bogimin Craft</h3>
                        <p class="lead">Perusahaan ini bergerak di bidang industri gerabah yang menjual barang-barang unik seperti guci, vas, meja, kursi, teraacota, dan patung.</p>
                    </div>

                </div>
            </div>
        </section>
        

        <!-- *** GET IT ***
_________________________________________________________ -->

        <!-- <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>Do you want cool website like this one?</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">Buy this template now</a>
                </div>
            </div>
        </div> -->


        <!-- *** GET IT END *** -->


        <!-- *** FOOTER ***
_________________________________________________________ -->

        <footer id="footer" style="padding-bottom: 10px; padding-top: 10px;">
            <div class="container">
                <div class="col-md-4 col-md-offset-1 col-sm-6">
                    <h4>Bogimin Craft</h4>
                    <p><strong>Jalan Kasongan</strong>
                        <br>RT 02, DK 17,
                        <br>Bangunjiwo, Kasihan, Bantul
                        <br>
                        <strong>Daerah Istimewa Yogyakarta</strong>
                    </p>

                    <hr class="hidden-md hidden-lg hidden-sm">

                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-6">
                    <h4>About us</h4>
                        <a href="https://www.facebook.com/bogimin.craft/" target="_blank" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i> Bogimin Craft</a>  
                        <br>
                        <a href="https://www.instagram.com/bogimincraft/" target="_blank" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i> @BogiminCraft</a> 
                    <img src="/images/bogimin-b-i.png" class="img-responsive" style="padding: 20px 0 0 0; margin-bottom: 5px; height: 70px;">
                    <hr class="hidden-md hidden-lg hidden-sm">
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- /#footer -->

        <!-- *** COPYRIGHT ***
_________________________________________________________ -->

        <div id="copyright" style="padding-bottom: 10px; padding-top: 10px;">
            <div class="container">
                <div class="col-md-12">
                    <p class="text-center">&copy; 2016. Bogimin Craft</p>
                    <!-- <p class="pull-right">Supported by <a href="https://bootstrapious.com/free-templates">Bootstrapious</a> 
                    </p> -->

                </div>
            </div>
        </div>



    </div>
    <!-- /#all -->


    <!-- #### JAVASCRIPT FILES ### -->

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/waypoints.min.js"></script>
    <script src="/js/jquery.counterup.min.js"></script>
    <script src="/js/jquery.parallax-1.1.3.js"></script>
    <script src="/js/owl.carousel.min.js"></script>
    <script src="/js/front.js"></script>
    @yield('js')
</body>

</html>