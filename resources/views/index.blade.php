<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bogimin Craft</title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,500,700,800' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes -->
    <link href="css/custom.css" rel="stylesheet">

    <!-- Responsivity for older IE -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- Favicon and apple touch icons-->
    <link rel="shortcut icon" href="/images/bogimin-c.png" type="image/x-icon" />
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="57x57" href="img/apple-touch-icon-57x57.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-touch-icon-76x76.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png" />
    <link rel="apple-touch-icon" sizes="120x120" href="img/apple-touch-icon-120x120.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon" sizes="152x152" href="img/apple-touch-icon-152x152.png" />
    <!-- owl carousel css -->

    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <style type="text/css">
        .slider{
            width: 100%;
            height: 100%;
            overflow: hidden;
            position: relative;
        }

        .slide{
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            float: left;
            position: absolute;
        }
        .thumb {
          position: relative;
          width: 100%;
          height: 250px;
          overflow: hidden;
        }
        .thumb img {
          position: absolute;
          left: 50%;
          top: 50%;
          height: 100%;
          width: auto;
          -webkit-transform: translate(-50%,-50%);
              -ms-transform: translate(-50%,-50%);
                  transform: translate(-50%,-50%);
        }
        .thumb img.portrait {
          width: 100%;
          height: auto;
        }

        .thumbSmall {
          position: relative;
          width: 100%;
          height: 200px;
          overflow: hidden;
        }
        .thumbSmall img {
          position: absolute;
          left: 50%;
          top: 50%;
          height: 100%;
          width: auto;
          -webkit-transform: translate(-50%,-50%);
              -ms-transform: translate(-50%,-50%);
                  transform: translate(-50%,-50%);
        }
        .thumbSmall img.portrait {
          width: 100%;
          height: auto;
        }
    </style>
</head>

<body>

    <div id="all">

        <header>

            <!-- *** TOP ***
_________________________________________________________ -->
            <div id="top">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 contact">
                            <div class="login pull-right">
                                @if(auth('pelanggan')->check())
                                <a href="{{Route('pelanggan_profil')}}"><i class="fa fa-user"></i> <span class="hidden-xs text-uppercase">{{auth('pelanggan')->user()->nama}}</span></a>
                                <a href="{{Route('pelanggan_logout')}}"><i class="fa fa-sign-out"></i> <span class="hidden-xs text-uppercase">Logout</span></a>
                                @else
                                <a href="#" data-toggle="modal" data-target="#login-modal"><i class="fa fa-sign-in"></i> <span class="hidden-xs text-uppercase">Log in</span></a>
                                <a href="{{Route('pelanggan_register')}}"><i class="fa fa-user"></i> <span class="hidden-xs text-uppercase">Register</span></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- *** TOP END *** -->

            <!-- *** NAVBAR ***
    _________________________________________________________ -->

            <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                    <div class="container">
                        <div class="navbar-header">

                            <a class="navbar-brand home" href="/">
                                <img src="/images/bogimin-b.png" alt="Bogimin Craft" class="img-responsive hidden-xs hidden-sm" style="height:40px; width:187px;">
                                <img src="/images/bogimin-b.png" alt="Bogimin Craft" class="visible-xs visible-sm" style="height:40px; width:187px;"><span class="sr-only">Bogimin Craft</span>
                            </a>
                            <div class="navbar-buttons">
                                <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <i class="fa fa-align-justify"></i>
                                </button>
                            </div>
                        </div>
                        <!--/.navbar-header -->

                        <div class="navbar-collapse collapse" id="navigation">
                            <ul class="nav navbar-nav navbar-left">
                                <li class="dropdown
                                    @if(Route::current()->getName() == 'pelanggan_index')
                                        active
                                    @endif
                                ">
                                    <a href="/">Beranda</a>
                                </li>
                                <li class="dropdown
                                    @if(Route::current()->getName() == 'pelanggan_profil')
                                        active
                                    @endif
                                ">
                                    <a href="/profil">Profil</a>
                                </li>
                                <li class="dropdown
                                    @if(Route::current()->getName() == 'pelanggan_panduan')
                                        active
                                    @endif
                                ">
                                    <a href="/panduan">Info</a>
                                </li>
                            </ul>
                            
                            <div class="navbar-buttons" style="padding-top:7px;">
                                <button type="button" class="btn navbar-btn" data-toggle="collapse" data-target="#search">
                                        <i class="fa fa-search"></i>
                                </button>
                            </div>
                            <div class="navbar-buttons" style="padding-top:7px;">
                                <div class="navbar-collapse collapse right" id="basket-overview">
                                    <a href="/kantong-belanja" class="btn navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm"> {{ Cart::count() }} barang</span>
                                    </a>
                                </div>
                            </div>
                        <!--/.nav-collapse -->

                        <div class="collapse clearfix" id="search">

                            <form class="navbar-form" role="search" action="/index" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search" name="search">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-template-main"><i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </form>

                        </div>
                        <!--/.nav-collapse -->

                    </div>


                </div>
                <!-- /#navbar -->

            </div>

            <!-- *** NAVBAR END *** -->

        </header>

        <!-- *** LOGIN MODAL ***
_________________________________________________________ -->

        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
            <div class="modal-dialog modal-sm">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="Login">Login Pelanggan</h4>
                    </div>
                    <div class="modal-body">
                        <form action="{{Route('pelanggan_login_post')}}" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" id="email_modal" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password_modal" placeholder="Password" name="password">
                            </div>

                            <p class="text-center">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-template-main"><i class="fa fa-sign-in"></i> Log in</button>
                            </p>
                        </form>

                        <p class="text-center text-muted">Belum punya akun? <a href="{{Route('pelanggan_register')}}"><strong>Daftar sekarang</strong></a>!</p>
                        <p class="text-center text-muted"></p>

                    </div>
                </div>
            </div>
        </div>
        <!-- *** LOGIN MODAL END *** -->
        @if(auth('pelanggan')->check())
        <section class="no-mb">
            <!-- *** HOMEPAGE CAROUSEL *** -->
            <div class="no-mb bar background-gray" style="height:100px; padding: 20px 0 0 0;">
                <div class="hidden-xs">
                    <h2 class="text-center"><b>Selamat Datang <span class="text-primary">{{auth('pelanggan')->user()->nama}}</span> di Bogimin Craft</b></h2>
                </div>
                <div class="visible-xs">
                    <h3 class="text-center"><b>Selamat Datang <span class="text-primary">{{auth('pelanggan')->user()->nama}}</span> di Bogimin Craft</b></h3>
                </div>
            </div>
            
            <div class="home-carousel">

                <div class="dark-mask"></div>

                <div class="container">
                    <div class="homepage owl-carousel">
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-5 text-center">
                                    <img src="/images/slider/Slider1.jpg" class="img-responsive" style="height: 300px; width: auto;">
                                </div>
                                <div class="col-sm-7">
                                    <h2>Bogimin Craft</h2>
                                    <ul class="list-style-none">
                                        <li>Industri Grabah</li>
                                        <li>Barang-barang Unik</li>
                                        <li>Guci, Vas, Teraacota, Patung</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-7">
                                    <h2>Bogimin Craft</h2>
                                    <ul class="list-style-none">
                                        <li>Industri Grabah</li>
                                        <li>Barang-barang Unik</li>
                                        <li>Guci, Vas, Teraacota, Patung</li>
                                    </ul>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <img src="/images/slider/Slider2.jpg" class="img-responsive" style="height: 300px; width: auto;">
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-5 text-center">
                                    <img src="/images/slider/Slider3.jpg" class="img-responsive" style="height: 300px; width: auto;">
                                </div>

                                <div class="col-sm-7">
                                    <h2>Bogimin Craft</h2>
                                    <ul class="list-style-none">
                                        <li>Industri Grabah</li>
                                        <li>Barang-barang Unik</li>
                                        <li>Guci, Vas, Teraacota, Patung</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-7">
                                    <h2>Bogimin Craft</h2>
                                    <ul class="list-style-none">
                                        <li>Industri Grabah</li>
                                        <li>Barang-barang Unik</li>
                                        <li>Guci, Vas, Teraacota, Patung</li>
                                    </ul>
                                </div>
                                <div class="col-sm-5 text-center">
                                    <img src="/images/slider/Slider4.jpg" class="img-responsive" style="height: 300px; width: auto;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.project owl-slider -->
                </div>
            </div>

            <!-- *** HOMEPAGE CAROUSEL END *** -->
        </section>
        @endif
        <section>
            <div id="heading-breadcrumbs" style="padding: 0;">
                <div class="container" >
                    <div class="row">
                        <div class="col-md-12 ">
                            <h2 class="text-center">
                                Bogimin Craft
                                <p class="lead text-center" style="margin-bottom: 10px;">Temukan segala yang anda cari disini</p>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div id="content">
                
                <div class="container">

                    <div class="row">
                        <!-- *** LEFT COLUMN ***
                _________________________________________________________ -->

                        <div class="col-sm-3">

                            <!-- *** MENUS AND FILTERS ***
            _________________________________________________________ -->
                            <div class="panel panel-default sidebar-menu">

                                <div class="panel-heading">
                                    <h3 class="panel-title">Kategori</h3>
                                </div>

                                <div class="panel-body">
                                    <ul class="nav nav-pills nav-stacked category-menu">
                                        @foreach($kategori as $k)
                                        <li>
                                            <a href="/index?category={{$k->nama}}">{{$k->nama}} <span class="badge pull-right">{{$k->barang->count()}}</span></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <br>
                                <hr>
                                @if($best_seller != null)
                                <div class="banner">
                                    <p>
                                        <h4>Best Seller</h4>
                                    </p>
                                    <a href="/barang/{{$best_seller->Id_Barang}}">
                                        <img src="/images/barang/{{$best_seller->foto}}" class="img-responsive">
                                        <br>
                                        <p class="text-muted">{{$best_seller->Nama_Barang}}</p>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** LEFT COLUMN END *** -->

                        <!-- *** RIGHT COLUMN ***
                _________________________________________________________ -->

                        <div class="col-sm-9">
                            @if($search != null)
                            <p class="lead">Hasil pencarian barang <b>{{$search}}</b></p>
                            @elseif($category != null)
                            <p class="lead">Kategori barang  <b>{{$category}}</b></p>
                            @else
                            <p class="lead">Barang yand dijual di <b>Bogimin Craft</b></p>
                            @endif

                            <div class="row products">
                                @foreach($data as $d)
                                <div class="col-md-3 col-sm-4">
                                    <div class="product" style="padding-bottom: 5px;">
                                        <div class="thumb">
                                            <a href="/barang/{{$d->id}}">
                                                @if($d->foto != 'default.jpg')
                                                <img src="/images/barang/{{$d->foto}}" alt="" class="img-responsive image1">
                                                @else
                                                <img src="/images/barang/default.jpg" alt="" class="img-responsive image1">
                                                @endif
                                            </a>
                                        </div>
                                        <!-- /.image -->
                                        <div class="text">
                                            <h3>
                                                <a href="/barang/{{$d->id}}">{{$d->nama}}</a>
                                                <br>        
                                                <p class="price">Rp {{ number_format($d->harga_jual, 0, ',','.') }},-</p>
                                            </h3>
                                        </div>
                                        <div class="text">
                                            <a href="/barang/{{$d->id}}" class="btn btn-template-main">Detail</a>
                                            <button class='btn btn-primary beli' data-toggle='modal' data-target='#keranjang' data-id='{{$d->id}}' data-harga='{{$d->harga_jual}}' data-foto='{{$d->foto}}' data-nama='{{$d->nama}}'><i class="fa fa-shopping-cart"></i> Beli</button>
                                        </div>
                                        <!-- /.text -->
                                    </div>
                                    <!-- /.product -->
                                </div>
                                @endforeach
                            </div>
                            <!-- /.products -->


                            <div class="pages">
                                {{ $data->links() }}
                            </div>


                        </div>
                        <!-- /.col-md-9 -->

                        <!-- *** RIGHT COLUMN END *** -->

                    </div>

                </div>
                <!-- /.container -->
            </div>
            <div class="modal fade" id="keranjang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <form method="post" action="/cart/tambah" class="form-horizontal">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Belanja</h4>
                </div>
                <div class="modal-body">
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <div class="thumbSmall">
                            <a href="#">
                                <img src="" alt="" class="img-responsive" id="foto_barang">
                            </a>
                        </div>
                    </div>
                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                        <div class="form-group">
                            <label for="nmprdk" class="col-sm-2 control-label">Nama</label>    
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name='nama' disabled id="nama_barang" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nmprdk" class="col-sm-2 control-label">Harga</label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name='harga' disabled id="harga_barang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nmprdk" class="col-sm-2 control-label">Jumlah</label>    
                            <div class="col-sm-3">
                              <input type="number" class="form-control" name='jumlah' value='1'>
                            </div>
                        </div>
                    </div>
                    {{csrf_field()}}
                    <input type="hidden" name="id" id="id_barang">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary pull-right">Lanjutkan</button>
                </div>
                
              </div>
              </form>
            </div>
          </div>
        </section>

        <section class="bar background-image-fixed-1 no-mb color-white text-center">
            <div class="dark-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="icon icon-lg"><i class="fa fa-shopping-cart"></i>
                        </div>
                        <h3 class="text-uppercase">Lihat selengkapnya di Bogimin Craft</h3>
                        <p class="lead">Perusahaan ini bergerak di bidang industri gerabah yang menjual barang-barang unik seperti guci, vas, meja, kursi, teraacota, dan patung.</p>
                    </div>

                </div>
            </div>
        </section>
        

        <!-- *** GET IT ***
_________________________________________________________ -->

        <!-- <div id="get-it">
            <div class="container">
                <div class="col-md-8 col-sm-12">
                    <h3>Do you want cool website like this one?</h3>
                </div>
                <div class="col-md-4 col-sm-12">
                    <a href="#" class="btn btn-template-transparent-primary">Buy this template now</a>
                </div>
            </div>
        </div> -->


        <!-- *** GET IT END *** -->


        <!-- *** FOOTER ***
_________________________________________________________ -->

        <footer id="footer" style="padding-bottom: 10px; padding-top: 10px;">
            <div class="container">
                <div class="col-md-4 col-md-offset-1 col-sm-6">
                    <h4>Bogimin Craft</h4>
                    <p><strong>Jalan Kasongan</strong>
                        <br>RT 02, DK 17,
                        <br>Bangunjiwo, Kasihan, Bantul
                        <br>
                        <strong>Daerah Istimewa Yogyakarta</strong>
                    </p>

                    <hr class="hidden-md hidden-lg hidden-sm">

                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-6">
                    <h4>About us</h4>
                        <a href="https://www.facebook.com/bogimin.craft/" target="_blank" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i> Bogimin Craft</a>  
                        <br>
                        <a href="https://www.instagram.com/bogimincraft/" target="_blank" class="instagram external" data-animate-hover="shake"><i class="fa fa-instagram"></i> @BogiminCraft</a> 
                    <img src="/images/bogimin-b-i.png" class="img-responsive" style="padding: 20px 0 0 0; margin-bottom: 5px; height: 70px;">
                    <hr class="hidden-md hidden-lg hidden-sm">
                </div>
                <!-- /.col-md-3 -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- /#footer -->

        <!-- *** FOOTER END *** -->

        <!-- *** COPYRIGHT ***
_________________________________________________________ -->

        <div id="copyright" style="padding-bottom: 10px; padding-top: 10px;">
            <div class="container">
                <div class="col-md-12">
                    <p class="text-center">&copy; 2016. Bogimin Craft</p>
                    <!-- <p class="pull-right">Supported by <a href="https://bootstrapious.com/free-templates">Bootstrapious</a> 
                    </p> -->

                </div>
            </div>
        </div>
        <!-- /#copyright -->

        <!-- *** COPYRIGHT END *** -->
    </div>
    <!-- /#all -->

    <!-- #### JAVASCRIPT FILES ### -->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/jquery.parallax-1.1.3.js"></script>
    <script src="js/front.js"></script>

    

    <!-- owl carousel -->
    <script src="js/owl.carousel.min.js"></script>
    <script type="text/javascript">
    $('.beli').click(function() {
        $('#nama_barang').val($(this).data('nama'));
        $('#harga_barang').val($(this).data('harga'));
        $('#id_barang').val($(this).data('id'));


        $('#foto_barang').attr('src', '/images/barang/'+$(this).data('foto'));
      });
    </script>
</body>

</html>