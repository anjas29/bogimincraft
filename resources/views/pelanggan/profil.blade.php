@extends('layouts.user_layout')
@section('css')
    <style type="text/css">
        .img-profile {
          position: relative;
        }
        .img-profile button {
          position: absolute;
          top: 5px;
          right: 20px;
          display: none;
        }
        .img-profile:hover > button {
          display: block;
        }
    </style>
@endsection
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Profil Pelanggan</h1>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="clearfix">

            <div class="container">

                <div class="row">
                    <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
                        <div class="panel panel-default sidebar-menu">


                            <div class="panel-heading">
                                <h3 class="panel-title">Menu</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                    <li class="active">
                                        <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                                    </li>                                 
                                    <li>
                                        <a href="/password"><i class="fa fa-key"></i> Password</a>
                                    </li>
                                    <li>
                                        <a href="/pesanan"><i class="fa fa-list"></i> Pesanan</a>
                                    </li>
                                    <li>
                                        <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                    <!-- *** LEFT COLUMN ***
			 _________________________________________________________ -->

                    <div class="col-md-9 clearfix" id="customer-account">
                        <form action="/profil" method="post" enctype="multipart/form-data">
                            <div class="box">
                                <h2 class="text-lead">
                                    Data diri pelanggan
                                    <button  type="button" class="btn btn-sm btn-primary pull-right" id="edit_profil"><i class="fa fa-pencil"></i> Edit Profil </button>
                                    <button  type="submit" class="btn btn-sm btn-success pull-right" id="save_profil" style="display: none;"><i class="fa fa-save"></i> Simpan </button>
                                </h2>
                                <div class="row">
                                    <div class='col-sm-4 col-xs-12'>
                                        @if(is_null($data->foto))
                                        <img src="/images/user/default.jpg" alt="" class="img-responsive" id="detailImage">
                                        @else
                                        <img src="/images/user/{{ $data->foto }}" alt="" class="img-responsive" id="detailImage">
                                        @endif
                                        <br>
                                        <div class="form-group">
                                            <input type="file" name="foto" style="display: none;" class="form-control" id="input_foto">
                                        </div>
                                    </div>
                                    <div class='col-sm-5'>
                                        <div class="form-group">
                                            <strong>Nama</strong>
                                            <input type="text" name="nama" class="form-control editable" disabled value="{{$data->nama}}">
                                        </div>
                                        <div class="form-group">
                                            <strong>Email</strong>
                                            <input type="email" name="email" class="form-control" disabled value="{{$data->email}}">
                                        </div>
                                        <div class="form-group">
                                            <strong>Kode Pos</strong>
                                            <input type="text" name="kode_pos" class="form-control editable" disabled value="{{$data->kode_pos}}">
                                        </div>
                                        <div class="form-group">
                                            <strong>Kota</strong>
                                            <input type="text" name="kota" class="form-control editable" disabled value="{{$data->kota}}">
                                        </div>
                                        <div class="form-group">
                                            <strong>Alamat</strong>
                                            <textarea class="form-control editable" disabled name="alamat">{{$data->alamat}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <strong>Telepon</strong>
                                            <input type="text" name="no_telepon" class="form-control editable" disabled value="{{$data->no_telepon}}">
                                        </div>
                                        {{csrf_field()}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-md-9 -->

                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
			 _________________________________________________________ -->


                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@endsection
@section('js')
    <script type="text/javascript">
        var editable = false;
        $('#edit_profil').click(function(){
            if(editable){
                $(".editable").attr("disabled", true)
                editable = false;
            }else{
                $('.editable').removeAttr("disabled");
                $('#edit_profil').hide();
                $('#save_profil').show();
                $('#input_foto').show();
                editable = true;
            }
        });
        document.getElementById("input_foto").onchange = function () {
          var reader = new FileReader();

          reader.onload = function (e) {
              // get loaded data and render thumbnail.
               $("#detailImage").fadeOut(1000, function() {
                    document.getElementById("detailImage").src = e.target.result;
                }).fadeIn(1000);
              
          };

          // read the image file as a data URL.
          reader.readAsDataURL(this.files[0]);
      };
    </script>
@endsection