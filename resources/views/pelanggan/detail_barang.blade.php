@extends('layouts.user_layout')
@section('css')
    <style type="text/css">
        .thumbSmall {
          position: relative;
          width: 100%;
          height: 200px;
          overflow: hidden;
        }
        .thumbSmall img {
          position: absolute;
          left: 50%;
          top: 50%;
          height: 100%;
          width: auto;
          -webkit-transform: translate(-50%,-50%);
              -ms-transform: translate(-50%,-50%);
                  transform: translate(-50%,-50%);
        }
        .thumbSmall img.portrait {
          width: 100%;
          height: auto;
        }

    </style>
@stop
@section('content')

        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2>{{$data->nama}}</h2>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container ">

                <div class="row">

                    <!-- *** LEFT COLUMN ***
		    _________________________________________________________ -->

                    <div class="col-md-8 col-md-offset-2">

                        <div class="row" id="productMain">
                            <div class="col-sm-4">
                                <div id="mainImage">
                                    <img src="/images/barang/{{$data->foto}}" alt="" class="img-responsive" style="margin: 0 auto 0 auto;">
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="box">
                                    <h3 class="text-center">
                                        {{$data->nama}}<br>
                                        <span class="text-muted">(Stok hanya {{$data->persediaan}} buah)</span>
                                    </h3>
                                    <p class="price text-primary">Rp {{ number_format($data->harga_jual, 0, ',','.') }},-</p>

                                    <p class="text-center">
                                        <button class='btn btn-primary' data-toggle='modal' data-target='#keranjang'><i class="fa fa-shopping-cart"></i> Beli</button>
                                        <a href="/" class="btn btn-default"></i>Lihat lainnya</a>
                                    </p>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label>Cek estimasi pengiriman</label>
                                        </div>
                                        <div class="col-md-8 col-sm-7 col-xs-6">
                                            <p class="form-group">
                                                <input type="text" name="search_city" id="nama_kota" class="form-control autocomplete" placeholder="Masukan nama kota disini">
                                            </p>
                                        </div>
                                        <div class="col-md-4 col-sm-5 col-xs-6">
                                            <label class="text-muted" style="margin-top: 3px;" id="hasil_estimasi"></label>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="row" id="thumbs">
                                    <div class="col-xs-4">
                                        <a href="/images/barang/{{$data->foto}}" class="thumb">
                                            <img src="/images/barang/{{$data->foto}}" alt="" class="img-responsive">
                                        </a>
                                    </div>
                                </div> -->
                            </div>
                        </div>


                        <div class="box" id="details">
                            <p>
                                <h4>Deskripsi</h4>
                                <blockquote>
                                    <p>
                                        <em>{{$data->deskripsi}}</em>
                                    </p>
                                </blockquote>
                                <h4>Ukuran</h4>
                                <blockquote>
                                    <p>
                                        <em>{{$data->ukuran}}</em>
                                    </p>
                                </blockquote>
                                <h4>Berat</h4>
                                <blockquote>
                                    <p>
                                        <em>{{$data->berat}} gram</em>
                                    </p>
                                </blockquote>
                                <h4>Persediaan</h4>
                                <blockquote>
                                    <p>
                                        <em>{{$data->persediaan}} buah</em>
                                    </p>
                                </blockquote>
                        </div>

                    </div>
                    <!-- /.col-md-9 -->


                    <!-- *** LEFT COLUMN END *** -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        <div class="modal fade" id="keranjang" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
          <form method="post" action="/cart/tambah" class="form-horizontal">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Belanja</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="thumbSmall">
                        <a href="#">
                            <img src="/images/barang/{{ $data->foto }}" alt="" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <div class="form-group">
                        <label for="nmprdk" class="col-sm-2 control-label">Nama</label>    
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name='nama' disabled value='{{ $data->nama }}'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nmprdk" class="col-sm-2 control-label">Harga</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name='harga' disabled value='{{ $data->harga_jual }}'>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nmprdk" class="col-sm-2 control-label">Jumlah</label>    
                        <div class="col-sm-3">
                          <input type="number" class="form-control" name='jumlah' value='1'>
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
                <input type="hidden" value="{{ $data->id }}" name="id">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary pull-right">Lanjutkan</button>
            </div>
            
          </div>
          </form>
            </div>
        </div>

 @endsection
 @section('js')
    <script type="text/javascript">
        var _token = "{{csrf_token()}}";
        var estimasi_data;

        $("#nama_kota").on('change keydown paste input', function(){
            var nama_kota = $(this).val();
            var arr = $.map(estimasi_data, function(el){ 
                if(el.nama_kota == nama_kota){
                    alert(el.nama_kota+" "+el.estimasi_waktu);
                }
            });
            /*$.post("/check-estimasi-pengiriman", {nama_kota: nama_kota, _token:_token})
            .done(function(result) {
                if(result != -1)
                    $('#hasil_estimasi').html('Estimasi waktu <b>'+ result+ ' hari</b>');
                else
                    $('#hasil_estimasi').html('Hasil tidak ditemukan!');     
            })
            .fail(function(result) {
               $('#hasil_estimasi').html('Hasil tidak ditemukan!');
            });*/
        });

        $("#nama_kota").autocomplete({
            source: function (request, response) {
                 $.ajax({
                     url: "/list-kota-pengiriman",
                     type: "GET",
                     data: request,
                     success: function (data) {
                         response($.map(data, function (el) {
                             return {
                                    id : el.estimasi_waktu,
                                    label: el.nama_kota,
                                    value: el.nama_kota
                             };
                         }));
                     },
                     error: function () {
                        response([]);
                     }
                 });
            },
            select: function (event, ui) {
                $('#hasil_estimasi').html('Estimasi waktu <b>'+ ui.item.id+ ' hari</b>');
            }
        });
    </script>
 @endsection