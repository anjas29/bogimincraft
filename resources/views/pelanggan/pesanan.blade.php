@extends('layouts.user_layout')
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Pesanan Pelanggan</h1>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="clearfix">

            <div class="container">

                <div class="row">
                    <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Menu</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                    <li>
                                        <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                                    </li>                   
                                    <li>
                                        <a href="/password"><i class="fa fa-key"></i> Password</a>
                                    </li>                 
                                    <li class="active">
                                        <a href="/pesanan"><i class="fa fa-list"></i> Pesanan</a>
                                    </li>
                                    <li>
                                        <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                    <!-- *** LEFT COLUMN ***
			 _________________________________________________________ -->

                    <div class="col-md-9 clearfix" id="customer-account">
                        <div class="box">
                            <h1>Pesanan</h1>

                            <p class="text-muted">Jika terjadi kesalahan pemesanan <a href="#">contact us</a>, Pesanan akan dilayani 24/7.</p>

                            <hr>
                            @if(count($data) != 0)
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Kode Pesanan</th>
                                            <th>Jenis Transaksi</th>
                                            <th>Tanggal</th>
                                            <th>Total</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data as $d)
                                        <tr>
                                            <th>{{ $d->no_pesanan }}</th>
                                            <td>{{ $d->jenis_transaksi }}</td>
                                            <td>{{ $d->tanggal_pesanan }}</td>
                                            <td>Rp {{ number_format($d->total_pembayaran, 0, ',','.') }}</td>
                                            <td>
                                                @if($d->status == 'Diterima')
                                                    <span class="label label-success">Diterima</span>
                                                @elseif($d->status == 'Konfirmasi Pembayaran')
                                                    <span class="label label-warning">Konfirmasi Pembayaran</span>
                                                @elseif($d->status == 'Proses')
                                                    <span class="label label-info">Proses</span>
                                                @elseif($d->status == 'Dalam Pengiriman')
                                                    <span class="label label-primary">Dalam Pengiriman</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/detail-pesanan/{{ $d->id }}" class="btn btn-primary btn-sm">Lihat</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                                <h3>Tidak ada Pesanan</h3>
                            @endif
                        </div>

                    </div>
                    <!-- /.col-md-9 -->

                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
			 _________________________________________________________ -->


                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@endsection