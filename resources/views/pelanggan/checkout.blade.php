@extends('layouts.user_layout')
@section('css')
<style type="text/css">
.thumbe {
  position: relative;
  width: 100%;
  height: 100px;
  overflow: hidden;
}
.thumbe img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbe img.portrait {
  width: 100%;
  height: auto;
}
</style>
@stop
@section('content')
<div id="content">
    <div class="container">

        <div class="col-md-12" id="checkout">

            <div class="box">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h2 class="text-primary">Checkout Berhasil</h2>
                        <p class="muted">
                            <strong>Terima kasih</strong>, anda telah melakukan checkout pemesanan dengan memilih pembayaran manual.
                        </p>
                        <div class="row">
                            <button data-toggle='modal' data-target='#inputKonfirmasi' class='btn btn-primary col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3'>Konfirmasi Pembayaran
                            </a>
                        </div>
                        <hr>
                        <p class="muted">
                            1. Transfer sejumlah <b>Rp {{ number_format($total, 0, ',','.') }},-</b> ke salah satu nomor rekening di bawah ini:
                            <br>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-1">
                                    <div class="thumbe">
                                        <img src="/img/bni.png" class="img-responsive potrait">
                                    </div>
                                    <p>
                                        <br>
                                        <strong>BNI</strong>
                                        <br><b>0307373714</b>
                                        <br><strong>A.N Bogimin, S. Sos </strong>
                                    </p>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    
                                </div>
                            </div>
                            2. Setelah melakukan transfer, segera konfirmasi Pembayaran. Pembayaran tanpa melakukan konfirmasi tidak dapat kami proses lebih lanjut. Pastikan Anda mengisi data yang tepat saat melakukan konfirmasi pembayaran. 
                            <br>3. Pesanan akan otomatis <strong>dibatalkan</strong> dalam waktu 1 x 24jam jika Anda tidak melakukan pembayaran dan konfirmasi pembayaran.
                            <br>4. Cek Kode Pesanan menggunakan kode berikut : <strong style="color: #009688;">{{$kode}}</strong>
                        </p>
                    </div>
                </div>
            </div>
            <!-- /.box -->


        </div>
        <!-- /.col-md-9 -->
    </div>
</div>
<div class="modal fade" id="inputKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form method="post" action="/konfirmasi-pembayaran" class="form-horizontal" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Konfirmasi Pembayaran</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama Pembeli</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="nama" class="form-control"  value="{{$data->nama_pelanggan}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">No Pesanan</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="no_pesanan" class="form-control" value="{{$data->no_pesanan}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Bank Anda</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="bank" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Rekening atas nama</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="nama_pengirim" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">No Rekening pengirim</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="no_rekening" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nominal Transfer</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
                    <input type="text" name="nominal" class="form-control input-disabled" value="{{$data->total_pembayaran - $data->diskon + $data->kode_unik + $data->tarif_pengiriman}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tanggal Transfer</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="date" name="tanggal" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Bukti Pembayaran</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="file" name="gambar" class="form-control">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{csrf_field()}}
            <input type="hidden" value="{{ $data->id }}" name="id">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Konfirmasi</button>
        </div>
        </div>
        </form>
      </div>
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script type="text/javascript">
    $('#submit-it').click(function(){
        $('.input-disabled').removeAttr("disabled");
    });
</script>
@stop