@extends('layouts.user_layout')
@section('content')

        <div id="heading-breadcrumbs" style="margin-bottom:10px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h2>Invoice Pemesanan</h2>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" >
            <div class="container" >
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 clearfix" id="checkout" >
                        <div class="box" style="padding-top: 0px; margin-top: 0;">
                            <form method="post" action="/invoice">
                                <h2> <a href="/print-invoice" target='_blank' class="btn btn-success pull-right">Cetak</a></h2>
                                <h4>Data Pembeli</h4>

                                <div class="content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-md-6">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="firstname">No Pesanan</label>
                                                        <p><b>{{Session::get('temp_no_pesanan')}}</b></p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="firstname">Nama</label>
                                                        <p><b>{{Session::get('temp_nama')}}</b></p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="lastname">Email</label>
                                                        <p><b>{{auth('pelanggan')->user()->email}}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="firstname">Alamat</label>
                                                        <p><b>{{Session::get('temp_alamat')}}</b></p>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="firstname">No Telepon</label>
                                                        <p><b>{{Session::get('temp_no_telepon')}}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <h3>Konfirmasi Kantong Belanja Anda</h3>
                                        <div class="table-responsive">
                                            <table class="table">
                                            <?php $total = 0; $total_berat =0; ?>
                                            @if ($data['count'] != 0)
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Barang</th>
                                                        <th>Berat</th>
                                                        <th>Jumlah</th>
                                                        <th>Harga Satuan</th>
                                                        <th colspan="2">Total</th>
                                                    </tr>
                                                </thead>
                                                    <tbody>
                                                    @foreach($data['details'] as $d)
                                                        <tr>
                                                            <td>
                                                                <a href="/barang/{{$d->id}}">
                                                                    <img src="/images/barang/{{ $d->options->foto }}" >
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="/barang/{{$d->id}}" class=""><strong> {{ $d->name }}</strong></a>
                                                            </td>
                                                            <td>
                                                                {{ $d->options->berat }}
                                                                <?php $total_berat += $d->options->berat ?>
                                                            </td>
                                                            <td>
                                                                {{ $d->qty }}
                                                            </td>
                                                            <?php $total += $d->subtotal; ?>
                                                            <td>Rp {{ number_format($d->price, 0, ',','.') }},-</td>
                                                            <td>Rp {{ number_format($d->subtotal, 0, ',','.') }},-</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan='5'>Total Belanja</th>
                                                            <th class="2">Rp {{ number_format($total, 0, ',','.') }},-</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="5">Diskon</th>
                                                            <th class="2">Rp {{ number_format(Session::get('temp_diskon'), 0, ',','.') }},-</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Kota Tujuan</th>
                                                            <th class="2">{{Session::get('temp_kota_tujuan')}}</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Ongkos kirim per kg</th>
                                                            <th class="2">Rp {{ number_format(Session::get('temp_harga_per_kg'), 0, ',','.') }},-</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Total Ongkos Kirim</th>
                                                            <th class="2">Rp {{ number_format(Session::get('temp_tarif_pengiriman'), 0, ',','.') }},-</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Kode Unik</th>
                                                            <th class="2">Rp {{ number_format(Session::get('temp_kode_unik'), 0, ',','.')}},-</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Grand Total</th>
                                                            <th class="2">Rp {{ number_format($total - Session::get('temp_diskon') + Session::get('temp_tarif_pengiriman') + Session::get('temp_kode_unik'), 0, ',','.' )}},-</th>
                                                        </tr>
                                                    </tfoot>
                                                @else
                                                    <thead>
                                                        <tr>
                                                            <th> <h3>Kosong</h3> </th>
                                                        </tr>
                                                    </thead>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <div class="pull-left">
                                        <a href="/kantong-belanja/detail" class="btn btn-default"><i class="fa fa-chevron-left"></i>Kembali</a>
                                    </div>
                                    <div class="pull-right">
                                        <input type="hidden" name="berat" value="{{$total_berat}}">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-template-main">Selesai<i class="fa fa-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col-md-9 -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@stop