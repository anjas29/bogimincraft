@extends('layouts.user_layout')
@section('content')
        <div id="heading-breadcrumbs" style="margin-bottom:0px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
            </div>
        </div>
        <div id="content" class="bar background-image-fixed-1 no-mb" style="padding-bottom: 10px;  padding-top: 20px;">
            
            <div class="container">
                <div class="row">
                    <div class="col-md-5 panel panel-primary" style="padding: 20px;">
                        <div class="col-md-12">
                            <h2 class="text-uppercase text-center">Login </h2>
                        </div>
                        <div class="col-md-12">
                            <form action="{{Route('pelanggan_login_post')}}" method="post">
                                @if(Session::has('loginFailed'))
                                  <div class="alert alert-danger alert-dismissable" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <strong>Login Gagal!</strong> Email atau password salah.
                                  </div>
                                @endif
                                @if(Session::has('registrationSuccess'))
                                    <div class="alert alert-success alert-dismissable" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Registrasi Berhasil. </strong> Silahkan login untuk melanjutkan.
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" id="email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password">
                                </div>
                                <div class="text-center">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-template-main"><i class="fa fa-sign-in"></i> Log in</button>
                                </div>
                                <p class="text-center text-muted" style="margin-top: 10px;">Belum punya akun? <a href="{{Route('pelanggan_register')}}"><strong>Daftar sekarang</strong></a>!</p>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-1" style="padding-top: 50px;">
                        <div class="col-md-12">
                            <div class="col-md-6 col-md-offset-3">
                                <img src="/images/bogimin-b-i.png" class="img-responsive">
                            </div>
                            <div class="col-md-12">
                                <br>
                                <p class="lead text-center" style="color: #fff;">
                                    <b>
                                        Berbelanja kerajinan gerabah menjadi lebih mudah. Tersedia banyak pilihan, transaksi mudah dan aman.
                                    </b>
                                </p>
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <div class="team-member">
                                        <div class="social">
                                            <a href="#" class="external facebook" data-animate-hover="pulse"><i class="fa fa-facebook"></i></a>
                                            <a href="#" class="external gplus" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                                            <a href="#" class="external twitter" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                                            <a href="#" class="email" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@stop
