@extends('layouts.user_layout')
@section('content')
        <div id="heading-breadcrumbs" style="margin-bottom:0px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                    </div>
                </div>
            </div>
        </div>
        <div id="content" class="bar background-image-fixed-2 no-mb" style="padding-top: 20px; padding-bottom: 10px;">
            <div class="dark-mask"></div>
            <div class="container" >
                <form action="{{Route('pelanggan_register_post')}}" method="post">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 panel panel-primary" style="padding: 20px;">
                            <div class="col-md-12">
                                <h2 class="text-center text-uppercase">Register</h2>
                                @if(Session::has('messageError'))
                                <div class="alert alert-danger alert-dismissable" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>Registrasi Gagal!</strong> Cek kembali form registrasi anda.
                                </div>
                                @endif
                            </div>
                        
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name-login">Nama Lengkap</label>
                                    <input type="text" class="form-control" id="name-login" name="nama">
                                </div>
                                <div class="form-group">
                                    <label for="email-login">Email</label>
                                    <input type="text" class="form-control" id="email-login" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="password-login">Password</label>
                                    <input type="password" class="form-control" id="password-login" name="password">
                                </div>
                                <div class="form-group">
                                    <label for="password-login">Ulangi Password</label>
                                    <input type="password" class="form-control" id="password-login" name="password_2">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email-login">Kota</label>
                                    <input type="text" class="form-control" name="kota">
                                </div>
                                <div class="form-group">
                                    <label for="email-login">Alamat</label>
                                    <input type="text" class="form-control" name="alamat">
                                </div>
                                <div class="form-group">
                                    <label>Kode Pos</label>
                                    <input type="text" class="form-control" name="kode_pos">
                                </div>
                                <div class="form-group">
                                    <label>No Telepon</label>
                                    <input type="text" class="form-control" name="no_telepon">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <p><?php  echo captcha_img(); ?></p>
                                    <label>Masukan Kode</label>
                                    <input type="text" class="form-control" name="captcha">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="text-center">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-template-main"><i class="fa fa-user-md"></i> Register</button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <!-- /.row -->
                </form>
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@stop
