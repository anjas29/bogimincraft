@extends('layouts.user_layout')
@section('css')

<style type="text/css">
.slider{
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: relative;
}

.slide{
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    float: left;
    position: absolute;
}
.thumb {
  position: relative;
  width: 100%;
  height: 250px;
  overflow: hidden;
}
.thumb img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumb img.portrait {
  width: 100%;
  height: auto;
}

.thumbSmall {
  position: relative;
  width: 100%;
  height: 200px;
  overflow: hidden;
}
.thumbSmall img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbSmall img.portrait {
  width: 100%;
  height: auto;
}
</style>
@endsection
@section('content')
<div id="content">
                
    <div class="container">
        <div class="row">
            <div class='slider'>
    <div class='slide'>
        <div class='slidecontent'>
            <h1>Solar Dolar Wolar Woot</h1>
            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere cursus euismod.Aenean ut tortor leoing elit. Etiam posuere cursus euismod.Aenean ut tortor leo.</h2>
        </div>
    </div>
    <div class='slide' >
        <div class='slidecontent'>
            <h1>Solar Dolar Wolar Woot</h1>
            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam posuere cursus euismod.Aenean ut tortor leo.</h2>
        </div>
    </div>
</div>
        </div>

        <div class="row">
            <!-- *** LEFT COLUMN ***
	_________________________________________________________ -->

            <div class="col-sm-3">

                <!-- *** MENUS AND FILTERS ***
_________________________________________________________ -->
                <div class="panel panel-default sidebar-menu">

                    <div class="panel-heading">
                        <h3 class="panel-title">Kategori</h3>
                    </div>

                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked category-menu">
                            @foreach($kategori as $k)
                            <li>
                                <a href="shop-category.html">{{$k->nama}} <span class="badge pull-right">12</span></a>
                            </li>
                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
            <!-- /.col-md-3 -->

            <!-- *** LEFT COLUMN END *** -->

            <!-- *** RIGHT COLUMN ***
	_________________________________________________________ -->

            <div class="col-sm-9">

                <p class="text-muted lead">Bogimin Craft ....</p>

                <div class="row products">
                    @foreach($data as $d)
                    <div class="col-md-3 col-sm-4">
                        <div class="product">
                            <div class="thumb">
                                <a href="/barang/{{$d->id}}">
                                    @if($d->foto != 'default.jpg')
                                    <img src="/images/barang/{{$d->foto}}" alt="" class="img-responsive image1">
                                    @else
                                    <img src="/images/barang/default.jpg" alt="" class="img-responsive image1">
                                    @endif
                                </a>
                            </div>
                            <!-- /.image -->
                            <div class="text">
                                <h3>
                                    <a href="shop-detail.html">{{$d->nama}}</a>
                                    <br>        
                                    <p class="price">Rp {{ number_format($d->harga_jual, 0, ',','.') }},-</p>
                                </h3>

                                <a href="/barang/{{$d->id}}" class="btn btn-template-main">Detail</a>
                            </div>
                            <!-- /.text -->
                        </div>
                        <!-- /.product -->
                    </div>
                    @endforeach
                </div>
                <!-- /.products -->


                <div class="pages">
                    {{ $data->links() }}
                </div>


            </div>
            <!-- /.col-md-9 -->

            <!-- *** RIGHT COLUMN END *** -->

        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
@endsection