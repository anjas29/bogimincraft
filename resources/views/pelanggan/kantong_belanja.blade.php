@extends('layouts.user_layout')
@section('content')
<div id="content">
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-offset-1">
                        <h2>Kantong Belanja</h2>
                    </div>  
                </div>
            </div>
        </div>
    <div class="container">

        <div class="col-md-10 col-md-offset-1" id="basket">
            <div class="box">
                @if(Session::has('messageError'))
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong>Update jumlah barang gagal!</strong> {{Session::get('messageError')}}
                </div>
                @endif
                <form method="post" action="/cart/update">
                    <div class="table-responsive">
                        <table class="table">
                        <?php $total = 0; ?>
                        @if ($data['count'] != 0)
                            <thead>
                                <tr>
                                    <th colspan="2">Barang</th>
                                    <th>Harga Satuan</th>
                                    <th>Jumlah</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
	                            <tbody>
	                            @foreach($data['details'] as $d)
	                                <tr>
	                                    <td>
	                                        <a href="/barang/{{$d->id}}">
	                                            <img src="/images/barang/{{ $d->options->foto }}" >
	                                        </a>
	                                    </td>
	                                    <td>
                                            <a href="/barang/{{$d->id}}" class=""><strong> {{ $d->name }}</strong></a>
	                                    </td>
                                        <?php $total += $d->subtotal; ?>
	                                    <td>Rp {{ number_format($d->price, 0, ',','.') }},-</td>
                                        <td>
                                            <input type="number" name='qty[]' value="{{ $d->qty }}" class="form-control" style="width:80px;">
                                        </td>
	                                    <td>Rp {{ number_format($d->subtotal, 0, ',','.') }},-</td>
	                                    <td>
	                                    	<input type="hidden" name='rowId[]' value="{{ $d->rowId }}">
                                            <input type="hidden" name='id[]' value="{{ $d->id }}">
	                                    	<a type='button' class='delete-item' data-rowid="{{ $d->rowId }}" data-name="{{ $d->name }}"><i class="fa fa-trash-o"></i></a>
	                                    </td>
	                                </tr>
	                            @endforeach
	                            </tbody>
	                            <tfoot>
	                                <tr>
	                                    <th colspan="4">Total</th>
										<th class="2">Rp {{ number_format($total, 0, ',','.') }},-</th>
	                                </tr>
                                    <tr>
                                        <th colspan="4">Diskon</th>
                                        <th class="2">Rp {{ number_format($diskon, 0, ',','.') }},-</th>
                                    </tr>
                                    <tr>
                                        <th colspan="4">Grand Total</th>
                                        <th class="2">Rp {{ number_format($total-$diskon, 0, ',','.') }},-</th>
                                    </tr>
	                            </tfoot>
	                        @else
	                       		<thead>
	                                <tr>
	                                    <th> <h3>Kosong</h3> </th>
	                                </tr>
	                            </thead>
	                        @endif
                        </table>
                    </div>
                    <!-- /.table-responsive -->

                    <div class="box-footer">
                        
                            <blockquote>
                                <ul>
                                    <li>Diskon 10% berlaku setiap pembelian minimal 10 barang.</li>
                                    <li>Total Harga barang diatas belum termasuk ongkos kirim yang akan dihitung saat Selesa Belanja</li>
                                </ul>
                            </blockquote>
                        
                        <div class="pull-left">
                            <a href="/" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali Belanja</a>
                        </div>
                        <div class="pull-right">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-default"><i class="fa fa-refresh"></i> Perbarui jumlah barang</button>
                            <a href="/kantong-belanja/detail" class="btn btn-primary">Lanjutkan Pembayaran <i class="fa fa-chevron-right"></i></a>
                        </div>
                    </div>

                </form>

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col-md-9 -->

        

    </div>
    <!-- /.container -->
</div>
@stop
@section('js')
<script src="{{ asset('/js/bootbox.min.js') }}"></script>
<script src="{{ asset('/js/toastr.min.js') }}"></script>
<script type="text/javascript" charset="utf-8">

$('.delete-item').click(function() {
	var rowid = $(this).data('rowid');
	var barang = $(this).data('name');
    var _token = '{{csrf_token()}}';

	bootbox.confirm("Anda yakin akan menghapus barang <b>"+barang+"</b> ?", function(result) {
		if (result) {
			toastr.options.timeOut = 0;
			toastr.options.extendedTimeOut = 0;
			toastr.info('<i class="fa fa-spinner fa-spin"></i><br>Sedang menghapus...');
			toastr.options.timeOut = 5000;
			toastr.options.extendedTimeOut = 1000;
			$.post("/cart/delete", {rowid: rowid, _token:_token})
			.done(function(result) {
				window.location.replace("/kantong-belanja");
			})
			.fail(function(result) {
				toastr.clear();
				toastr.error('Kesalahan server! Silahkan reload halaman dan coba lagi');
			});
		};
	}); 
});
</script>
@stop