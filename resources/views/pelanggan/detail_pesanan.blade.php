@extends('layouts.user_layout')
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Pesanan Pelanggan</h1>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="clearfix">

            <div class="container">

                <div class="row">
                    <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Menu</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                    <li>
                                        <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                                    </li>
                                    <li>
                                        <a href="/password"><i class="fa fa-key"></i> Password</a>
                                    </li>                      
                                    <li class="active">
                                        <a href="/pesanan"><i class="fa fa-list"></i> Pesanan</a>
                                    </li>
                                    <li>
                                        <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                    <!-- *** LEFT COLUMN ***
			 _________________________________________________________ -->

                    <div class="col-md-9 clearfix" id="customer-account">
                        <div class="box">
                            
                                <h2>
                                    Detail Pesanan {{$data->no_pesanan}}
                                    @if($data->status == 'Diterima')
                                        <button class="btn btn-success pull-right">Diterima</button>
                                    @elseif($data->status == 'Konfirmasi Pembayaran')
                                        <button class="btn btn-danger pull-right verifikasi" data-toggle='modal' data-target='#inputKonfirmasi'>Konfirmasi Pembayaran</button>
                                    @elseif($data->status == 'Proses')
                                        <button class="btn btn-info pull-right">Proses</button>
                                    @elseif($data->status == 'Dalam Pengiriman')
                                        <button class="btn btn-primary pull-right">Dalam Pengiriman</button>
                                    @endif
                                </h2>
                                <h4>Data Pembeli</h4>
                                <div class="content">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="firstname">Nama</label>
                                                    <p><b>{{$data->nama_pelanggan}}</b></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="firstname">No Telepon</label>
                                                    <p><b>{{$data->no_telepon}}</b></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="lastname">Email</label>
                                                    <p><b>{{$data->pelanggan->email}}</b></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="firstname">Kota</label>
                                                    <p><b>{{$data->kota}}</b></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="firstname">Alamat</label>
                                                    <p><b>{{$data->alamat}}</b></p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <h3>Data Barang</h3>
                                        <div class="table-responsive">
                                            <table class="table">
                                            <?php $total = 0; $total_berat =0; ?>
                                            @if ($data->detail_pesanan != null)
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Barang</th>
                                                        <th>Berat</th>
                                                        <th>Jumlah</th>
                                                        <th>Harga Satuan</th>
                                                        <th colspan="2">Total</th>
                                                    </tr>
                                                </thead>
                                                    <tbody>
                                                    @foreach($data->detail_pesanan as $d)
                                                        <tr>
                                                            <td>
                                                                <a href="/barang/{{$d->id}}">
                                                                    <img src="/images/barang/{{ $d->barang->foto }}" style="width: auto; height: 80px;">
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="/barang/{{$d->id}}" class=""><strong> {{ $d->barang->nama }}</strong></a>
                                                            </td>
                                                            <td>
                                                                {{ $d->barang->berat }}
                                                                <?php $total_berat += $d->barang->berat ?>
                                                            </td>
                                                            <td>
                                                                {{ $d->jumlah }}
                                                            </td>
                                                            <?php $total += $d->harga*$d->jumlah; ?>
                                                            <td>Rp {{ number_format($d->harga, 0, ',','.') }}</td>
                                                            <td>Rp {{ number_format($d->harga*$d->jumlah, 0, ',','.') }}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan='5'>Total Belanja</th>
                                                            <th class="2">Rp {{ number_format($total, 0, ',','.') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="5">Diskon</th>
                                                            <th class="2">Rp {{ number_format($data->potongan_harga, 0, ',','.') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Total Ongkos Kirim</th>
                                                            <th class="2">Rp {{ number_format($data->tarif_pengiriman, 0, ',','.') }}</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Kode Unik</th>
                                                            <th class="2">Rp {{ number_format($data->kode_unik, 0, ',','.')}}</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan='5'>Grand Total</th>
                                                            <th class="2">Rp {{ number_format($total - $data->potongan_harga + $data->tarif_pengiriman+$data->kode_unik, 0, ',','.') }}</th>
                                                        </tr>
                                                        
                                                    </tfoot>
                                                @else
                                                    <thead>
                                                        <tr>
                                                            <th> <h3>Kosong</h3> </th>
                                                        </tr>
                                                    </thead>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
                        <!-- /.box -->

                    </div>
                    <!-- /.col-md-9 -->

                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
			 _________________________________________________________ -->


                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

<div class="modal fade" id="inputKonfirmasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <form method="post" action="/konfirmasi-pembayaran" class="form-horizontal" enctype="multipart/form-data">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">Konfirmasi Pembayaran</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label class="col-sm-3 control-label">Nama Pembeli</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="nama" class="form-control" value="{{$data->nama_pelanggan}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">No Pesanan</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="no_pesanan" class="form-control" value="{{$data->no_pesanan}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Bank Anda</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="bank" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Rekening atas nama</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="nama_pengirim" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">No Rekening pengirim</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="no_rekening" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nominal Transfer</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="text" name="nominal" class="form-control input-disabled" value="{{$data->total_pembayaran - $data->diskon + $data->kode_unik + $data->tarif_pengiriman}}" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Tanggal Transfer</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="date" name="tanggal" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Bukti Pembayaran</label>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">      
                    <input type="file" name="gambar" class="form-control">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            {{csrf_field()}}
            <input type="hidden" value="{{ $data->id }}" name="id">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" id="submit-it" class="btn btn-primary">Konfirmasi</button>
        </div>
        </div>
        </form>
      </div>
</div>
@endsection
@section('js')
    <script type="text/javascript">
        $('#submit-it').click(function(){
            $('.input-disabled').removeAttr("disabled");
        });
    </script>
@endsection