@extends('layouts.user_layout')
@section('css')
    <style type="text/css">
        #id{
            position: fixed;
            bottom: 200px;
            right: 200px;
        }
    </style>
@endsection
@section('content')
        <div id="heading-breadcrumbs" style="padding: 0; margin-bottom: 0;">
            <div class="container" >
                <div class="row">
                    <div class="col-md-12 ">
                        <h2 class="text-center">
                            Informasi
                        </h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="bar background-gray no-mb" style="margin-top: 0; padding: 40px 0 40px 0;">
            <div class="dark-mask"></div>
            <div class="dark-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="#sejarah">
                        <div class="box-simple" style="margin-bottom: 5px; color: #fff;">
                            <div class="icon" style="color: #fff; border-color: #fff; border-width: 3px; margin-bottom: 10px;">
                                <i class="fa fa-info"></i>
                            </div>
                            <h3 style="margin: 5px 0 5px 0; color: #fff;">Sejarah</h3>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="#syarat">
                        <div class="box-simple" style="margin-bottom: 5px; color: #fff;">
                            <div class="icon" style="color: #fff; border-color: #fff; border-width: 3px; margin-bottom: 10px;">
                                <i class="fa fa-list-ul"></i>
                            </div>
                            <h3 style="margin: 5px 0 5px 0; color: #fff;">Syarat dan Ketentuan</h3>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="#akun">
                            <div class="box-simple" style="margin-bottom: 5px; color: #fff;">
                                <div class="icon" style="color: #fff; border-color: #fff; border-width: 3px; margin-bottom: 10px;">
                                    <i class="fa fa-vcard"></i>
                                </div>
                                <h3 style="margin: 5px 0 5px 0; color: #fff;">Cara Membuat Akun</h3>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <a href="#berbelanja">
                        <div class="box-simple" style="margin-bottom: 5px; color: #fff;">
                            <div class="icon" style="color: #fff; border-color: #fff; border-width: 3px; margin-bottom: 10px;">
                                <i class="fa fa-shopping-bag"></i>
                            </div>
                            <h3 style="margin: 5px 0 5px 0; color: #fff;">Cara Berbelanja</h3>
                        </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="clearfix"style="padding-top: 0px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-8" id="sejarah">
                        <div class="heading">
                            <h3><i class="fa fa-info"></i> Sejarah</h3>
                        </div>
                        <p>Bogimin Craft didirikan pada tahun 1995. Perusahaan ini bergerak di bidang industri gerabah yang menjual barang-barang unik seperti guci, vas, meja, kursi, teraacota, dan patung. Bogimin Craft secara resmi terdaftar sebagai Usaha Kecil Menengah pada tahun 1997 berdasarkan pada Surat Izin Usaha Perdagangan (SIUP) yang dimilikinya.</p>
                        <br>
                    </div>
                    <div class="col-md-8" id="syarat">
                        <div class="heading">
                            <h3><i class="fa fa-list-ul"></i> Syarat dan Ketentuan</h3>
                        </div>
                        <ul class="ul-icons">
                            <li>
                                <i class="fa fa-check"></i>
                                Bogimin Craft hanya menerima anggota berusia 17 tahun ke atas dan atau layak bertindak sesuai hukum.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Semua data harus diisi dengan lengkap dan benar serta tidak berkenankan menyalahgunakan data termasuk nomor telepon, email, dan alamat milik orang lain dengan alasan dan tujuan apapun. Anda bersedia dituntut secara hukum oleh pemilik data yang sah dan Bogimin Craft berhak mencabut hak keanggotaan dan atau memilih langkah hukum untuk pelanggaran ini.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Setiap anggota bertanggung jawab sepenuhnya terhadap keamanan password dan penggunaan akunnya. 
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Anggota Bogimin Craft setuju untuk tidak menyalahgunakan fasilitas dan fitur yang ada untuk merugikan manapun juga, individu maupun kelompok.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Bogimin Craft berhak memperingatkan dan menghapus akun keanggotaan secara sepihak jika dicurigai berkaitan dengan pemalsuan/penyalahgunaan data, penipuan, melanggar salah satu poin dalam Syarat dan Ketentuan Bogimin Craft dan segala jenis pelanggaran hukum. 
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Bogimin Craft berhak kapanpun dan dengan alasan apapun, mengubah bagian manapun dalam Syarat dan Kondisi ini dan setiap anggota menyetujuinya.
                            </li> 
                        </ul>
                    </div>
                    <div class="col-md-8" id="akun">
                        <div class="heading">
                            <h3><i class="fa fa-vcard"></i> Cara Membuat Akun</h3>
                        </div>
                        <ul class="ul-icons">
                            <li>
                                <i class="fa fa-check"></i>
                                Pada menu kanan atas, klik 'DAFTAR'.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Isi semua rincian yang dibutuhkan.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Klik 'SUBMIT' setelah Kamu selesai. Semua rincian Kamu akan tersimpan
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Setelah mengklik "SUBMIT", pendaftaran Kamu sudah selesai. Selamat datang di Bogimin Craft !!!
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8" id="berbelanja">
                        <div class="heading">
                            <h3><i class="fa fa-shopping-bag"></i> Cara Berbelanja di Bogimin Craft</h3>
                        </div>
                        <ul class="ul-icons">
                            <li>
                                <i class="fa fa-check"></i>
                                Pertama sebelum memulai melakukan belanja, silahkan melakukan pendaftaran pelanggan terlebih dahulu.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Setelah pendaftaran selesai dilakukan silahkan cari barang yang akan dibeli pada halaman utama yang menyediakan tampilan barang yang dijual dan kategori, kemudian pilih barang tersebut, Misal nya Anda akan membeli guci, maka klik guci tersebut trus klik Beli untuk memasukkan barang belanja ke kantong belanja Anda.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Dalam kantong, Anda dapat menambahkan atau mengubah barang yang akan dibeli yang disertai dengan pemberitahuan harga barang.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Bila sudah menentukan barang yang ingin dibelii pada kantong belanja selanjutnya klik Lanjut yang mengarahkan anda kepada halaman detail kantong belanja.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Pada halaman detail kantong belanja, Anda tidak perlu mengisikan nama, alamat, nomor telepon, email karena sudah terisi otomatis namun alamat pengiriman dan pemilihan paket jasa pengiriman bisa diperbarui.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Setelah itu klik proses pesanan yang selanjutnya Anda akan mendapatkan tampilan inoive yang berisikan detail pesanan, total pembayaran yang harus dibayarkan, dan nomor pesanan.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Kemudian setelah Anda melakukan pembayaran, lakukan konfirmasi pembayaran segera pada halaman Konfirmasi Pembayaran.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Biasanya setelah melakukan konfirmasi anda akan menunggu maksimal selama 24 jam, pihak Bogimin Craft akan mencek terlebih dahulu pembayaran dari agan, biasanya bila pembayaran sudah dicek/diterima zalora maka akan ada pemberitahuan lewat notifikasi akun Bogimin Craft anda atau lewat sms.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Bila pemberitahuan dari Bogimin Craft belum masuk sedangkan agan sudah menunggu selama 24 jam silahkan telpon CS Bogimin Craft di nomor ini 0813-2802-6271.
                            </li>
                            <li>
                                <i class="fa fa-check"></i>
                                Seteleh mendapat konfrmasi dari pihak zalora bahwa pembayaran telah diterima maka silahkan tunggu pesanan Anda datang kerumah, biasanya lama pengiriman di Bogimin Craft tergantung lokasi Anda.
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        
@endsection
@section('js')
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('a').on('click', function (event) {
                var target = $(this.hash),
                    top;

                if (target) {
                    top = target.offset().top;

                    event.preventDefault();

                    $('html, body').animate({
                        scrollTop: top - 135
                    }, 1000, 'swing');
                }
            });
        });
    </script>
@endsection