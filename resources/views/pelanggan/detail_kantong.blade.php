@extends('layouts.user_layout')
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-md-offset-1">
                        <h2>Detail Kantong Belanja</h2>
                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 clearfix" id="checkout">
                        <div class="box">
                            <form method="post" action="">
                                <h3>Data Pembeli <button type="button" class="btn btn-sm btn-warning  pull-right" id="editBtn">Ubah Data</button></h3>

                                <div class="content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="firstname">Nama</label>
                                                    <input type="text" class="form-control edit-data" name="nama" value="{{auth('pelanggan')->user()->nama}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="firstname">Alamat</label>
                                                    <textarea class="form-control edit-data" name="alamat" disabled>{{auth('pelanggan')->user()->alamat}}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="firstname">No Telepon</label>
                                                    <input type="text" class="form-control edit-data" name="no_telepon" value="{{auth('pelanggan')->user()->no_telepon}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="lastname">Email</label>
                                                    <input type="text" class="form-control item-disabled" name="email" value="{{auth('pelanggan')->user()->email}}" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="company">Kota Tujuan</label>
                                                        <select name="kota" class="form-control jasa_pengiriman edit-data" id="kota" disabled>
                                                            @foreach($kota as $j)
                                                                @if($j->nama_kota == auth('pelanggan')->user()->kota)
                                                                    <option value="{{$j->nama_kota}}" selected>{{$j->nama_kota}}</option>
                                                                @else
                                                                    <option value="{{$j->nama_kota}}">{{$j->nama_kota}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="company">Jasa pengiriman</label>
                                                        <select name="jasa_pengiriman" class="form-control jasa_pengiriman input-disabled" id="nama_jasa" disabled>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="street">Harga Pengiriman</label>
                                                        <input type="text" class="form-control input-disabled" name="harga_per_kg" id="ongkos" disabled value="0">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="street">Estimasi Waktu</label>
                                                        <input type="text" class="form-control input-disabled" name="estimasi_waktu" id="estimasi_waktu" disabled value="0">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                    <div class="row">
                                        <h3>Konfirmasi Kantong Belanja Anda</h3>
                                        <div class="table-responsive">
                                            <table class="table">
                                            <?php $total = 0; ?>
                                            @if ($data['count'] != 0)
                                                <thead>
                                                    <tr>
                                                        <th colspan="2">Barang</th>
                                                        <th>Berat</th>
                                                        <th>Jumlah</th>
                                                        <th>Harga Satuan</th>
                                                        <th colspan="2">Total</th>
                                                    </tr>
                                                </thead>
                                                    <tbody>
                                                    <?php $total_berat = 0; ?>
                                                    @foreach($data['details'] as $d)
                                                        <tr>
                                                            <td>
                                                                <a href="/barang/{{$d->id}}">
                                                                    <img src="/images/barang/{{ $d->options->foto }}" >
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="/barang/{{$d->id}}" class=""><strong> {{ $d->name }}</strong></a>
                                                            </td>
                                                            <td>
                                                                <?php $total_berat += $d->options->berat; ?>
                                                                {{$d->options->berat}}
                                                            </td>
                                                            <td>
                                                                {{ $d->qty }}
                                                            </td>
                                                            <?php $total += $d->subtotal; ?>
                                                            <td>Rp {{ number_format($d->price, 0, ',','.') }},-</td>
                                                            <td>Rp {{ number_format($d->subtotal, 0, ',','.') }},-</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="5">Total</th>
                                                            <th class="2">Rp {{number_format($total, 0, ',', '.')}},-</th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="5">Diskon</th>
                                                            @if($diskon != null)
                                                            <th class="2">Rp {{number_format($diskon, 0, ',', '.')}},-</th>
                                                            @else
                                                            <th class="2">Rp 0,-</th>
                                                            @endif
                                                        </tr>
                                                        <tr>
                                                            <th colspan="5">Tarif Pengiriman</th>
                                                            <th class="2">Rp <span class="hasil_tarif_pengiriman_2">0</span></th>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="5">Total</th>
                                                            <th class="2">Rp <span class="hasil_total">{{$total}}</span></th>
                                                        </tr>
                                                    </tfoot>
                                                @else
                                                    <thead>
                                                        <tr>
                                                            <th> <h3>Kosong</h3> </th>
                                                        </tr>
                                                    </thead>
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <div class="pull-left">
                                        <a href="/kantong-belanja" class="btn btn-default"><i class="fa fa-chevron-left"></i>Kembali</a>
                                    </div>
                                    <div class="pull-right">
                                        {{csrf_field()}}
                                        <input type="hidden" name="" id="hasil_total" value="{{$total}}">
                                        <input type="hidden" name="" id="hasil_diskon" value="{{$diskon}}">
                                        <input type="hidden" name="total_berat" id="hasil_total_berat" value="{{$total_berat}}">
                                        <input type="hidden" name="tarif_pengiriman" class='hasil_tarif_pengiriman'>
                                        <button type="submit" class="btn btn-template-main" class='btn_submit' id='submit_it'>Lanjut<i class="fa fa-chevron-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col-md-9 -->

                    <div class="col-md-3">

                        <div class="box" id="order-summary">
                            <div class="box-header">
                                <h3>Ringkasan</h3>
                            </div>
                            <p class="text-muted">Total Harga pembelian barang anda</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <td>Subtotal</td>
                                            <th>Rp {{number_format($total, 0, ',', '.')}},-</th>
                                        </tr>
                                        <tr>
                                            <td>Diskon</td>
                                            <th>Rp {{number_format($diskon, 0, ',', '.')}},-</th>
                                        </tr>
                                        <tr>
                                            <td>Tarif Pengiriman</td>
                                            <th>Rp <span class="hasil_tarif_pengiriman_2">0</span></th>
                                        </tr>
                                        <tr class="total">
                                            <td>Grand Total</td>
                                            <th>Rp <span class="hasil_total">{{$total}}</span></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@stop
@section('js')
    <script type="text/javascript">
        var _token = '{{csrf_token()}}';
        var edit = false;
        $('#kota').change(function() {
            var kota = $(this).val();
            $.post("/kota-pengiriman", {'kota':kota, '_token':_token})
            .done(function(result) {
                $('#nama_jasa').removeAttr("disabled");
                $('#nama_jasa').empty();
                $.each(result, function(key, value) {
                     $('#nama_jasa')
                          .append($('<option>', { value: value.id })
                          .text(value.jasa_pengiriman)); 
                });

                var jasa = result[0].jasa_pengiriman;
                var berat = $('#hasil_total_berat').val();
                var diskon = $('#hasil_diskon').val();
                var total = $('#hasil_total').val();
                $.post("/jasa-pengiriman", {'kota':kota, 'jasa':jasa, '_token':_token})
                .done(function(result2) {
                    $('#ongkos').val(result2.tarif_pengiriman);
                    $('#estimasi_waktu').val(result2.estimasi_waktu);
                    var hasil = berat * result2.tarif_pengiriman;
                    var hasil_total = eval(+total + +hasil - diskon);
                    $('.hasil_tarif_pengiriman').val(hasil);
                    $('.hasil_tarif_pengiriman_2').text(hasil.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+",-");
                    $('.hasil_total').text(hasil_total.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+",-");
                });
            });
        });

        $('#editBtn').click(function(){
            if(edit){
                $('.edit-data').removeAttr("disabled");
                $("#editBtn").html('Simpan Data');
                $("#editBtn").addClass("btn-primary");
                $("#editBtn").removeClass("btn-warning");
                edit = false;
            }else{
                $(".edit-data").attr("disabled", true);
                $("#editBtn").html('Ubah Data');
                $("#editBtn").addClass("btn-warning");
                $("#editBtn").removeClass("btn-primary");
                
                edit = true;
            }
        });
        $('.jasa_pengiriman').change(function() {
            var kota = $('#kota').val();
            var jasa = $('#nama_jasa').val();
            var berat = $('#hasil_total_berat').val();
            var total = $('#hasil_total').val();
            var diskon = $('#hasil_diskon').val();

            $.post("/jasa-pengiriman", {'kota':kota, 'jasa':jasa, '_token':_token})
            .done(function(result) {
                $('#ongkos').val(result.tarif_pengiriman);
                $('#estimasi_waktu').val(result.estimasi_waktu);
                var hasil = berat * result.tarif_pengiriman;
                var hasil_total = eval(+total + +hasil-diskon);
                $('.hasil_tarif_pengiriman').val(hasil);
                $('.hasil_tarif_pengiriman_2').text(hasil.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+",-");
                    $('.hasil_total').text(hasil_total.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+",-");
            });
        });

        function pageLoad() {
            var kota = $('#kota').val();
            $.post("/kota-pengiriman", {'kota':kota, '_token':_token})
            .done(function(result) {
                $('#nama_jasa').empty();
                $.each(result, function(key, value) {
                     $('#nama_jasa')
                          .append($('<option>', { value: value.id })
                          .text(value.jasa_pengiriman)); 
                });

                var jasa = result[0].jasa_pengiriman;
                var berat = $('#hasil_total_berat').val();
                var total = $('#hasil_total').val();
                var diskon  = $('#hasil_diskon').val();

                $.post("/jasa-pengiriman", {'kota':kota, 'jasa':jasa, '_token':_token})
                .done(function(result2) {
                    $('#ongkos').val(result2.tarif_pengiriman);
                    $('#estimasi_waktu').val(result2.estimasi_waktu);
                    var hasil = berat * result2.tarif_pengiriman;
                    var hasil_total = eval(+total + +hasil - diskon);
                    $('.hasil_tarif_pengiriman').val(hasil);
                    $('.hasil_tarif_pengiriman_2').text(hasil.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+",-");
                    $('.hasil_total').text(hasil_total.toFixed(0).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")+",-");
                });
            });
        }

        window.onload = pageLoad;

        $('#submit_it').click(function() {
            $('.input-disabled').removeAttr("disabled");
            $('.edit-data').removeAttr("disabled");
            $("#ongkos").removeAttr("disabled");
        });
    </script>
@stop