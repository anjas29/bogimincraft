@extends('layouts.user_layout')
@section('content')
        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1>Password Pelanggan</h1>
                    </div>
                </div>
            </div>
        </div>

        <div id="content" class="clearfix">

            <div class="container">

                <div class="row">
                    <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
 _________________________________________________________ -->
                        <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Menu</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                    <li class="">
                                        <a href="/profil"><i class="fa fa-user"></i> Profil</a>
                                    </li>                                 
                                    <li class="active">
                                        <a href="/password"><i class="fa fa-key"></i> Password</a>
                                    </li>
                                    <li>
                                        <a href="/pesanan"><i class="fa fa-list"></i> Pesanan</a>
                                    </li>
                                    <li>
                                        <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                    <!-- *** LEFT COLUMN ***
			 _________________________________________________________ -->

                    <div class="col-md-9 clearfix" id="customer-account">
                        <form action="/password" method="post" enctype="multipart/form-data">
                            <div class="box col-md-8">
                                @if(Session::has('messageError'))
                                  <div class="alert alert-danger alert-dismissable" role="alert">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <strong>Gagal. </strong> Ubah password gagal dilakukan.
                                  </div>
                                @endif
                                @if(Session::has('messageSuccess'))
                                    <div class="alert alert-success alert-dismissable" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Berhasil. </strong> Ubah password berhasil dilakukan.
                                    </div>
                                @endif
                                <h2 class="text-lead">
                                    Ubah password
                                    <button  type="submit" class="btn btn-sm btn-success pull-right" id="save_profil"><i class="fa fa-save"></i> Simpan </button>
                                </h2>
                                <div class="row">
                                    <div class='col-xs-10'>
                                        <div class="form-group">
                                            <strong>Password lama</strong>
                                            <input type="password" name="old-password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <strong>Password baru</strong>
                                            <input type="password" name="password" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <strong>Ulangi password baru</strong>
                                            <input type="password" name="re-password" class="form-control">
                                        </div>
                                        {{csrf_field()}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- /.col-md-9 -->

                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
			 _________________________________________________________ -->


                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@endsection
@section('js')
@endsection