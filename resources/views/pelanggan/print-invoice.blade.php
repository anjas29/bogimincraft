<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Bogimin Craft</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ asset('/admin/bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/admin/dist/css/AdminLTE.min.css') }}">
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
    <section class="content">
      <div class='row'>
        <div class="col-md-12">
          <b><span class="logo-mini pull-left"><b><img src="/images/bogimin-b.png" style="height: auto; width: 180px;"></b></span></b>
          <span class="pull-right">Bogimin Craft beralamat di Jalan Kasongan, <br>RT 02, DK 17, Bangunjiwo, Kasihan, <br>Bantul, Daerah Istimewa Yogyakarta.
          </span>
          <br>
          <br>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"  style="margin: 0; padding: 0">
          <div class="box">
          <div class="box-body">
            <div class="content" style="padding-top:0;">
                <div class="row">
                    <h2 style="margin-top: 0;">Invoice</h2>
                    <h4><i class="fa fa-user"></i> <b>Detail Pelanggan</b></h4>
                    <div class="col-sm-12">
                        <div class="col-md-6">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="firstname">No Pesanan</label>
                                    <p><b>{{Session::get('temp_no_pesanan')}}</b></p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="firstname">Nama</label>
                                    <p><b>{{Session::get('temp_nama')}}</b></p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="lastname">Email</label>
                                    <p><b>{{auth('pelanggan')->user()->email}}</b></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="firstname">Alamat</label>
                                    <p><b>{{Session::get('temp_alamat')}}</b></p>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="firstname">No Telepon</label>
                                    <p><b>{{Session::get('temp_no_telepon')}}</b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <h4><i class="fa fa-cubes"></i> <b>Detail Barang</b></h4>
                    <div class="table-responsive">
                        <table class="table">
                        <?php $total = 0; $total_berat =0; ?>
                        @if ($data['count'] != 0)
                            <thead>
                                <tr>
                                    <th colspan="2">Barang</th>
                                    <th>Berat</th>
                                    <th>Jumlah</th>
                                    <th>Harga Satuan</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
                                <tbody>
                                @foreach($data['details'] as $d)
                                    <tr>
                                        <td>
                                            <img src="/images/barang/{{ $d->options->foto }}"  style="width:80px; height:80px"> 
                                        </td>
                                        <td>
                                            <strong> {{ $d->name }}</strong>
                                        </td>
                                        <td>
                                            {{ $d->options->berat }}
                                            <?php $total_berat += $d->options->berat ?>
                                        </td>
                                        <td>
                                            {{ $d->qty }}
                                        </td>
                                        <?php $total += $d->subtotal; ?>
                                        <td>Rp {{ number_format($d->price, 0, ',','.') }},-</td>
                                        <td>Rp {{ number_format($d->subtotal, 0, ',','.') }},-</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan='5'>Total Belanja</th>
                                        <th class="2">Rp {{ number_format($total, 0, ',','.') }},-</th>
                                    </tr>
                                    <tr>
                                        <th colspan="5">Diskon</th>
                                        <th class="2">Rp {{ number_format(Session::get('temp_diskon'), 0, ',','.') }},-</th>
                                    </tr>
                                    <tr>
                                        <th colspan='5'>Kota Tujuan</th>
                                        <th class="2">{{Session::get('temp_kota_tujuan')}}</th>
                                    </tr>
                                    <tr>
                                        <th colspan='5'>Ongkos kirim per kg</th>
                                        <th class="2">Rp {{ number_format(Session::get('temp_harga_per_kg'), 0, ',','.') }},-</th>
                                    </tr>
                                    <tr>
                                        <th colspan='5'>Total Ongkos Kirim</th>
                                        <th class="2">Rp {{ number_format(Session::get('temp_tarif_pengiriman'), 0, ',','.') }},-</th>
                                    </tr>
                                    <tr>
                                        <th colspan='5'>Kode Unik</th>
                                        <th class="2">Rp {{ number_format(Session::get('temp_kode_unik'), 0, ',','.')}},-</th>
                                    </tr>
                                    <tr>
                                        <th colspan='5'>Grand Total</th>
                                        <th class="2">Rp {{ number_format($total - Session::get('temp_diskon') + Session::get('temp_tarif_pengiriman') + Session::get('temp_kode_unik'), 0, ',','.' )}},-</th>
                                    </tr>
                                </tfoot>
                            @else
                                <thead>
                                    <tr>
                                        <th> <h3>Kosong</h3> </th>
                                    </tr>
                                </thead>
                            @endif
                        </table>
                    </div>
                </div>
            </div>
          </div>
          </div>
      </div>
      <!-- /.row -->
    </section>
    
  <script src="{{ asset('/admin/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
  
  <script src="{{ asset('/admin/dist/js/demo.js') }}"></script>
  <script type="text/javascript">
      $(document).ready(function() {
        window.print();
        setTimeout(function(){window.close();},1000);
        window.onfocus=function(){ window.close();}
      });
  </script>
</body>
</html>
